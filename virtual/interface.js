"use strict";
let express = require("express");
let webRouter = new express.Router();
let webInterface = require("./index.js");
let vm_webInterface = new webInterface();

webRouter.use("/baseInfo", (req, res, next) => {
    vm_webInterface.vmSystem_baseInfo(req, res, next);
});
webRouter.use("/create", (req, res, next) => {
    vm_webInterface.vmSystem_create(req, res, next);
});
webRouter.use("/modify", (req, res, next) => {
    vm_webInterface.vmSystem_modify(req, res, next);
});
webRouter.use("/remove", (req, res, next) => {
    vm_webInterface.vmSystem_remove(req, res, next);
});
webRouter.use("/start", (req, res, next) => {
    vm_webInterface.vmSystem_start(req, res, next);
});
webRouter.use("/stop", (req, res, next) => {
    vm_webInterface.vmSystem_stop(req, res, next);
});
webRouter.use("/ui_res", (req, res, next) => {
    vm_webInterface.vmSystem_UI_ExtraResource(req, res);
});
webRouter.use("/hostDetails", (req, res, next) => {
    vm_webInterface.vmSystem_HostDetails_Info(req, res);
});
webRouter.use("/softwareSetting", (req, res, next) => {
    vm_webInterface.vmSystem_softwareSetting(req, res);
});
webRouter.use("/ioVM_data", (req, res, next) => {
    vm_webInterface.vmSystem_ioData(req, res, next);
});
webRouter.use("/logging", (req, res, next) => {
    vm_webInterface.vmSystem_Logging(req, res);
});
module.exports = {vm_webInterface, webRouter}