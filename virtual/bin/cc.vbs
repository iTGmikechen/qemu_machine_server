'控制台输出显示
sub console(data)
    WScript.Echo data
End Sub

'数组、对象内成员数量
Function ArrOrObjLength(vl)
    dim lens
    lens=0
    For Each x in vl
        lens=lens+1
    Next
    ArrOrObjLength = lens
End Function

Function EnCompress(Target,Source)
    CreateObject("Scripting.FileSystemObject").CreateTextFile(Target, True).Write "PK" & Chr(5) & Chr(6) & String(18, vbNullChar)
    Set AccessPoint = CreateObject("Shell.Application").NameSpace(Target)
    AccessPoint.CopyHere Source,4
    Do
    WScript.Sleep 1000
    dim ge
    ge=AccessPoint.Items().Count
    Loop While ge < 1
End Function

Function DeCompress(Source,Target)
    Set ShellApplication = CreateObject("Shell.application")
    Set AccessPoint = ShellApplication.NameSpace(Source)
    ' CreateObject("Scripting.FileSystemObject").GetFolder(Target)
    Set TargetPoint = ShellApplication.NameSpace(Target)
    Set gj = AccessPoint.Items()
    TargetPoint.CopyHere gj,4
    Do
    WScript.Sleep 1000
    dim ge
    ge=TargetPoint.Items().Count
    Loop While ge < 1
End Function

'主函数
Function main(Param)
    console "欢迎使用zip文件处理"
    console "welcome using zip file process"
    console ""
    dim gf
    gf = ArrOrObjLength(Param)
    Select Case gf
        case 3
            dim OperateSymbol
            OperateSymbol=Param(0)
            Select Case OperateSymbol
                case "-c"
                    EnCompress Param(1),Param(2)
                    WScript.Quit(0)
                case "-d"
                    DeCompress Param(1),Param(2)
                    WScript.Quit(0)
                case Else
                    console "未知的指令！"
                    WScript.Quit(404)
            End Select
        case Else
            console "Usage:"
            console ""
            console "-c  要压缩文件或文件夹"
            console ""
            console "-d  解压缩文件或文件夹"
    End Select
End Function

main(WScript.Arguments)