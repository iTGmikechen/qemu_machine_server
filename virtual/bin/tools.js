module.exports = {
    confirmReadyStatus(confirmCount) {
        this.__readyReduce++;
        if (this.__readyReduce < confirmCount) {
            this.__readyStatus = false;
        } else {
            this.__readyStatus = true;
            this.__readyReduce = 0;
        }
    },
    stopDelay(confirmValue) {
        this.__delayReduce++;
        if (this.__delayReduce < confirmValue) {
            this.__delayStatus = true;
        } else {
            this.__delayStatus = false;
            this.__delayReduce = 0;
        }
    },
    detectData(data) {
        let detectValue
        try {
            detectValue = new this.__vmObject();
            for (let item in data) {
                if (String(item).includes("machine_configure") || String(item).includes("machine_hardware")) {
                    if (!String(item).includes("machine_hardware")) {
                        switch (String(item).toLowerCase()) {
                            case "machine_configure_name":
                                detectValue.name = data[item];
                                break;
                            case "machine_configure_model":
                                detectValue.model = data[item];
                                break;
                            case "machine_configure_framework":
                                detectValue.framework = data[item];
                                break;
                            case "machine_configure_ostype":
                                detectValue.osType = data[item];
                                break;
                            case "machine_configure_kvm":
                                switch (String(data[item]).toLowerCase()) {
                                    case "true":
                                        detectValue.kvm = true;
                                        break;
                                    case "false":
                                        detectValue.kvm = false;
                                        break;
                                    default:
                                        detectValue.kvm = true;
                                        break;
                                }
                                break;
                            case "machine_configure_fips":
                                switch (String(data[item]).toLowerCase()) {
                                    case "true":
                                        detectValue.fips = true;
                                        break;
                                    case "false":
                                        detectValue.fips = false;
                                        break;
                                    default:
                                        detectValue.fips = true;
                                        break;
                                }
                                break;
                            case "machine_configure_cpu":
                                detectValue.cpu = data[item];
                                break;
                            case "machine_configure_memory":
                                detectValue.mem = data[item];
                                break;
                            case "machine_configure_gpu":
                                detectValue.gpu = data[item];
                                break;
                            case "machine_configure_display":
                                detectValue.monitor = data[item];
                                break;
                            case "machine_configure_sound":
                                detectValue.sound = data[item];
                                break;
                            case "machine_configure_usb":
                                if (this.__vm_GenTools.DataTypeDetect(data[item]) === 'obj') {
                                    let count = 0;
                                    let isDAMON = true;
                                    detectValue.usb = {};
                                    for (let chkItem in data[item]) {
                                        switch (String(chkItem).toLowerCase()) {
                                            case "usb":
                                                if (typeof data[item][chkItem] === 'boolean') {
                                                    detectValue.usb.usb = data[item][chkItem];
                                                } else {
                                                    detectValue.usb.usb = false;
                                                }
                                                isDAMON = false;
                                                count++;
                                                break;
                                            case "keyboard":
                                                if (typeof data[item][chkItem] === 'boolean') {
                                                    detectValue.usb.keyboard = data[item][chkItem];
                                                } else {
                                                    detectValue.usb.keyboard = false;
                                                }
                                                count++;
                                                break;
                                            case "mouse":
                                                if (typeof data[item][chkItem] === 'boolean') {
                                                    detectValue.usb.mouse = data[item][chkItem];
                                                } else {
                                                    detectValue.usb.mouse = false;
                                                }
                                                count++;
                                                break;
                                            case "tablet":
                                                if (typeof data[item][chkItem] === 'boolean') {
                                                    detectValue.usb.tablet = data[item][chkItem];
                                                } else {
                                                    detectValue.usb.tablet = false;
                                                }
                                                count++;
                                                break;
                                            case "braille":
                                                if (typeof data[item][chkItem] === 'boolean') {
                                                    detectValue.usb.braille = data[item][chkItem];
                                                } else {
                                                    detectValue.usb.braille = false;
                                                }
                                                count++;
                                                break;
                                            case "wacom-tablet":
                                                if (typeof data[item][chkItem] === 'boolean') {
                                                    detectValue.usb["wacom-tablet"] = data[item][chkItem];
                                                } else {
                                                    detectValue.usb["wacom-tablet"] = false;
                                                }
                                                count++;
                                                break;
                                            default:
                                                throw new Error("USB 配置项目异常！");
                                        }
                                    }
                                    if (count !== 6 && isDAMON) {
                                        throw  new Error("");
                                    }
                                } else {
                                    detectValue.usb = {
                                        usb: false,
                                        keyboard: false,
                                        mouse: false,
                                        tablet: false,
                                        braille: false,
                                        "wacom-tablet": false
                                    }
                                }
                                break;
                            case "machine_configure_smp":
                                let convertSmp;
                                try {
                                    if (this.__vm_GenTools.DataTypeDetect(data[item]) === "str") {
                                        convertSmp = JSON.parse(data[item]);
                                    } else if (this.__vm_GenTools.DataTypeDetect(data[item]) === "num") {
                                        convertSmp = data[item];
                                    } else if (this.__vm_GenTools.DataTypeDetect(data[item]) === "obj") {
                                        convertSmp = this.__vm_GenTools.lightCopy(data[item]);
                                    }
                                    if (this.__vm_GenTools.DataTypeDetect(convertSmp) === "obj" && this.__vm_GenTools.emptyObjectDetect(convertSmp) === false) {
                                        detectValue.smp = {};
                                        for (let smpItem in convertSmp) {
                                            let numVal = parseInt(convertSmp[smpItem]);
                                            switch (String(smpItem).toLowerCase()) {
                                                case "clusters":
                                                    if (isFinite(numVal) === true && isNaN(numVal) === false && numVal > 0) {
                                                        detectValue.smp[String(smpItem).toLowerCase()] = numVal;
                                                    } else {
                                                        detectValue.smp[String(smpItem).toLowerCase()] = 1;
                                                    }
                                                    break;
                                                case "cores":
                                                    if (isFinite(numVal) === true && isNaN(numVal) === false && numVal > 0) {
                                                        detectValue.smp[String(smpItem).toLowerCase()] = numVal;
                                                    } else {
                                                        detectValue.smp[String(smpItem).toLowerCase()] = 1;
                                                    }
                                                    break;
                                                case "cpus":
                                                    if (isFinite(numVal) === true && isNaN(numVal) === false && numVal > 0) {
                                                        detectValue.smp[String(smpItem).toLowerCase()] = numVal;
                                                    } else {
                                                        detectValue.smp[String(smpItem).toLowerCase()] = 1;
                                                    }
                                                    break;
                                                case "dies":
                                                    if (isFinite(numVal) === true && isNaN(numVal) === false && numVal > 0) {
                                                        detectValue.smp[String(smpItem).toLowerCase()] = numVal;
                                                    } else {
                                                        detectValue.smp[String(smpItem).toLowerCase()] = 1;
                                                    }
                                                    break;
                                                case "maxcpus":
                                                    if (isFinite(numVal) === true && isNaN(numVal) === false && numVal > 0) {
                                                        detectValue.smp[String(smpItem).toLowerCase()] = numVal;
                                                    } else {
                                                        detectValue.smp[String(smpItem).toLowerCase()] = 1;
                                                    }
                                                    break;
                                                case "sockets":
                                                    if (isFinite(numVal) === true && isNaN(numVal) === false && numVal > 0) {
                                                        detectValue.smp[String(smpItem).toLowerCase()] = numVal;
                                                    } else {
                                                        detectValue.smp[String(smpItem).toLowerCase()] = 1;
                                                    }
                                                    break;
                                                case "threads":
                                                    if (isFinite(numVal) === true && isNaN(numVal) === false && numVal > 0) {
                                                        detectValue.smp[String(smpItem).toLowerCase()] = numVal;
                                                    } else {
                                                        detectValue.smp[String(smpItem).toLowerCase()] = 1;
                                                    }
                                                    break;
                                                default:
                                                    throw new Error("");
                                            }
                                        }
                                    } else {
                                        if (isNaN(convertSmp) === false && isFinite(convertSmp) === true && parseInt(convertSmp) > 0) {
                                            detectValue.smp = parseInt(data[item]);
                                        } else {
                                            detectValue.smp = 1;
                                        }
                                    }
                                } catch (e) {
                                    convertSmp = parseInt(data[item]);
                                    if (isNaN(convertSmp) === false && isFinite(convertSmp) === true && parseInt(convertSmp) > 0) {
                                        detectValue.smp = parseInt(data[item]);
                                    } else {
                                        detectValue.smp = 1;
                                    }
                                }
                                break;
                            case "machine_configure_uefi":
                                detectValue.efi = data[item];
                                break;
                            case "machine_configure_boot":
                                if (this.__vm_GenTools.DataTypeDetect(data[item]) === "str") {
                                    if (String(data[item]).toString() !== "") {
                                        detectValue.boot = data[item];
                                    } else {
                                        detectValue.boot = "c";
                                    }
                                } else if (this.__vm_GenTools.DataTypeDetect(data[item]) === "obj") {
                                    detectValue.boot = data[item];
                                }
                                break;
                            case "machine_configure_vts":
                                if (this.__vm_GenTools.DataTypeDetect(data[item]) === "str") {
                                    detectValue.vts = data[item];
                                } else if (this.__vm_GenTools.DataTypeDetect(data[item]) === "obj" && this.__vm_GenTools.emptyObjectDetect(data[item]) === false) {
                                    detectValue.vts = {}
                                    for (let insItem in data[item]) {
                                        if (String(insItem).toLowerCase() !== "accel") {
                                            if (data[item][insItem].enable) {
                                                detectValue.vts[insItem] = data[item][insItem].value;
                                            }
                                        } else {
                                            detectValue.vts[insItem] = data[item][insItem];
                                        }
                                    }
                                } else {
                                    throw new Error("");
                                }
                                break;
                            case "machine_configure_rtc":
                                if (this.__vm_GenTools.DataTypeDetect(data[item]) === "str") {
                                    if (String(data[item]).toString() !== "") {
                                        detectValue.rtc = data[item];
                                    } else {
                                        detectValue.rtc = "localtime"
                                    }
                                } else if (this.__vm_GenTools.DataTypeDetect(data[item]) === "obj") {
                                    detectValue.rtc = data[item];
                                }
                                break;
                            default:
                                throw new Error("");
                        }
                    } else {
                        if (this.__vm_GenTools.DataTypeDetect(detectValue.hardware) !== "arr") {
                            detectValue.hardware = [];
                        }
                        let ddc, ddg;
                        try {
                            ddc = JSON.parse(data[item]);
                        } catch (e) {
                            ddc = this.__vm_GenTools.DeepCopyInstance(data[item]);
                        }
                        try {
                            if (this.__vm_GenTools.DataTypeDetect(ddc) === "obj" && this.__vm_GenTools.emptyObjectDetect(ddc) === false) {
                                ddg = {};
                                for (let num in ddc) {
                                    switch (String(num).toLowerCase()) {
                                        case "devicename":
                                            ddg.DeviceName = ddc[num];
                                            break;
                                        case "operate_value":
                                            ddg.Operate_Value = ddc[num];
                                            break;
                                        case "operate_item":
                                            ddg.Operate_Item = ddc[num];
                                            break;
                                        case "extra_param":
                                            ddg.Extra_Param = ddc[num];
                                            break;
                                        case "needoperate":
                                            ddg.NeedOperate = ddc[num];
                                            break;
                                        case "devicetype":
                                            ddg.DeviceType = ddc[num];
                                            break;
                                        default:
                                            throw new Error("");
                                    }
                                }
                                if (ddg.DeviceType === 'drive') {
                                    if (this.__vm_GenTools.DataTypeDetect(ddg.Operate_Value) === "obj"
                                        &&
                                        this.__vm_GenTools.emptyObjectDetect(ddg.Operate_Value) === false
                                    ) {
                                        for (let driveChk in ddg.Operate_Value) {
                                            switch (String(driveChk).toLowerCase()) {
                                                case "index":
                                                    break;
                                                case "id":
                                                    break;
                                                case "if":
                                                    break;
                                                case"media":
                                                    break;
                                                case"aio":
                                                    break;
                                                case "format":
                                                    break;
                                                case "cache":
                                                    break;
                                                case"file":
                                                    break;
                                                default:
                                                    throw new Error("");
                                            }
                                        }
                                    } else {
                                        throw new Error("");
                                    }
                                }
                                detectValue.hardware.push(ddg);
                            }
                        } catch (e) {
                        }
                    }
                } else {
                    if (String(item).toLowerCase() !== "id") {
                        throw new Error("");
                    }
                }
            }
        } catch (e) {
            detectValue = false;
        }
        return detectValue;
    },
    preDataDetect(dataSource) {
        let status;
        let detectPass;
        try {
            let PreConfigDetect = (dataCollect) => {
                let ps
                try {
                    if (Object.prototype.toString.call(dataCollect) === "[object Object]") {
                        ps = {};
                        for (let test in dataCollect) {
                            let temp3 = {};
                            let tmp1;
                            switch (String(test).toLowerCase()) {
                                case "machine_configure_model":
                                    if (Object.prototype.toString.call(dataCollect[test]) === "[object Object]") {
                                        for (let item3 in dataCollect[test]) {
                                            switch (String(item3).toLowerCase()) {
                                                case "enable":
                                                    if (typeof dataCollect[test][item3] === 'boolean') {
                                                        temp3.enable = dataCollect[test][item3];
                                                    } else {
                                                        throw new Error("");
                                                    }
                                                    break;
                                                case "value":
                                                    if (typeof dataCollect[test][item3] === 'string') {
                                                        temp3.value = dataCollect[test][item3];
                                                    } else {
                                                        throw new Error("");
                                                    }
                                                    break;
                                                default:
                                                    throw new Error("");
                                            }
                                        }
                                        ps.machine_configure_model = temp3;
                                    } else {
                                        throw new Error("");
                                    }
                                    break;
                                case "machine_configure_rtc":
                                    if (Object.prototype.toString.call(dataCollect[test]) === "[object Object]") {
                                        for (let item3 in dataCollect[test]) {
                                            switch (String(item3).toLowerCase()) {
                                                case "enable":
                                                    if (typeof dataCollect[test][item3] === 'boolean') {
                                                        temp3.enable = dataCollect[test][item3];
                                                    } else {
                                                        throw new Error("");
                                                    }
                                                    break;
                                                case "value":
                                                    if (typeof dataCollect[test][item3] === 'string') {
                                                        temp3.value = dataCollect[test][item3];
                                                    } else if (Object.prototype.toString.call(dataCollect[test][item3]) === "[object Object]") {
                                                        tmp1 = dataCollect[test][item3];
                                                    } else {
                                                        throw new Error("");
                                                    }
                                                    break;
                                                case "mode":
                                                    if (typeof dataCollect[test][item3] === 'boolean') {
                                                        temp3.mode = dataCollect[test][item3];
                                                    } else {
                                                        throw new Error("");
                                                    }
                                                    break;
                                                case "sim_config":
                                                    if (typeof dataCollect[test][item3] === 'boolean') {
                                                        temp3.sim_config = dataCollect[test][item3];
                                                    } else {
                                                        throw new Error("");
                                                    }
                                                    break;
                                                default:
                                                    throw new Error("");
                                            }
                                        }
                                        if (!temp3.mode) {
                                            if (Object.prototype.toString.call(tmp1) === "[object Object]") {
                                                if (Reflect.ownKeys(tmp1).length > 0) {
                                                    let tmp2 = {}
                                                    for (let item4 in tmp1) {
                                                        switch (String(item4).toLowerCase()) {
                                                            case "base":
                                                                tmp2.base = tmp1[item4];
                                                                break;
                                                            case "clock":
                                                                if (tmp1[item4] === "host" || tmp1[item4] === "rt" || tmp1[item4] === "vm") {
                                                                    tmp2.clock = tmp1[item4];
                                                                } else {
                                                                    throw new Error("");
                                                                }
                                                                break;
                                                            case "driftfix":
                                                                if (tmp1[item4] === "none" || tmp1[item4] === "slew") {
                                                                    tmp2.driftfix = tmp1[item4];
                                                                } else {
                                                                    throw new Error("");
                                                                }
                                                                break;
                                                            default:
                                                                throw new Error("");
                                                        }
                                                    }
                                                    temp3.value = tmp2;
                                                } else {
                                                    throw new Error("");
                                                }
                                            } else {
                                                throw new Error("");
                                            }
                                        }
                                        ps.machine_configure_rtc = temp3;
                                    } else {
                                        throw new Error("");
                                    }
                                    break;
                                case"machine_configure_cpu":
                                    if (Object.prototype.toString.call(dataCollect[test]) === "[object Object]") {
                                        for (let item3 in dataCollect[test]) {
                                            switch (String(item3).toLowerCase()) {
                                                case "enable":
                                                    if (typeof dataCollect[test][item3] === 'boolean') {
                                                        temp3.enable = dataCollect[test][item3];
                                                    } else {
                                                        throw new Error("");
                                                    }
                                                    break;
                                                case "value":
                                                    if (typeof dataCollect[test][item3] === 'string') {
                                                        temp3.value = dataCollect[test][item3];
                                                    } else {
                                                        throw new Error("");
                                                    }
                                                    break;
                                                default:
                                                    throw new Error("");
                                            }
                                        }
                                        ps.machine_configure_cpu = temp3;
                                    } else {
                                        throw new Error("");
                                    }
                                    break;
                                case "machine_configure_memory":
                                    if (Object.prototype.toString.call(dataCollect[test]) === "[object Object]") {
                                        for (let item3 in dataCollect[test]) {
                                            switch (String(item3).toLowerCase()) {
                                                case "enable":
                                                    if (typeof dataCollect[test][item3] === 'boolean') {
                                                        temp3.enable = dataCollect[test][item3];
                                                    } else {
                                                        throw new Error("");
                                                    }
                                                    break;
                                                case "value":
                                                    if (typeof dataCollect[test][item3] === 'number') {
                                                        temp3.value = dataCollect[test][item3];
                                                    } else {
                                                        throw new Error("");
                                                    }
                                                    break;
                                                default:
                                                    throw new Error("");
                                            }
                                        }
                                        ps.machine_configure_memory = temp3;
                                    } else {
                                        throw new Error("");
                                    }
                                    break;
                                case"machine_configure_gpu":
                                    if (Object.prototype.toString.call(dataCollect[test]) === "[object Object]") {
                                        for (let item3 in dataCollect[test]) {
                                            switch (String(item3).toLowerCase()) {
                                                case "enable":
                                                    if (typeof dataCollect[test][item3] === 'boolean') {
                                                        temp3.enable = dataCollect[test][item3];
                                                    } else {
                                                        throw new Error("");
                                                    }
                                                    break;
                                                case "value":
                                                    if (typeof dataCollect[test][item3] === 'string') {
                                                        temp3.value = dataCollect[test][item3];
                                                    } else {
                                                        throw new Error("");
                                                    }
                                                    break;
                                                default:
                                                    throw new Error("");
                                            }
                                        }
                                        ps.machine_configure_gpu = temp3;
                                    } else {
                                        throw new Error("");
                                    }
                                    break;
                                case "machine_configure_display":
                                    if (Object.prototype.toString.call(dataCollect[test]) === "[object Object]") {
                                        for (let item3 in dataCollect[test]) {
                                            switch (String(item3).toLowerCase()) {
                                                case "enable":
                                                    if (typeof dataCollect[test][item3] === 'boolean') {
                                                        temp3.enable = dataCollect[test][item3];
                                                    } else {
                                                        throw new Error("");
                                                    }
                                                    break;
                                                case "mode":
                                                    temp3.mode = dataCollect[test][item3];
                                                    break;
                                                case "value":
                                                    if (typeof dataCollect[test][item3] === 'string') {
                                                        temp3.value = dataCollect[test][item3];
                                                    } else {
                                                        throw new Error("");
                                                    }
                                                    break;
                                                case "fullscreen":
                                                    if (typeof dataCollect[test][item3] === 'boolean') {
                                                        temp3.fullscreen = dataCollect[test][item3];
                                                    } else {
                                                        throw new Error("");
                                                    }
                                                    break;
                                                case "gl":
                                                    if (typeof dataCollect[test][item3] === 'boolean') {
                                                        temp3.gl = dataCollect[test][item3];
                                                    } else {
                                                        throw new Error("");
                                                    }
                                                    break;
                                                default:
                                                    throw new Error("");
                                            }
                                        }
                                        if (typeof temp3.gl === 'boolean') {
                                            if (temp3.value !== 'sdl') {
                                                throw new Error("");
                                            } else {
                                                temp3.mode = false;
                                            }
                                        }
                                        ps.machine_configure_display = temp3;
                                    } else {
                                        throw new Error("");
                                    }
                                    break;
                                case"machine_configure_vts":
                                    if (Object.prototype.toString.call(dataCollect[test]) === "[object Object]") {
                                        let dgw;
                                        for (let item3 in dataCollect[test]) {
                                            switch (String(item3).toLowerCase()) {
                                                case "enable":
                                                    if (typeof dataCollect[test][item3] === 'boolean') {
                                                        temp3.enable = dataCollect[test][item3];
                                                    } else {
                                                        throw new Error("");
                                                    }
                                                    break;
                                                case "mode":
                                                    temp3.mode = dataCollect[test][item3];
                                                    break;
                                                case "value":
                                                    if (typeof dataCollect[test][item3] === 'string') {
                                                        temp3.value = dataCollect[test][item3];
                                                    } else if (Object.prototype.toString.call(dataCollect[test][item3])) {
                                                        dgw = dataCollect[test][item3];
                                                    } else {
                                                        throw new Error("");
                                                    }
                                                    break;
                                                default:
                                                    throw new Error("");
                                            }
                                        }
                                        if (!temp3.mode) {
                                            if (Object.prototype.toString.call(dgw) === "[object Object]" && Reflect.ownKeys(dgw).length > 0) {
                                                temp3.value = {};
                                                for (let gda in dgw) {
                                                    switch (String(gda).toLowerCase()) {
                                                        case "accel":
                                                            if (typeof dgw[gda] === 'string') {
                                                                temp3.value.accel = dgw[gda];
                                                            } else {
                                                                throw new Error("");
                                                            }
                                                            break;
                                                        case "igd-passthru":
                                                            if (Object.prototype.toString.call(dgw[gda]) === "[object Object]" && Reflect.ownKeys(dgw[gda]).length > 0) {
                                                                temp3.value["igd-passthru"] = {};
                                                                for (let dgh in dgw[gda]) {
                                                                    switch (String(dgh).toLowerCase()) {
                                                                        case"enable":
                                                                            if (typeof dgw[gda][dgh] !== "boolean") {
                                                                                throw new Error("");
                                                                            } else {
                                                                                temp3.value["igd-passthru"].enable = dgw[gda][dgh];
                                                                            }
                                                                            break;
                                                                        case "value":
                                                                            if (typeof dgw[gda][dgh] === "string") {
                                                                                if (String(dgw[gda][dgh]).toLowerCase() === "on") {
                                                                                    temp3.value["igd-passthru"].value = "on"
                                                                                } else if (String(dgw[gda][dgh]).toLowerCase() === "off") {
                                                                                    temp3.value["igd-passthru"].value = "off"
                                                                                } else {
                                                                                    throw new Error("");
                                                                                }
                                                                            } else {
                                                                                throw new Error("");
                                                                            }
                                                                            break;
                                                                        default:
                                                                            throw new Error("");
                                                                    }
                                                                }
                                                            } else {
                                                                throw new Error("");
                                                            }
                                                            break;
                                                        case "kernel-irqchip":
                                                            if (Object.prototype.toString.call(dgw[gda]) === "[object Object]" && Reflect.ownKeys(dgw[gda]).length > 0) {
                                                                temp3.value["kernel-irqchip"] = {};
                                                                for (let dgh in dgw[gda]) {
                                                                    switch (String(dgh).toLowerCase()) {
                                                                        case"enable":
                                                                            if (typeof dgw[gda][dgh] !== "boolean") {
                                                                                throw new Error("");
                                                                            } else {
                                                                                temp3.value["kernel-irqchip"].enable = dgw[gda][dgh];
                                                                            }
                                                                            break;
                                                                        case "value":
                                                                            if (typeof dgw[gda][dgh] === "string") {
                                                                                if (String(dgw[gda][dgh]).toLowerCase() === "on") {
                                                                                    temp3.value["kernel-irqchip"].value = "on"
                                                                                } else if (String(dgw[gda][dgh]).toLowerCase() === "off") {
                                                                                    temp3.value["kernel-irqchip"].value = "off"
                                                                                } else if (String(dgw[gda][dgh]).toLowerCase() === "split") {
                                                                                    temp3.value["kernel-irqchip"].value = "split"
                                                                                } else {
                                                                                    throw new Error("");
                                                                                }
                                                                            } else {
                                                                                throw new Error("");
                                                                            }
                                                                            break;
                                                                        default:
                                                                            throw new Error("");
                                                                    }
                                                                }
                                                            } else {
                                                                throw new Error("");
                                                            }
                                                            break;
                                                        case "split-wx":
                                                            if (Object.prototype.toString.call(dgw[gda]) === "[object Object]" && Reflect.ownKeys(dgw[gda]).length > 0) {
                                                                temp3.value["split-wx"] = {};
                                                                for (let dgh in dgw[gda]) {
                                                                    switch (String(dgh).toLowerCase()) {
                                                                        case"enable":
                                                                            if (typeof dgw[gda][dgh] !== "boolean") {
                                                                                throw new Error("");
                                                                            } else {
                                                                                temp3.value["split-wx"].enable = dgw[gda][dgh];
                                                                            }
                                                                            break;
                                                                        case "value":
                                                                            if (typeof dgw[gda][dgh] === "string") {
                                                                                if (String(dgw[gda][dgh]).toLowerCase() === "on") {
                                                                                    temp3.value["split-wx"].value = "on"
                                                                                } else if (String(dgw[gda][dgh]).toLowerCase() === "off") {
                                                                                    temp3.value["split-wx"].value = "off"
                                                                                } else {
                                                                                    throw new Error("");
                                                                                }
                                                                            } else {
                                                                                throw new Error("");
                                                                            }
                                                                            break;
                                                                        default:
                                                                            throw new Error("");
                                                                    }
                                                                }
                                                            } else {
                                                                throw new Error("");
                                                            }
                                                            break;
                                                        case "kvm-shadow-mem":
                                                            if (Object.prototype.toString.call(dgw[gda]) === "[object Object]" && Reflect.ownKeys(dgw[gda]).length > 0) {
                                                                temp3.value["kvm-shadow-mem"] = {};
                                                                for (let dgh in dgw[gda]) {
                                                                    switch (String(dgh).toLowerCase()) {
                                                                        case"enable":
                                                                            if (typeof dgw[gda][dgh] !== "boolean") {
                                                                                throw new Error("");
                                                                            } else {
                                                                                temp3.value["kvm-shadow-mem"].enable = dgw[gda][dgh];
                                                                            }
                                                                            break;
                                                                        case "value":
                                                                            if (typeof dgw[gda][dgh] === "number") {
                                                                                temp3.value["kvm-shadow-mem"].value = dgw[gda][dgh];
                                                                            } else {
                                                                                throw new Error("");
                                                                            }
                                                                            break;
                                                                        default:
                                                                            throw new Error("");
                                                                    }
                                                                }
                                                            } else {
                                                                throw new Error("");
                                                            }
                                                            break;
                                                        case "tb-size":
                                                            if (Object.prototype.toString.call(dgw[gda]) === "[object Object]" && Reflect.ownKeys(dgw[gda]).length > 0) {
                                                                temp3.value["tb-size"] = {};
                                                                for (let dgh in dgw[gda]) {
                                                                    switch (String(dgh).toLowerCase()) {
                                                                        case"enable":
                                                                            if (typeof dgw[gda][dgh] !== "boolean") {
                                                                                throw new Error("");
                                                                            } else {
                                                                                temp3.value["tb-size"].enable = dgw[gda][dgh];
                                                                            }
                                                                            break;
                                                                        case "value":
                                                                            if (typeof dgw[gda][dgh] === "number") {
                                                                                temp3.value["tb-size"].value = dgw[gda][dgh];
                                                                            } else {
                                                                                throw new Error("");
                                                                            }
                                                                            break;
                                                                        default:
                                                                            throw new Error("");
                                                                    }
                                                                }
                                                            } else {
                                                                throw new Error("");
                                                            }
                                                            break;
                                                        case "dirty-ring-size":
                                                            if (Object.prototype.toString.call(dgw[gda]) === "[object Object]" && Reflect.ownKeys(dgw[gda]).length > 0) {
                                                                temp3.value["dirty-ring-size"] = {};
                                                                for (let dgh in dgw[gda]) {
                                                                    switch (String(dgh).toLowerCase()) {
                                                                        case"enable":
                                                                            if (typeof dgw[gda][dgh] !== "boolean") {
                                                                                throw new Error("");
                                                                            } else {
                                                                                temp3.value["dirty-ring-size"].enable = dgw[gda][dgh];
                                                                            }
                                                                            break;
                                                                        case "value":
                                                                            if (typeof dgw[gda][dgh] === "number") {
                                                                                temp3.value["dirty-ring-size"].value = dgw[gda][dgh];
                                                                            } else {
                                                                                throw new Error("");
                                                                            }
                                                                            break;
                                                                        default:
                                                                            throw new Error("");
                                                                    }
                                                                }
                                                            } else {
                                                                throw new Error("");
                                                            }
                                                            break;
                                                        case "thread":
                                                            if (Object.prototype.toString.call(dgw[gda]) === "[object Object]" && Reflect.ownKeys(dgw[gda]).length > 0) {
                                                                temp3.value["thread"] = {};
                                                                for (let dgh in dgw[gda]) {
                                                                    switch (String(dgh).toLowerCase()) {
                                                                        case"enable":
                                                                            if (typeof dgw[gda][dgh] !== "boolean") {
                                                                                throw new Error("");
                                                                            } else {
                                                                                temp3.value["thread"].enable = dgw[gda][dgh];
                                                                            }
                                                                            break;
                                                                        case "value":
                                                                            if (typeof dgw[gda][dgh] === "string") {
                                                                                if (String(dgw[gda][dgh]).toLowerCase() === "single") {
                                                                                    temp3.value["thread"].value = "single"
                                                                                } else if (String(dgw[gda][dgh]).toLowerCase() === "multi") {
                                                                                    temp3.value["thread"].value = "multi";
                                                                                } else {
                                                                                    throw new Error("");
                                                                                }
                                                                            } else {
                                                                                throw new Error("");
                                                                            }
                                                                            break;
                                                                        default:
                                                                            throw new Error("");
                                                                    }
                                                                }
                                                            } else {
                                                                throw new Error("");
                                                            }
                                                            break;
                                                        default:
                                                            throw new Error("");
                                                    }
                                                }
                                            }
                                        }
                                        ps.machine_configure_vts = temp3;
                                    } else {
                                        throw new Error("");
                                    }
                                    break;
                                case"machine_configure_kvm":
                                    if (Object.prototype.toString.call(dataCollect[test]) === "[object Object]") {
                                        for (let item3 in dataCollect[test]) {
                                            switch (String(item3).toLowerCase()) {
                                                case "enable":
                                                    if (typeof dataCollect[test][item3] === 'boolean') {
                                                        temp3.enable = dataCollect[test][item3];
                                                    } else {
                                                        throw new Error("");
                                                    }
                                                    break;
                                                case "value":
                                                    if (typeof dataCollect[test][item3] === 'boolean') {
                                                        temp3.value = dataCollect[test][item3];
                                                    } else {
                                                        throw new Error("");
                                                    }
                                                    break;
                                                default:
                                                    throw new Error("");
                                            }
                                        }
                                        ps.machine_configure_kvm = temp3;
                                    } else {
                                        throw new Error("");
                                    }
                                    break;
                                case "machine_configure_fips":
                                    if (Object.prototype.toString.call(dataCollect[test]) === "[object Object]") {
                                        for (let item3 in dataCollect[test]) {
                                            switch (String(item3).toLowerCase()) {
                                                case "enable":
                                                    if (typeof dataCollect[test][item3] === 'boolean') {
                                                        temp3.enable = dataCollect[test][item3];
                                                    } else {
                                                        throw new Error("");
                                                    }
                                                    break;
                                                case "value":
                                                    if (typeof dataCollect[test][item3] === 'boolean') {
                                                        temp3.value = dataCollect[test][item3];
                                                    } else {
                                                        throw new Error("");
                                                    }
                                                    break;
                                                default:
                                                    throw new Error("");
                                            }
                                        }
                                        ps.machine_configure_fips = temp3;
                                    } else {
                                        throw new Error("");
                                    }
                                    break;
                                case "machine_configure_smp":
                                    if (Object.prototype.toString.call(dataCollect[test]) === "[object Object]") {
                                        for (let item3 in dataCollect[test]) {
                                            switch (String(item3).toLowerCase()) {
                                                case "enable":
                                                    if (typeof dataCollect[test][item3] === 'boolean') {
                                                        temp3.enable = dataCollect[test][item3];
                                                    } else {
                                                        throw new Error("");
                                                    }
                                                    break;
                                                case "value":
                                                    if (typeof dataCollect[test][item3] === 'number') {
                                                        temp3.value = dataCollect[test][item3];
                                                    } else if (Object.prototype.toString.call(dataCollect[test][item3]) === "[object Object]") {
                                                        tmp1 = dataCollect[test][item3];
                                                    } else {
                                                        throw new Error("");
                                                    }
                                                    break;
                                                case "mode":
                                                    if (typeof dataCollect[test][item3] === 'boolean') {
                                                        temp3.mode = dataCollect[test][item3];
                                                    } else {
                                                        throw new Error("");
                                                    }
                                                    break;
                                                default:
                                                    throw new Error("");
                                            }
                                        }
                                        if (!temp3.mode) {
                                            if (Object.prototype.toString.call(tmp1) === "[object Object]") {
                                                if (Reflect.ownKeys(tmp1).length > 0) {
                                                    let tmp2 = {}
                                                    let dfs = (det) => {
                                                        let pgs;
                                                        try {
                                                            if (Object.prototype.toString.call(det) === "[object Object]" && Reflect.ownKeys(det).length > 0) {
                                                                pgs = {}
                                                                for (let it1 in det) {
                                                                    switch (String(it1).toLowerCase()) {
                                                                        case "enable":
                                                                            if (typeof det[it1] === "boolean") {
                                                                                pgs.enable = det[it1];
                                                                            } else {
                                                                                throw new Error("");
                                                                            }
                                                                            break;
                                                                        case "value":
                                                                            if (typeof det[it1] === "number") {
                                                                                pgs.value = det[it1];
                                                                            } else {
                                                                                throw new Error("");
                                                                            }
                                                                            break;
                                                                        case "name":
                                                                            if (typeof det[it1] === "string") {
                                                                                pgs.name = det[it1];
                                                                            } else {
                                                                                throw new Error("");
                                                                            }
                                                                            break;
                                                                        default:
                                                                            throw new Error("");
                                                                    }
                                                                }
                                                            } else {
                                                                throw new Error("")
                                                            }
                                                        } catch (e) {
                                                            pgs = false;
                                                        }
                                                        return pgs;
                                                    }
                                                    for (let item4 in tmp1) {
                                                        switch (String(item4).toLowerCase()) {
                                                            case "clusters":
                                                                if (dfs(tmp1[item4])) {
                                                                    tmp2.clusters = dfs(tmp1[item4]);
                                                                } else {
                                                                    throw new Error("")
                                                                }
                                                                break;
                                                            case "maxcpus":
                                                                if (dfs(tmp1[item4])) {
                                                                    tmp2.maxcpus = dfs(tmp1[item4]);
                                                                } else {
                                                                    throw new Error("")
                                                                }
                                                                break;
                                                            case "sockets":
                                                                if (dfs(tmp1[item4])) {
                                                                    tmp2.socket = dfs(tmp1[item4]);
                                                                } else {
                                                                    throw new Error("")
                                                                }
                                                                break;
                                                            case "threads":
                                                                if (dfs(tmp1[item4])) {
                                                                    tmp2.threads = dfs(tmp1[item4]);
                                                                } else {
                                                                    throw new Error("")
                                                                }
                                                                break;
                                                            case "cores":
                                                                if (dfs(tmp1[item4])) {
                                                                    tmp2.cores = dfs(tmp1[item4]);
                                                                } else {
                                                                    throw new Error("")
                                                                }
                                                                break;
                                                            case "cpus":
                                                                if (dfs(tmp1[item4])) {
                                                                    tmp2.cpus = dfs(tmp1[item4]);
                                                                } else {
                                                                    throw new Error("")
                                                                }
                                                                break;
                                                            case "dies":
                                                                if (dfs(tmp1[item4])) {
                                                                    tmp2.dies = dfs(tmp1[item4]);
                                                                } else {
                                                                    throw new Error("");
                                                                }
                                                                break;
                                                            default:
                                                                throw new Error("");
                                                        }
                                                    }
                                                    temp3.value = tmp2;
                                                } else {
                                                    throw new Error("");
                                                }
                                            } else {
                                                throw new Error("");
                                            }
                                        }
                                        ps.machine_configure_smp = temp3;
                                    } else {
                                        throw new Error("");
                                    }
                                    break;
                                case "machine_configure_sound":
                                    if (Object.prototype.toString.call(dataCollect[test]) === "[object Object]") {
                                        for (let item3 in dataCollect[test]) {
                                            switch (String(item3).toLowerCase()) {
                                                case "enable":
                                                    if (typeof dataCollect[test][item3] === 'boolean') {
                                                        temp3.enable = dataCollect[test][item3];
                                                    } else {
                                                        throw new Error("");
                                                    }
                                                    break;
                                                case "value":
                                                    if (typeof dataCollect[test][item3] === 'string') {
                                                        temp3.value = dataCollect[test][item3];
                                                    } else {
                                                        throw new Error("");
                                                    }
                                                    break;
                                                default:
                                                    throw new Error("");
                                            }
                                        }
                                        ps.machine_configure_sound = temp3;
                                    } else {
                                        throw new Error("");
                                    }
                                    break;
                                case "machine_configure_uefi":
                                    if (Object.prototype.toString.call(dataCollect[test]) === "[object Object]") {
                                        for (let item3 in dataCollect[test]) {
                                            switch (String(item3).toLowerCase()) {
                                                case "enable":
                                                    if (typeof dataCollect[test][item3] === 'boolean') {
                                                        temp3.enable = dataCollect[test][item3];
                                                    } else {
                                                        throw new Error("");
                                                    }
                                                    break;
                                                case "value":
                                                    if (typeof dataCollect[test][item3] === 'string') {
                                                        temp3.value = dataCollect[test][item3];
                                                    } else {
                                                        throw new Error("");
                                                    }
                                                    break;
                                                default:
                                                    throw new Error("");
                                            }
                                        }
                                        ps.machine_configure_uefi = temp3;
                                    } else {
                                        throw new Error("");
                                    }
                                    break;
                                case"machine_configure_boot":
                                    if (Object.prototype.toString.call(dataCollect[test]) === "[object Object]") {
                                        let dgw;
                                        for (let item3 in dataCollect[test]) {
                                            switch (String(item3).toLowerCase()) {
                                                case "enable":
                                                    if (typeof dataCollect[test][item3] === 'boolean') {
                                                        temp3.enable = dataCollect[test][item3];
                                                    } else {
                                                        throw new Error("");
                                                    }
                                                    break;
                                                case "mode":
                                                    temp3.mode = dataCollect[test][item3];
                                                    break;
                                                case "value":
                                                    if (typeof dataCollect[test][item3] === 'string') {
                                                        temp3.value = dataCollect[test][item3];
                                                    } else if (Object.prototype.toString.call(dataCollect[test][item3])) {
                                                        dgw = dataCollect[test][item3];
                                                    } else {
                                                        throw new Error("");
                                                    }
                                                    break;
                                                default:
                                                    throw new Error("");
                                            }
                                        }
                                        if (!temp3.mode) {
                                            if (Object.prototype.toString.call(dgw) === "[object Object]" && Reflect.ownKeys(dgw).length > 0) {
                                                temp3.value = {};
                                                for (let gda in dgw) {
                                                    switch (String(gda).toLowerCase()) {
                                                        case "order":
                                                            if (typeof dgw[gda] === 'string') {
                                                                if (String(dgw[gda]).toLowerCase() === "a") {
                                                                    temp3.value.order = "a"
                                                                } else if (String(dgw[gda]).toLowerCase() === "c") {
                                                                    temp3.value.order = "c"
                                                                } else if (String(dgw[gda]).toLowerCase() === "d") {
                                                                    temp3.value.order = "d"
                                                                } else if (String(dgw[gda]).toLowerCase() === "n") {
                                                                    temp3.value.order = "n"
                                                                } else {
                                                                    throw new Error("");
                                                                }
                                                            } else {
                                                                throw new Error("");
                                                            }
                                                            break;
                                                        case "menu":
                                                            if (typeof dgw[gda] === "boolean") {
                                                                temp3.value.menu = dgw[gda];
                                                            } else {
                                                                throw new Error("");
                                                            }
                                                            break;
                                                        case "strict":
                                                            if (typeof dgw[gda] === "boolean") {
                                                                temp3.value.strict = dgw[gda];
                                                            } else {
                                                                throw new Error("");
                                                            }
                                                            break;
                                                        case "splash-time":
                                                            if (typeof dgw[gda] === "number" && Number(dgw[gda]).valueOf() >= 0) {
                                                                temp3.value["splash-time"] = parseInt(dgw[gda]);
                                                            } else {
                                                                throw new Error("");
                                                            }
                                                            break;
                                                        case "reboot-timeout":
                                                            if (typeof dgw[gda] === "number" && Number(dgw[gda]).valueOf() >= 0) {
                                                                temp3.value["reboot-timeout"] = parseInt(dgw[gda]);
                                                            } else {
                                                                throw new Error("");
                                                            }
                                                            break;
                                                        default:
                                                            throw new Error("");
                                                    }
                                                }
                                            }
                                        }
                                        ps.machine_configure_boot = temp3;
                                    } else {
                                        throw new Error("");
                                    }
                                    break;
                                default:
                                    throw new Error("");
                            }
                        }
                    } else {
                        throw new Error("");
                    }
                } catch (e) {
                    ps = false;
                }
                return ps;
            };
            let convert;
            if (typeof dataSource === 'string') {
                convert = JSON.parse(dataSource);
            } else {
                convert = dataSource;
            }
            if (Object.prototype.toString.call(convert) === "[object Array]" && convert.length > 0) {
                detectPass = [];
                convert.forEach((value, index, array) => {
                    let tempObj = {}
                    for (let item in value) {
                        switch (String(item).toLowerCase()) {
                            case "typename":
                                switch (String(value[item]).toLowerCase()) {
                                    case "x86_64":
                                        tempObj.TypeName = "x86_64";
                                        break;
                                    case "i386":
                                        tempObj.TypeName = "i386";
                                        break;
                                    case "arm":
                                        tempObj.TypeName = "arm";
                                        break;
                                    case "aarch64":
                                        tempObj.TypeName = "aarch64";
                                        break;
                                    case "mips":
                                        tempObj.TypeName = "mips";
                                        break;
                                    case "mips64":
                                        tempObj.TypeName = "mips64";
                                        break;
                                    case "ppc":
                                        tempObj.TypeName = "ppc";
                                        break;
                                    case "ppc64":
                                        tempObj.TypeName = "ppc64";
                                        break;
                                    default:
                                        throw new Error("");
                                }
                                break;
                            case "listcollect":
                                if (Object.prototype.toString.call(value[item]) === "[object Array]" && value[item].length > 0) {
                                    tempObj.ListCollect = [];
                                    value[item].forEach((v1, i1, a1) => {
                                        let temp1 = {}
                                        for (let item1 in v1) {
                                            switch (String(item1).toLowerCase()) {
                                                case "subtypename":
                                                    if (typeof v1[item1] === 'string') {
                                                        temp1.subTypeName = v1[item1];
                                                    } else {
                                                        throw  new Error("");
                                                    }
                                                    break;
                                                case "sublistcollect":
                                                    temp1.subListCollect = [];
                                                    if (Object.prototype.toString.call(v1[item1]) === '[object Array]' && v1[item1].length > 0) {
                                                        v1[item1].forEach((v2, i2, a2) => {
                                                            let temp2 = {};
                                                            for (let item2 in v2) {
                                                                switch (String(item2).toLowerCase()) {
                                                                    case "osname":
                                                                        if (typeof v2[item2] === 'string') {
                                                                            temp2.osName = v2[item2];
                                                                        } else {
                                                                            throw  new Error("");
                                                                        }
                                                                        break;
                                                                    case "value":
                                                                        if (typeof v2[item2] === 'string') {
                                                                            temp2.value = v2[item2];
                                                                        } else {
                                                                            throw  new Error("");
                                                                        }
                                                                        break;
                                                                    case "preconfig":
                                                                        let cors = PreConfigDetect(v2[item2]);
                                                                        if (cors) {
                                                                            temp2.PreConfig = cors;
                                                                        } else {
                                                                            throw new Error("");
                                                                        }
                                                                        break;
                                                                    default:
                                                                        throw new Error("");
                                                                }
                                                            }
                                                            temp1.subListCollect.push(temp2);
                                                        });
                                                    } else {
                                                        throw new Error("");
                                                    }
                                                    break;
                                                default:
                                                    throw new Error("");
                                            }
                                        }
                                        tempObj.ListCollect.push(temp1);
                                    });
                                } else {
                                    throw new Error("");
                                }
                                break;
                            default:
                                throw new Error("");
                        }
                    }
                    detectPass.push(tempObj);
                });
            } else {
                throw new Error("");
            }
            status = true;
        } catch (e) {
            status = false;
        }
        if (!status) {
            return status;
        } else {
            return detectPass;
        }
    },
    softwareDetectDataCheck(dataSource) {
        let checkStatus;
        let FinalCheck;
        try {
            let convert;
            if (typeof dataSource === 'string') {
                convert = JSON.parse(dataSource);
            } else {
                convert = dataSource;
            }
            if (Object.prototype.toString.call(convert) === "[object Array]" && convert.length > 0) {
                FinalCheck = [];
                convert.forEach((value, index, array) => {
                    if (Object.prototype.toString.call(value) === "[object Object]") {
                        if (Reflect.ownKeys(value).length > 0) {
                            let tmp = {};
                            for (let item in value) {
                                switch (String(item).toLowerCase()) {
                                    case "platformtype":
                                        if (typeof value[item] === 'string') {
                                            tmp.PlatformType = String(value[item]).toString();
                                        } else {
                                            throw new Error("");
                                        }
                                        break;
                                    case "body":
                                        if (Object.prototype.toString.call(value[item]) === "[object Array]" && value[item].length > 0) {
                                            tmp.Body = [];
                                            value[item].forEach((v1, i1, a1) => {
                                                if (Object.prototype.toString.call(v1) === "[object Object]" && Reflect.ownKeys(v1).length > 0) {
                                                    let tmp1 = {}
                                                    for (let ins in v1) {
                                                        switch (String(ins).toLowerCase()) {
                                                            case "path":
                                                                if (Object.prototype.toString.call(v1[ins]) === "[object Array]" && v1[ins].length > 0) {
                                                                    tmp1.path = [];
                                                                    v1[ins].forEach((v2, i2, a2) => {
                                                                        if (typeof v2 === "string") {
                                                                            tmp1.path.push(v2);
                                                                        } else {
                                                                            throw new Error("")
                                                                        }
                                                                    });
                                                                } else {
                                                                    throw new Error("");
                                                                }
                                                                break;
                                                            case "program":
                                                                if (Object.prototype.toString.call(v1[ins]) === "[object Array]" && v1[ins].length > 0) {
                                                                    tmp1.program = [];
                                                                    v1[ins].forEach((v2, i2, a2) => {
                                                                        if (typeof v2 === "string" && String(v2).toLowerCase().includes("qemu-system-")) {
                                                                            tmp1.program.push(v2);
                                                                        } else {
                                                                            throw new Error("");
                                                                        }
                                                                    });
                                                                } else {
                                                                    throw new Error("");
                                                                }
                                                                break;
                                                            default:
                                                                throw new Error("");
                                                        }
                                                    }
                                                    tmp.Body.push(tmp1);
                                                } else {
                                                    throw new Error("")
                                                }
                                            });
                                        } else {
                                            throw new Error("")
                                        }
                                        break;
                                    default:
                                        throw new Error("");
                                }
                            }
                            FinalCheck.push(tmp);
                        } else {
                            throw new Error("");
                        }
                    } else {
                        throw new Error("");
                    }
                });
            } else {
                throw new Error("");
            }
            let existCurOS = false;
            FinalCheck.forEach((value, index, array) => {
                if (value.PlatformType === this.__vm_HostPlatform) {
                    existCurOS = true;
                }
            });
            if (!existCurOS) {
                throw new Error("");
            }
            checkStatus = true;
        } catch (e) {
            checkStatus = false;
        }
        if (!checkStatus) {
            return checkStatus;
        } else {
            return FinalCheck;
        }
    },
    downloadResponseHeaderInfo(FileName, customHeaderLib) {
        let DefineDetect = (Source) => {
            let FinalDetect;
            try {
                if (Object.prototype.toString.call(Source) === "[object Array]") {
                    FinalDetect = [];
                    Source.forEach((value, index, array) => {
                        if (Object.prototype.toString.call(value) === "[object Object]") {
                            let tmp = {};
                            let count = 0;
                            for (let num in value) {
                                switch (String(num).toLowerCase()) {
                                    case "extname":
                                        if (Object.prototype.toString.call(value[num]) === "[object String]") {
                                            tmp.extName = value[num];
                                        } else {
                                            throw new Error("");
                                        }
                                        count++;
                                        break;
                                    case "body":
                                        if (Object.prototype.toString.call(value[num]) === "[object Object]") {
                                            tmp.body = {};
                                            for (let item in value[num]) {
                                                switch (String(item).toLowerCase()) {
                                                    case "content-type":
                                                        tmp.body["Content-Type"] = value[num][item];
                                                        break;
                                                    case "content-disposition":
                                                        tmp.body["Content-Disposition"] = value[num][item];
                                                        break;
                                                    default:
                                                        throw new Error("");
                                                }
                                            }
                                        } else {
                                            throw new Error("");
                                        }
                                        count++;
                                        break;
                                    case "format":
                                        if (Object.prototype.toString.call(value[num]) === "[object String]") {
                                            tmp.format = value[num];
                                        } else {
                                            throw new Error("");
                                        }
                                        count++;
                                        break;
                                    default:
                                        throw new Error("");
                                }
                            }
                            if (count === 3) {
                                FinalDetect.push(tmp);
                            } else {
                                throw new Error("")
                            }
                        } else {
                            throw new Error("");
                        }
                    });
                    return FinalDetect;
                } else {
                    throw new Error("");
                }
            } catch (e) {
                FinalDetect = undefined;
            }
            return FinalDetect;
        }
        let getExtName = (SourceName) => {
            if (String(SourceName).indexOf(".") > -1) {
                let tmp;
                for (let num = String(SourceName).length - 1; num >= 0; num--) {
                    if (String(SourceName).charAt(num) === ".") {
                        tmp = String(SourceName).substr(num + 1);
                        break;
                    }
                }
                return tmp;
            } else {
                return undefined;
            }
        }
        let foundMatch = false;
        let HeaderInfoLib;
        let FinalResponseBody = {
            body: {
                'Content-Type': '',
                'Content-Disposition': 'attachment; filename='
            },
            format: ""
        }
        if (DefineDetect(customHeaderLib)) {
            HeaderInfoLib = DefineDetect(customHeaderLib);
            HeaderInfoLib.forEach((value, index, array) => {
                if (String(value.extName).toLowerCase().includes(String(getExtName(FileName)).toLowerCase())) {
                    foundMatch = true;
                    FinalResponseBody.format = value.format;
                    FinalResponseBody.body["Content-Disposition"] = value.body["Content-Disposition"] + encodeURI(FileName);
                    FinalResponseBody.body["Content-Type"] = value.body["Content-Type"]
                }
            });
        } else {
            HeaderInfoLib = [
                {
                    extName: 'txt,log',
                    body: {
                        'Content-Type': 'text/plain',
                        'Content-Disposition': 'attachment; filename='
                    },
                    format: 'utf8'
                },
                {
                    extName: 'json',
                    body: {
                        'Content-Type': 'application/json',
                        'Content-Disposition': 'attachment; filename='
                    },
                    format: 'utf8'
                },
                {
                    extName: 'reg',
                    body: {
                        'Content-Type': 'text/x-ms-regedit',
                        'Content-Disposition': 'attachment; filename='
                    },
                    format: 'utf8'
                },
                {
                    extName: 'md',
                    body: {
                        'Content-Type': 'text/markdown',
                        'Content-Disposition': 'attachment; filename='
                    },
                    format: 'binary'
                },
                {
                    extName: 'htm,html',
                    body: {
                        'Content-Type': 'text/html',
                        'Content-Disposition': 'attachment; filename='
                    },
                    format: 'utf8'
                },
                {
                    extName: 'xml',
                    body: {
                        'Content-Type': 'text/xml',
                        'Content-Disposition': 'attachment; filename='
                    },
                    format: 'utf8'
                },
                {
                    extName: 'js',
                    body: {
                        'Content-Type': 'text/javascript',
                        'Content-Disposition': 'attachment; filename='
                    },
                    format: 'utf8'
                },
                {
                    extName: 'css',
                    body: {
                        'Content-Type': 'text/css',
                        'Content-Disposition': 'attachment; filename='
                    },
                    format: 'utf8'
                },
                {
                    extName: 'scss',
                    body: {
                        'Content-Type': 'text/x-scss',
                        'Content-Disposition': 'attachment; filename='
                    },
                    format: 'utf8'
                },
                {
                    extName: 'sass',
                    body: {
                        'Content-Type': 'text/x-sass',
                        'Content-Disposition': 'attachment; filename='
                    },
                    format: 'utf8'
                },
                {
                    extName: 'wma',
                    body: {
                        'Content-Type': 'audio/x-ms-wma',
                        'Content-Disposition': 'attachment; filename='
                    },
                    format: 'binary'
                },
                {
                    extName: 'mid',
                    body: {
                        'Content-Type': 'audio/midi',
                        'Content-Disposition': 'attachment; filename='
                    },
                    format: 'binary'
                },
                {
                    extName: 'mp3',
                    body: {
                        'Content-Type': 'audio/mpeg',
                        'Content-Disposition': 'attachment; filename='
                    },
                    format: 'binary'
                },
                {
                    extName: 'wmv',
                    body: {
                        'Content-Type': 'video/x-ms-wmv',
                        'Content-Disposition': 'attachment; filename='
                    },
                    format: 'binary'
                },
                {
                    extName: 'avi',
                    body: {
                        'Content-Type': 'video/x-msvideo',
                        'Content-Disposition': 'attachment; filename='
                    },
                    format: 'binary'
                },
                {
                    extName: 'mp4',
                    body: {
                        'Content-Type': 'video/mp4',
                        'Content-Disposition': 'attachment; filename='
                    },
                    format: 'binary'
                },
                {
                    extName: 'jpg,jpeg',
                    body: {
                        'Content-Type': 'image/jpeg',
                        'Content-Disposition': 'attachment; filename='
                    },
                    format: 'binary'
                },
                {
                    extName: 'png',
                    body: {
                        'Content-Type': 'image/png',
                        'Content-Disposition': 'attachment; filename='
                    },
                    format: 'binary'
                },
                {
                    extName: 'gif',
                    body: {
                        'Content-Type': 'image/gif',
                        'Content-Disposition': 'attachment; filename='
                    },
                    format: 'binary'
                },
                {
                    extName: 'webp',
                    body: {
                        'Content-Type': 'image/webp',
                        'Content-Disposition': 'attachment; filename='
                    },
                    format: 'binary'
                },
                {
                    extName: 'psd',
                    body: {
                        'Content-Type': 'image/vnd.adobe.photoshop',
                        'Content-Disposition': 'attachment; filename='
                    },
                    format: 'binary'
                },
                {
                    extName: 'cur',
                    body: {
                        'Content-Type': 'image/x-win-bitmap',
                        'Content-Disposition': 'attachment; filename='
                    },
                    format: 'binary'
                },
                {
                    extName: 'bmp',
                    body: {
                        'Content-Type': 'image/bmp',
                        'Content-Disposition': 'attachment; filename='
                    },
                    format: 'binary'
                },
                {
                    extName: 'ttf',
                    body: {
                        'Content-Type': 'font/ttf',
                        'Content-Disposition': 'attachment; filename='
                    },
                    format: 'binary'
                },
                {
                    extName: 'woff',
                    body: {
                        'Content-Type': 'font/woff',
                        'Content-Disposition': 'attachment; filename='
                    },
                    format: 'binary'
                },
                {
                    extName: 'woff2',
                    body: {
                        'Content-Type': 'font/woff2',
                        'Content-Disposition': 'attachment; filename='
                    },
                    format: 'binary'
                },
                {
                    extName: 'ttc',
                    body: {
                        'Content-Type': 'font/collection',
                        'Content-Disposition': 'attachment; filename='
                    },
                    format: 'binary'
                },
                {
                    extName: 'pdf',
                    body: {
                        'Content-Type': 'application/pdf',
                        'Content-Disposition': 'attachment; filename='
                    },
                    format: 'binary'
                },
                {
                    extName: 'doc,docx',
                    body: {
                        'Content-Type': 'application/msword',
                        'Content-Disposition': 'attachment; filename='
                    },
                    format: 'binary'
                },
                {
                    extName: 'zip',
                    body: {
                        'Content-Type': 'application/zip',
                        'Content-Disposition': 'attachment; filename='
                    },
                    format: 'binary'
                },
                {
                    extName: '7z',
                    body: {
                        'Content-Type': 'application/x-7z-compressed',
                        'Content-Disposition': 'attachment; filename='
                    },
                    format: 'binary'
                },
                {
                    extName: 'exe',
                    body: {
                        'Content-Type': 'application/x-ms-dos-executable',
                        'Content-Disposition': 'attachment; filename='
                    },
                    format: 'binary'
                },
                {
                    extName: 'jar',
                    body: {
                        'Content-Type': 'application/x-java-archive',
                        'Content-Disposition': 'attachment; filename='
                    },
                    format: 'binary'
                },
                {
                    extName: 'crx',
                    body: {
                        'Content-Type': 'application/x-chrome-extension',
                        'Content-Disposition': 'attachment; filename='
                    },
                    format: 'binary'
                },
                {
                    extName: 'php',
                    body: {
                        'Content-Type': 'application/x-php',
                        'Content-Disposition': 'attachment; filename='
                    },
                    format: 'binary'
                },
                {
                    extName: 'sql',
                    body: {
                        'Content-Type': 'application/sql',
                        'Content-Disposition': 'attachment; filename='
                    },
                    format: 'binary'
                },
                {
                    extName: 'svg',
                    body: {
                        'Content-Type': 'image/svg+xml',
                        'Content-Disposition': 'attachment; filename='
                    },
                    format: 'binary'
                },
                {
                    extName: 'img',
                    body: {
                        'Content-Type': 'application/x-raw-disk-image',
                        'Content-Disposition': 'attachment; filename='
                    },
                    format: 'binary'
                }
            ];
            HeaderInfoLib.forEach((value, index, array) => {
                if (String(value.extName).toLowerCase().includes(String(getExtName(FileName)).toLowerCase())) {
                    foundMatch = true;
                    FinalResponseBody.format = value.format;
                    FinalResponseBody.body["Content-Disposition"] = value.body["Content-Disposition"] + encodeURI(FileName);
                    FinalResponseBody.body["Content-Type"] = value.body["Content-Type"]
                }
            });
        }
        if (!foundMatch) {
            FinalResponseBody.body["Content-Type"] = "application/octet-stream";
            FinalResponseBody.body["Content-Disposition"] = `attachment;filename=${encodeURI(FileName)}`;
            FinalResponseBody.format = "binary";
        }
        return FinalResponseBody;
    },
    importProFileCheck(dataSource, strictMode) {
        if (typeof strictMode !== 'boolean') {
            strictMode = false;
        }
        if (this.__vm_GenTools.DataTypeDetect(dataSource) === "obj") {
            let FinalCheck = {}
            if (this.__vm_GenTools.notEmptyDetect(dataSource)) {
                try {
                    let count = 0;
                    for (let chkItem in dataSource) {
                        switch (String(chkItem).toLowerCase()) {
                            case "name":
                                if (typeof dataSource[chkItem] === 'string') {
                                    FinalCheck.name = dataSource[chkItem];
                                } else {
                                    throw new Error("");
                                }
                                count++;
                                break;
                            case "framework":
                                if (typeof dataSource[chkItem] === 'string') {
                                    FinalCheck.framework = dataSource[chkItem];
                                }
                                count++;
                                break;
                            case "ostype":
                                if (typeof dataSource[chkItem] === 'string') {
                                    FinalCheck.osType = dataSource[chkItem];
                                }
                                /*count++;*/
                                break;
                            case "model":
                                if (typeof dataSource[chkItem] === 'string') {
                                    FinalCheck.model = dataSource[chkItem];
                                }
                                /*count++;*/
                                break;
                            case "rtc":
                                switch (this.__vm_GenTools.DataTypeDetect(dataSource[chkItem])) {
                                    case "obj":
                                        if (this.__vm_GenTools.notEmptyDetect(dataSource[chkItem])) {
                                            FinalCheck.rtc = {}
                                            for (let subItem in dataSource[chkItem]) {
                                                switch (String(subItem).toLowerCase()) {
                                                    case "base":
                                                        FinalCheck.rtc.base = this.__vm_GenTools.lightCopy(dataSource[chkItem][subItem])
                                                        break;
                                                    case "clock":
                                                        if (
                                                            String(dataSource[chkItem][subItem]).toString() === 'host'
                                                            ||
                                                            String(dataSource[chkItem][subItem]).toString() === 'rt'
                                                            ||
                                                            String(dataSource[chkItem][subItem]).toString() === 'vm'
                                                        ) {
                                                            switch (String(dataSource[chkItem][subItem]).toLowerCase()) {
                                                                case "host":
                                                                    FinalCheck.rtc.clock = 'host';
                                                                    break;
                                                                case "rt":
                                                                    FinalCheck.rtc.clock = 'rt';
                                                                    break;
                                                                case "vm":
                                                                    FinalCheck.rtc.clock = 'vm';
                                                                    break;
                                                            }
                                                        } else {
                                                            throw new Error("");
                                                        }
                                                        break;
                                                    case "driftfix":
                                                        if (
                                                            String(dataSource[chkItem][subItem]).toString() === 'none'
                                                            ||
                                                            String(dataSource[chkItem][subItem]).toString() === 'slew'
                                                        ) {
                                                            switch (String(dataSource[chkItem][subItem]).toLowerCase()) {
                                                                case "none":
                                                                    FinalCheck.rtc.driftfix = 'none';
                                                                    break;
                                                                case "slew":
                                                                    FinalCheck.rtc.driftfix = 'slew';
                                                                    break;
                                                            }
                                                        } else {
                                                            throw new Error("")
                                                        }
                                                        break;
                                                    default:
                                                        throw new Error("");
                                                }
                                            }
                                        } else {
                                            throw new Error("");
                                        }
                                        break;
                                    case "str":
                                        FinalCheck.rtc = dataSource[chkItem];
                                        break;
                                    default:
                                        throw new Error("");
                                }
                                break;
                            case "cpu":
                                if (typeof dataSource[chkItem] === 'string') {
                                    FinalCheck.cpu = dataSource[chkItem];
                                }
                                /*count++;*/
                                break;
                            case "gpu":
                                if (typeof dataSource[chkItem] === 'string') {
                                    FinalCheck.gpu = dataSource[chkItem];
                                }
                                /*count++;*/
                                break;
                            case "mem":
                                if (typeof dataSource[chkItem] === 'number' && isFinite(dataSource[chkItem]) && !isNaN(dataSource[chkItem]) && parseInt(dataSource[chkItem]) > 0) {
                                    FinalCheck.mem = parseInt(dataSource[chkItem]);
                                } else {
                                    throw new Error("");
                                }
                                /*count++;*/
                                break;
                            case "monitor":
                                if (this.__vm_GenTools.DataTypeDetect(dataSource[chkItem]) === "obj" && this.__vm_GenTools.notEmptyDetect(dataSource[chkItem])) {
                                    FinalCheck.monitor = {}
                                    for (let subItem in dataSource[chkItem]) {
                                        switch (String(subItem).toLowerCase()) {
                                            case "display":
                                                FinalCheck.monitor.display = this.__vm_GenTools.lightCopy(dataSource[chkItem][subItem]);
                                                break;
                                            case "gl":
                                                FinalCheck.monitor.gl = this.__vm_GenTools.lightCopy(dataSource[chkItem][subItem]);
                                                break;
                                            case "fullscreen":
                                                FinalCheck.monitor.fullscreen = this.__vm_GenTools.lightCopy(dataSource[chkItem][subItem]);
                                                break;
                                            default:
                                                throw new Error("");
                                        }
                                    }
                                } else {
                                    throw new Error("");
                                }
                                break;
                            case "kvm":
                                if (typeof dataSource[chkItem] === 'boolean') {
                                    FinalCheck.kvm = dataSource[chkItem];
                                } else {
                                    FinalCheck.kvm = false;
                                }
                                /*count++;*/
                                break;
                            case "fips":
                                if (typeof dataSource[chkItem] === 'boolean') {
                                    FinalCheck.fips = dataSource[chkItem];
                                } else {
                                    FinalCheck.fips = false;
                                }
                                /*count++;*/
                                break;
                            case "smp":
                                switch (this.__vm_GenTools.DataTypeDetect(dataSource[chkItem])) {
                                    case "num":
                                        if (
                                            parseInt(dataSource[chkItem]) >= 1
                                            &&
                                            isFinite(dataSource[chkItem])
                                            &&
                                            !isNaN(dataSource[chkItem])
                                        ) {
                                            FinalCheck.smp = parseInt(dataSource[chkItem]);
                                        } else {
                                            throw new Error("");
                                        }
                                        break;
                                    case "obj":
                                        if (this.__vm_GenTools.notEmptyDetect(dataSource[chkItem])) {
                                            FinalCheck.smp = {};
                                            for (let subItem in dataSource[chkItem]) {
                                                switch (String(subItem).toLowerCase()) {
                                                    case "clusters":
                                                        if (
                                                            this.__vm_GenTools.DataTypeDetect(dataSource[chkItem][subItem]) === 'num'
                                                            &&
                                                            isFinite(dataSource[chkItem][subItem])
                                                            &&
                                                            !isNaN(dataSource[chkItem][subItem])
                                                            &&
                                                            parseInt(dataSource[chkItem][subItem]) >= 1
                                                        ) {
                                                            FinalCheck.smp.clusters = this.__vm_GenTools.lightCopy(parseInt(dataSource[chkItem][subItem]))
                                                        } else {
                                                            throw new Error("");
                                                        }
                                                        break;
                                                    case "maxcpus":
                                                        if (
                                                            this.__vm_GenTools.DataTypeDetect(dataSource[chkItem][subItem]) === 'num'
                                                            &&
                                                            isFinite(dataSource[chkItem][subItem])
                                                            &&
                                                            !isNaN(dataSource[chkItem][subItem])
                                                            &&
                                                            parseInt(dataSource[chkItem][subItem]) >= 1
                                                        ) {
                                                            FinalCheck.smp.maxcpus = this.__vm_GenTools.lightCopy(parseInt(dataSource[chkItem][subItem]))
                                                        } else {
                                                            throw new Error("");
                                                        }
                                                        break;
                                                    case "sockets":
                                                        if (
                                                            this.__vm_GenTools.DataTypeDetect(dataSource[chkItem][subItem]) === 'num'
                                                            &&
                                                            isFinite(dataSource[chkItem][subItem])
                                                            &&
                                                            !isNaN(dataSource[chkItem][subItem])
                                                            &&
                                                            parseInt(dataSource[chkItem][subItem]) >= 1
                                                        ) {
                                                            FinalCheck.smp.sockets = this.__vm_GenTools.lightCopy(parseInt(dataSource[chkItem][subItem]))
                                                        } else {
                                                            throw new Error("");
                                                        }
                                                        break;
                                                    case "threads":
                                                        if (
                                                            this.__vm_GenTools.DataTypeDetect(dataSource[chkItem][subItem]) === 'num'
                                                            &&
                                                            isFinite(dataSource[chkItem][subItem])
                                                            &&
                                                            !isNaN(dataSource[chkItem][subItem])
                                                            &&
                                                            parseInt(dataSource[chkItem][subItem]) >= 1
                                                        ) {
                                                            FinalCheck.smp.threads = this.__vm_GenTools.lightCopy(parseInt(dataSource[chkItem][subItem]))
                                                        } else {
                                                            throw new Error("");
                                                        }
                                                        break;
                                                    case "cores":
                                                        if (
                                                            this.__vm_GenTools.DataTypeDetect(dataSource[chkItem][subItem]) === 'num'
                                                            &&
                                                            isFinite(dataSource[chkItem][subItem])
                                                            &&
                                                            !isNaN(dataSource[chkItem][subItem])
                                                            &&
                                                            parseInt(dataSource[chkItem][subItem]) >= 1
                                                        ) {
                                                            FinalCheck.smp.cores = this.__vm_GenTools.lightCopy(parseInt(dataSource[chkItem][subItem]))
                                                        } else {
                                                            throw new Error("");
                                                        }
                                                        break;
                                                    case "cpus":
                                                        if (
                                                            this.__vm_GenTools.DataTypeDetect(dataSource[chkItem][subItem]) === 'num'
                                                            &&
                                                            isFinite(dataSource[chkItem][subItem])
                                                            &&
                                                            !isNaN(dataSource[chkItem][subItem])
                                                            &&
                                                            parseInt(dataSource[chkItem][subItem]) >= 1
                                                        ) {
                                                            FinalCheck.smp.cpus = this.__vm_GenTools.lightCopy(parseInt(dataSource[chkItem][subItem]))
                                                        } else {
                                                            throw new Error("");
                                                        }
                                                        break;
                                                    case "dies":
                                                        if (
                                                            this.__vm_GenTools.DataTypeDetect(dataSource[chkItem][subItem]) === 'num'
                                                            &&
                                                            isFinite(dataSource[chkItem][subItem])
                                                            &&
                                                            !isNaN(dataSource[chkItem][subItem])
                                                            &&
                                                            parseInt(dataSource[chkItem][subItem]) >= 1
                                                        ) {
                                                            FinalCheck.smp.dies = this.__vm_GenTools.lightCopy(parseInt(dataSource[chkItem][subItem]))
                                                        } else {
                                                            throw new Error("");
                                                        }
                                                        break;
                                                    default:
                                                        throw new Error("");
                                                }
                                            }
                                        } else {
                                            throw new Error("");
                                        }
                                        break;
                                    default:
                                        throw new Error("");
                                }
                                break;
                            case "sound":
                                if (typeof dataSource[chkItem] === 'string') {
                                    FinalCheck.sound = dataSource[chkItem];
                                }
                                /*count++;*/
                                break;
                            case "usb":
                                if (
                                    this.__vm_GenTools.DataTypeDetect(dataSource[chkItem]) === 'obj'
                                    &&
                                    this.__vm_GenTools.notEmptyDetect(dataSource[chkItem])
                                ) {
                                    FinalCheck.usb = {};
                                    for (let subItem in dataSource[chkItem]) {
                                        switch (String(subItem).toLowerCase()) {
                                            case "usb":
                                                if (typeof dataSource[chkItem][subItem] === 'boolean') {
                                                    FinalCheck.usb.usb = dataSource[chkItem][subItem];
                                                } else {
                                                    FinalCheck.usb.usb = false;
                                                }
                                                break;
                                            case "mouse":
                                                if (typeof dataSource[chkItem][subItem] === 'boolean') {
                                                    FinalCheck.usb.mouse = dataSource[chkItem][subItem];
                                                } else {
                                                    FinalCheck.usb.mouse = false;
                                                }
                                                break;
                                            case "keyboard":
                                                if (typeof dataSource[chkItem][subItem] === 'boolean') {
                                                    FinalCheck.usb.keyboard = dataSource[chkItem][subItem];
                                                } else {
                                                    FinalCheck.usb.keyboard = false;
                                                }
                                                break;
                                            case "tablet":
                                                if (typeof dataSource[chkItem][subItem] === 'boolean') {
                                                    FinalCheck.usb.tablet = dataSource[chkItem][subItem];
                                                } else {
                                                    FinalCheck.usb.tablet = false;
                                                }
                                                break;
                                            case "wacom-tablet":
                                                if (typeof dataSource[chkItem][subItem] === 'boolean') {
                                                    FinalCheck.usb["wacom-tablet"] = dataSource[chkItem][subItem];
                                                } else {
                                                    FinalCheck.usb["wacom-tablet"] = false;
                                                }
                                                break;
                                            case "braille":
                                                if (typeof dataSource[chkItem][subItem] === 'boolean') {
                                                    FinalCheck.usb.braille = dataSource[chkItem][subItem];
                                                } else {
                                                    FinalCheck.usb.braille = false;
                                                }
                                                break;
                                            default:
                                                throw new Error("");
                                        }
                                    }
                                } else {
                                    throw new Error("");
                                }
                                break;
                            case "efi":
                                if (typeof dataSource[chkItem] === 'string') {
                                    FinalCheck.efi = dataSource[chkItem];
                                }
                                /*count++;*/
                                break;
                            case "boot":
                                switch (this.__vm_GenTools.DataTypeDetect(dataSource[chkItem])) {
                                    case "str":
                                        if (
                                            String(dataSource[chkItem]).toLowerCase() === 'a'
                                            ||
                                            String(dataSource[chkItem]).toLowerCase() === 'c'
                                            ||
                                            String(dataSource[chkItem]).toLowerCase() === 'd'
                                            ||
                                            String(dataSource[chkItem]).toLowerCase() === 'n'
                                        ) {
                                            switch (String(dataSource[chkItem]).toLowerCase()) {
                                                case "a":
                                                    FinalCheck.boot = 'a'
                                                    break;
                                                case "c":
                                                    FinalCheck.boot = 'c'
                                                    break;
                                                case "d":
                                                    FinalCheck.boot = 'd'
                                                    break;
                                                case "n":
                                                    FinalCheck.boot = 'n'
                                                    break;
                                            }
                                        } else {
                                            throw new Error("");
                                        }
                                        break;
                                    case "obj":
                                        if (this.__vm_GenTools.notEmptyDetect(dataSource[chkItem])) {
                                            FinalCheck.boot = {}
                                            for (let subItem in dataSource[chkItem]) {
                                                switch (String(subItem).toLowerCase()) {
                                                    case "order":
                                                        if (
                                                            String(dataSource[chkItem][subItem]).toLowerCase() === 'a'
                                                            ||
                                                            String(dataSource[chkItem][subItem]).toLowerCase() === 'c'
                                                            ||
                                                            String(dataSource[chkItem][subItem]).toLowerCase() === 'd'
                                                            ||
                                                            String(dataSource[chkItem][subItem]).toLowerCase() === 'n'
                                                        ) {
                                                            switch (String(dataSource[chkItem]).toLowerCase()) {
                                                                case "a":
                                                                    FinalCheck.boot.order = 'a'
                                                                    break;
                                                                case "c":
                                                                    FinalCheck.boot.order = 'c'
                                                                    break;
                                                                case "d":
                                                                    FinalCheck.boot.order = 'd'
                                                                    break;
                                                                case "n":
                                                                    FinalCheck.boot.order = 'n'
                                                                    break;
                                                            }
                                                        } else {
                                                            FinalCheck.boot.order = 'c'
                                                        }
                                                        break;
                                                    case "splash":
                                                        if (typeof dataSource[chkItem] === "string") {
                                                            if (strictMode) {
                                                                FinalCheck.boot.splash = dataSource[chkItem];
                                                            } else {
                                                                FinalCheck.boot.splash = dataSource[chkItem];
                                                            }
                                                        } else {
                                                            throw new Error("");
                                                        }
                                                        break;
                                                    case "menu":
                                                        if (typeof dataSource[chkItem][subItem] === 'boolean') {
                                                            FinalCheck.boot.menu = dataSource[chkItem][subItem];
                                                        } else {
                                                            FinalCheck.boot.menu = false;
                                                        }
                                                        break;
                                                    case "strict":
                                                        if (typeof dataSource[chkItem][subItem] === 'boolean') {
                                                            FinalCheck.boot.strict = dataSource[chkItem][subItem];
                                                        } else {
                                                            FinalCheck.boot.strict = false;
                                                        }
                                                        break;
                                                    case "splash-time":
                                                        if (
                                                            typeof dataSource[chkItem][subItem] === 'number'
                                                            &&
                                                            parseInt(dataSource[chkItem][subItem]) >= 0
                                                            &&
                                                            isFinite(dataSource[chkItem][subItem])
                                                            &&
                                                            !isNaN(dataSource[chkItem][subItem])
                                                        ) {
                                                            FinalCheck.boot["splash-time"] = parseInt(dataSource[chkItem][subItem]);
                                                        } else {
                                                            throw new Error("");
                                                        }
                                                        break;
                                                    case "reboot-timeout":
                                                        if (
                                                            typeof dataSource[chkItem][subItem] === 'number'
                                                            &&
                                                            parseInt(dataSource[chkItem][subItem]) >= 0
                                                            &&
                                                            isFinite(dataSource[chkItem][subItem])
                                                            &&
                                                            !isNaN(dataSource[chkItem][subItem])
                                                        ) {
                                                            FinalCheck.boot["reboot-timeout"] = parseInt(dataSource[chkItem][subItem]);
                                                        } else {
                                                            throw new Error("");
                                                        }
                                                        break;
                                                    default:
                                                        throw new Error("");
                                                }
                                            }
                                        } else {
                                            throw new Error("");
                                        }
                                        break;
                                }
                                break;
                            case "hardware":
                                if (
                                    this.__vm_GenTools.DataTypeDetect(dataSource[chkItem]) === 'arr'
                                    &&
                                    this.__vm_GenTools.notEmptyDetect(dataSource[chkItem])
                                ) {
                                    FinalCheck.hardware = [];
                                    for (let num in dataSource[chkItem]) {
                                        if (
                                            this.__vm_GenTools.DataTypeDetect(dataSource[chkItem][num]) === 'obj'
                                            &&
                                            this.__vm_GenTools.notEmptyDetect(dataSource[chkItem][num])
                                        ) {
                                            let tmpObj = {};
                                            for (let subItem in dataSource[chkItem][num]) {
                                                switch (String(subItem).toLowerCase()) {
                                                    case "devicename":
                                                        if (typeof dataSource[chkItem][num][subItem] === 'string') {
                                                            tmpObj.DeviceName = dataSource[chkItem][num][subItem];
                                                        } else {
                                                            tmpObj.DeviceName = this.__vm_GenTools.randomStringMakerEx({
                                                                BuilderLength: 7,
                                                                EnableUpperEng: true,
                                                                EnableLowerEng: true,
                                                                EnableEngSymbol: false,
                                                                EnableNumberText: true
                                                            })
                                                        }
                                                        break
                                                    case "devicetype":
                                                        switch (String(dataSource[chkItem][num][subItem]).toLowerCase()) {
                                                            case "fda":
                                                                tmpObj.DeviceType = String(dataSource[chkItem][num][subItem]).toLowerCase();
                                                                break;
                                                            case "fdb":
                                                                tmpObj.DeviceType = String(dataSource[chkItem][num][subItem]).toLowerCase();
                                                                break;
                                                            case "hda":
                                                                tmpObj.DeviceType = String(dataSource[chkItem][num][subItem]).toLowerCase();
                                                                break;
                                                            case "hdb":
                                                                tmpObj.DeviceType = String(dataSource[chkItem][num][subItem]).toLowerCase();
                                                                break;
                                                            case "hdc":
                                                                tmpObj.DeviceType = String(dataSource[chkItem][num][subItem]).toLowerCase();
                                                                break;
                                                            case "hdd":
                                                                tmpObj.DeviceType = String(dataSource[chkItem][num][subItem]).toLowerCase();
                                                                break;
                                                            case "cdrom":
                                                                tmpObj.DeviceType = String(dataSource[chkItem][num][subItem]).toLowerCase();
                                                                break;
                                                            case "drive":
                                                                tmpObj.DeviceType = String(dataSource[chkItem][num][subItem]).toLowerCase();
                                                                break;
                                                            case "device":
                                                                tmpObj.DeviceType = String(dataSource[chkItem][num][subItem]).toLowerCase();
                                                                break;
                                                            case "custom":
                                                                tmpObj.DeviceType = String(dataSource[chkItem][num][subItem]).toLowerCase();
                                                                break;
                                                            default:
                                                                throw new Error("");
                                                        }
                                                        break
                                                    case "operate_item":
                                                        switch (String(dataSource[chkItem][num][subItem]).substr(1).toLowerCase()) {
                                                            case "fda":
                                                                tmpObj.Operate_Item = String(dataSource[chkItem][num][subItem]).toLowerCase();
                                                                break;
                                                            case "fdb":
                                                                tmpObj.Operate_Item = String(dataSource[chkItem][num][subItem]).toLowerCase();
                                                                break;
                                                            case "hda":
                                                                tmpObj.Operate_Item = String(dataSource[chkItem][num][subItem]).toLowerCase();
                                                                break;
                                                            case "hdb":
                                                                tmpObj.Operate_Item = String(dataSource[chkItem][num][subItem]).toLowerCase();
                                                                break;
                                                            case "hdc":
                                                                tmpObj.Operate_Item = String(dataSource[chkItem][num][subItem]).toLowerCase();
                                                                break;
                                                            case "hdd":
                                                                tmpObj.Operate_Item = String(dataSource[chkItem][num][subItem]).toLowerCase();
                                                                break;
                                                            case "cdrom":
                                                                tmpObj.Operate_Item = String(dataSource[chkItem][num][subItem]).toLowerCase();
                                                                break;
                                                            case "drive":
                                                                tmpObj.Operate_Item = String(dataSource[chkItem][num][subItem]).toLowerCase();
                                                                break;
                                                            case "device":
                                                                tmpObj.Operate_Item = String(dataSource[chkItem][num][subItem]).toLowerCase();
                                                                break;
                                                        }
                                                        break
                                                    case "operate_value":
                                                        switch (this.__vm_GenTools.DataTypeDetect(dataSource[chkItem][num][subItem])) {
                                                            case "str":
                                                                tmpObj.Operate_Value = dataSource[chkItem][num][subItem];
                                                                break;
                                                            case "obj":
                                                                if (this.__vm_GenTools.notEmptyDetect(dataSource[chkItem][num][subItem])) {
                                                                    tmpObj.Operate_Value = {};
                                                                    for (let test in dataSource[chkItem][num][subItem]) {
                                                                        switch (String(test).toLowerCase()) {
                                                                            case "media":
                                                                                switch (String(dataSource[chkItem][num][subItem][test]).toLowerCase()) {
                                                                                    case "disk":
                                                                                        tmpObj.Operate_Value.media = "disk";
                                                                                        break;
                                                                                    case "cdrom":
                                                                                        tmpObj.Operate_Value.media = "cdrom";
                                                                                        break;
                                                                                    default:
                                                                                        throw new Error("");
                                                                                }
                                                                                break;
                                                                            case "if":
                                                                                switch (String(dataSource[chkItem][num][subItem][test]).toLowerCase()) {
                                                                                    case "ide":
                                                                                        tmpObj.Operate_Value.if = String(dataSource[chkItem][num][subItem][test]).toLowerCase();
                                                                                        break;
                                                                                    case "scsi":
                                                                                        tmpObj.Operate_Value.if = String(dataSource[chkItem][num][subItem][test]).toLowerCase();
                                                                                        break;
                                                                                    case "sd":
                                                                                        tmpObj.Operate_Value.if = String(dataSource[chkItem][num][subItem][test]).toLowerCase();
                                                                                        break;
                                                                                    case "mtd":
                                                                                        tmpObj.Operate_Value.if = String(dataSource[chkItem][num][subItem][test]).toLowerCase();
                                                                                        break;
                                                                                    case "floppy":
                                                                                        tmpObj.Operate_Value.if = String(dataSource[chkItem][num][subItem][test]).toLowerCase();
                                                                                        break;
                                                                                    case "pflash":
                                                                                        tmpObj.Operate_Value.if = String(dataSource[chkItem][num][subItem][test]).toLowerCase();
                                                                                        break;
                                                                                    case "virtio":
                                                                                        tmpObj.Operate_Value.if = String(dataSource[chkItem][num][subItem][test]).toLowerCase();
                                                                                        break;
                                                                                    case "none":
                                                                                        tmpObj.Operate_Value.if = String(dataSource[chkItem][num][subItem][test]).toLowerCase();
                                                                                        break;
                                                                                    default:
                                                                                        throw new Error("");
                                                                                }
                                                                                break;
                                                                            case "cache":
                                                                                switch (String(dataSource[chkItem][num][subItem][test]).toLowerCase()) {
                                                                                    case "none":
                                                                                        tmpObj.Operate_Value.cache = String(dataSource[chkItem][num][subItem][test]).toLowerCase();
                                                                                        break;
                                                                                    case "writeback":
                                                                                        tmpObj.Operate_Value.cache = String(dataSource[chkItem][num][subItem][test]).toLowerCase();
                                                                                        break;
                                                                                    case "unsafe":
                                                                                        tmpObj.Operate_Value.cache = String(dataSource[chkItem][num][subItem][test]).toLowerCase();
                                                                                        break;
                                                                                    case "directsync":
                                                                                        tmpObj.Operate_Value.cache = String(dataSource[chkItem][num][subItem][test]).toLowerCase();
                                                                                        break;
                                                                                    case "writethrough":
                                                                                        tmpObj.Operate_Value.cache = String(dataSource[chkItem][num][subItem][test]).toLowerCase();
                                                                                        break;
                                                                                    default:
                                                                                        throw new Error("");
                                                                                }
                                                                                break;
                                                                            case "aio":
                                                                                switch (String(dataSource[chkItem][num][subItem][test]).toLowerCase()) {
                                                                                    case "threads":
                                                                                        tmpObj.Operate_Value.aio = String(dataSource[chkItem][num][subItem][test]).toLowerCase();
                                                                                        break;
                                                                                    case "native":
                                                                                        tmpObj.Operate_Value.aio = String(dataSource[chkItem][num][subItem][test]).toLowerCase();
                                                                                        break;
                                                                                    case "io_uring":
                                                                                        tmpObj.Operate_Value.aio = String(dataSource[chkItem][num][subItem][test]).toLowerCase();
                                                                                        break;
                                                                                    default:
                                                                                        throw new Error("");
                                                                                }
                                                                                break;
                                                                            case "format":
                                                                                switch (String(dataSource[chkItem][num][subItem][test]).toLowerCase()) {
                                                                                    case "raw":
                                                                                        tmpObj.Operate_Value.format = String(dataSource[chkItem][num][subItem][test]).toLowerCase();
                                                                                        break;
                                                                                    case "vdi":
                                                                                        tmpObj.Operate_Value.format = String(dataSource[chkItem][num][subItem][test]).toLowerCase();
                                                                                        break;
                                                                                    case "vhdx":
                                                                                        tmpObj.Operate_Value.format = String(dataSource[chkItem][num][subItem][test]).toLowerCase();
                                                                                        break;
                                                                                    case "vmdk":
                                                                                        tmpObj.Operate_Value.format = String(dataSource[chkItem][num][subItem][test]).toLowerCase();
                                                                                        break;
                                                                                    default:
                                                                                        throw new Error("");
                                                                                }
                                                                                break;
                                                                            case "index":
                                                                                if (
                                                                                    typeof dataSource[chkItem][num][subItem][test] === 'number'
                                                                                    &&
                                                                                    parseInt(dataSource[chkItem][num][subItem][test]) >= 0
                                                                                    &&
                                                                                    isFinite(dataSource[chkItem][num][subItem][test])
                                                                                    &&
                                                                                    !isNaN(dataSource[chkItem][num][subItem][test])
                                                                                ) {
                                                                                    tmpObj.Operate_Value.index = parseInt(dataSource[chkItem][num][subItem][test]);
                                                                                } else {
                                                                                    throw new Error("");
                                                                                }
                                                                                break;
                                                                            case "id":
                                                                                switch (typeof dataSource[chkItem][num][subItem][test]) {
                                                                                    case "number":
                                                                                        if (
                                                                                            parseInt(dataSource[chkItem][num][subItem][test]) >= 0
                                                                                            &&
                                                                                            isFinite(dataSource[chkItem][num][subItem][test])
                                                                                            &&
                                                                                            !isNaN(dataSource[chkItem][num][subItem][test])
                                                                                        ) {
                                                                                            tmpObj.Operate_Value.id = parseInt(dataSource[chkItem][num][subItem][test]);
                                                                                        } else {
                                                                                            throw new Error("");
                                                                                        }
                                                                                        break;
                                                                                    case "string":
                                                                                        tmpObj.Operate_Value.id = dataSource[chkItem][num][subItem][test];
                                                                                        break;
                                                                                    default:
                                                                                        throw new Error("");
                                                                                }
                                                                                break;
                                                                            case "file":
                                                                                if (typeof dataSource[chkItem][num][subItem][test]) {
                                                                                    if (strictMode) {
                                                                                        tmpObj.Operate_Value.file = dataSource[chkItem][num][subItem][test];
                                                                                    } else {
                                                                                        tmpObj.Operate_Value.file = dataSource[chkItem][num][subItem][test];
                                                                                    }
                                                                                } else {
                                                                                    throw new Error("");
                                                                                }
                                                                                break;
                                                                            default:
                                                                                throw new Error("");
                                                                        }
                                                                    }
                                                                } else {
                                                                    throw new Error("");
                                                                }
                                                                break;
                                                            default:
                                                                throw new Error("");
                                                        }
                                                        break
                                                    case "extra_param":
                                                        if (typeof dataSource[chkItem][num][subItem] === 'string') {
                                                            tmpObj.Extra_Param = dataSource[chkItem][num][subItem];
                                                        } else {
                                                            throw new Error("");
                                                        }
                                                        break;
                                                    default:
                                                        throw new Error("");
                                                }
                                            }
                                            if (tmpObj.DeviceType === 'custom') {
                                                FinalCheck.hardware.push(tmpObj);
                                            } else {
                                                switch (tmpObj.DeviceType) {
                                                    case "drive":
                                                        if (this.__vm_GenTools.DataTypeDetect(tmpObj.Operate_Value) !== 'obj') {
                                                            throw new Error("");
                                                        }
                                                        break;
                                                    default:
                                                        if (!String(tmpObj.Operate_Item).includes(tmpObj.DeviceType)) {
                                                            throw new Error("");
                                                        }
                                                        break;
                                                }
                                                FinalCheck.hardware.push(tmpObj);
                                            }
                                        } else {
                                            throw new Error("");
                                        }
                                    }
                                } else {
                                    throw new Error("");
                                }
                                break;
                            case "id":
                                if (typeof dataSource[chkItem] === 'string') {
                                    FinalCheck.id = dataSource[chkItem];
                                }
                                count++;
                                break;
                            case "vts":
                                switch (this.__vm_GenTools.DataTypeDetect(dataSource[chkItem])) {
                                    case "str":
                                        FinalCheck.vts = this.__vm_GenTools.lightCopy(dataSource[chkItem]);
                                        break;
                                    case "obj":
                                        if (this.__vm_GenTools.notEmptyDetect(dataSource[chkItem])) {
                                            FinalCheck.vts = {}
                                            for (let subItem in dataSource[chkItem]) {
                                                switch (String(subItem).toLowerCase()) {
                                                    case "accel":
                                                        FinalCheck.vts.accel = this.__vm_GenTools.lightCopy(dataSource[chkItem][subItem]);
                                                        break;
                                                    case "igd-passthru":
                                                        if (
                                                            String(dataSource[chkItem][subItem]).toLowerCase() === "on"
                                                            ||
                                                            String(dataSource[chkItem][subItem]).toLowerCase() === "off"
                                                        ) {
                                                            FinalCheck.vts["igd-passthru"] = this.__vm_GenTools.lightCopy(String(dataSource[chkItem][subItem]).toLowerCase());
                                                        } else {
                                                            throw new Error("");
                                                        }
                                                        break;
                                                    case "kernel-irqchip":
                                                        if (
                                                            String(dataSource[chkItem][subItem]).toLowerCase() === "on"
                                                            ||
                                                            String(dataSource[chkItem][subItem]).toLowerCase() === "off"
                                                            ||
                                                            String(dataSource[chkItem][subItem]).toLowerCase() === "split"
                                                        ) {
                                                            FinalCheck.vts["kernel-irqchip"] = this.__vm_GenTools.lightCopy(String(dataSource[chkItem][subItem]).toLowerCase());
                                                        } else {
                                                            throw new Error("");
                                                        }
                                                        break;
                                                    case "split-wx":
                                                        if (
                                                            String(dataSource[chkItem][subItem]).toLowerCase() === "on"
                                                            ||
                                                            String(dataSource[chkItem][subItem]).toLowerCase() === "off"
                                                        ) {
                                                            FinalCheck.vts["split-wx"] = this.__vm_GenTools.lightCopy(String(dataSource[chkItem][subItem]).toLowerCase());
                                                        } else {
                                                            throw new Error("");
                                                        }
                                                        break;
                                                    case "kvm-shadow-mem":
                                                        if (
                                                            this.__vm_GenTools.DataTypeDetect(dataSource[chkItem][subItem]) === "num"
                                                            &&
                                                            isFinite(dataSource[chkItem][subItem])
                                                            &&
                                                            !isNaN(dataSource[chkItem][subItem])
                                                            &&
                                                            parseInt(dataSource[chkItem][subItem]) >= 0
                                                        ) {
                                                            FinalCheck.vts["kvm-shadow-mem"] = parseInt(dataSource[chkItem][subItem]);
                                                        } else {
                                                            throw new Error("");
                                                        }
                                                        break;
                                                    case "tb-size":
                                                        if (
                                                            this.__vm_GenTools.DataTypeDetect(dataSource[chkItem][subItem]) === "num"
                                                            &&
                                                            isFinite(dataSource[chkItem][subItem])
                                                            &&
                                                            !isNaN(dataSource[chkItem][subItem])
                                                            &&
                                                            parseInt(dataSource[chkItem][subItem]) >= 0
                                                        ) {
                                                            FinalCheck.vts["tb-size"] = parseInt(dataSource[chkItem][subItem]);
                                                        } else {
                                                            throw new Error("");
                                                        }
                                                        break;
                                                    case "dirty-ring-size":
                                                        if (
                                                            this.__vm_GenTools.DataTypeDetect(dataSource[chkItem][subItem]) === "num"
                                                            &&
                                                            isFinite(dataSource[chkItem][subItem])
                                                            &&
                                                            !isNaN(dataSource[chkItem][subItem])
                                                            &&
                                                            parseInt(dataSource[chkItem][subItem]) >= 0
                                                        ) {
                                                            FinalCheck.vts["dirty-ring-size"] = parseInt(dataSource[chkItem][subItem]);
                                                        } else {
                                                            throw new Error("");
                                                        }
                                                        break;
                                                    case "thread":
                                                        if (
                                                            String(dataSource[chkItem][subItem]).toLowerCase() === "single"
                                                            ||
                                                            String(dataSource[chkItem][subItem]).toLowerCase() === "multi"
                                                        ) {
                                                            FinalCheck.vts.thread = this.__vm_GenTools.lightCopy(String(dataSource[chkItem][subItem]).toLowerCase());
                                                        } else {
                                                            throw new Error("");
                                                        }
                                                        break;
                                                    default:
                                                        throw new Error("");
                                                }
                                            }
                                        } else {
                                            throw new Error("");
                                        }
                                        break;
                                }
                                break;
                            default:
                                throw new Error("");
                        }
                    }
                    if (count < 2) {
                        throw new Error("");
                    }
                    return FinalCheck;
                } catch (e) {
                    return undefined
                }
            } else {
                return undefined
            }
        } else {
            return undefined
        }
    }
}