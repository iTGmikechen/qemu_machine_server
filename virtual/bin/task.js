module.exports = {
    FirstInitial() {
        try {
            console.log("开始初始化系统");
            let detectSoftware = 0;

            /**型号、CPU、GPU、屏幕、声卡、设备、加速、版本*/
            let ExtraCmd = [
                "-M help",
                "-cpu help",
                "-vga help",
                "-display help",
                "-soundhw help",
                "-device help",
                "-accel help",
                "-version"
            ];
            /**UEFI 固件*/
            let UEFI = {
                ARM32: [
                    ["firmware", "Origin_ARM32.fd"],
                ],
                ARM64: [
                    ["firmware", "Origin_ARM64.fd"],
                    ["firmware", "Extra_ARM64.img"],
                ],
                X32: [
                    ["firmware", "Origin_x32.fd"],
                ],
                X64: [
                    ["firmware", "Origin_x64.fd"],
                ]
            };

            let joinSplit;
            switch (this.__vm_HostPlatform) {
                case "linux":
                    joinSplit = "/";
                    /**linux 系统检测列表*/
                    let linuxDetectList;
                    this.__vm_softwareDetectList.forEach((value, index, array) => {
                        if (value.PlatformType === 'linux') {
                            linuxDetectList = value.Body;
                        }
                    });
                    linuxDetectList.forEach((v1, i1, a1) => {
                        v1.path.forEach((v2, i2, a2) => {
                            v1.program.forEach((v3, i3, a3) => {
                                let splitData = String(this.__vm_GenTools.PathStream.join(v2, joinSplit, v3)).split("-");
                                let infoObject = new this.__infoObject();
                                infoObject.FrameworkType = splitData[splitData.length - 1];
                                infoObject.ProgramPath = this.__vm_GenTools.PathStream.join(v2, joinSplit, v3);
                                this.__vm_framework.push(infoObject);
                                detectSoftware++;
                            });
                        });
                    });
                    break;
                case "win32":
                    joinSplit = "\\";
                    /**windows 系统检测列表*/
                    let win32DetectList;
                    this.__vm_softwareDetectList.forEach((value, index, array) => {
                        if (value.PlatformType === 'win32') {
                            win32DetectList = value.Body;
                        }
                    });
                    win32DetectList.forEach((v1, i1, a1) => {
                        v1.path.forEach((v2, i2, a2) => {
                            v1.program.forEach((v3, i3, a3) => {
                                let splitData = String(this.__vm_GenTools.PathStream.join(v2, v3)).split("-");
                                let infoObject = new this.__infoObject();
                                infoObject.FrameworkType = String(splitData[splitData.length - 1]).split(".")[0];
                                infoObject.ProgramPath = this.__vm_GenTools.PathStream.join(v2, v3);
                                this.__vm_framework.push(infoObject);
                                detectSoftware++;
                            });
                        });
                    });
                    break;
            }

            if (detectSoftware === 0) {
                throw new Error("未检测到QEMU虚拟机软件，无法完成初始化！");
            }
            this.__vm_framework.forEach((value, index, array) => {
                ExtraCmd.forEach((value1, index1, array1) => {
                    try {
                        let OriginData;
                        let ProcData;
                        let tc;
                        let pathProc;
                        let spSym;
                        if (String(value.FrameworkType).toLowerCase().includes("arm") === false && String(value.FrameworkType).toLowerCase().includes("aarch64") === false) {
                            switch (this.__vm_HostPlatform) {
                                case "linux":
                                    spSym = "\n";
                                    pathProc = `${value.ProgramPath} ${value1}`
                                    break;
                                case "win32":
                                    pathProc = `\"${value.ProgramPath}\" ${value1}`
                                    spSym = "\r\n";
                                    break;
                            }
                            OriginData = this.__ExtraChildSystem.execSync(pathProc);
                            tc = OriginData.toString("utf-8");
                            ProcData = String(tc).split(spSym);
                        }
                        switch (index1) {
                            case 0:
                                if (String(value.FrameworkType).toLowerCase().includes("arm") || String(value.FrameworkType).toLowerCase().includes("aarch64")) {
                                    switch (this.__vm_HostPlatform) {
                                        case "linux":
                                            spSym = "\n";
                                            pathProc = `${value.ProgramPath} ${value1}`
                                            break;
                                        case "win32":
                                            spSym = "\r\n";
                                            pathProc = `\"${value.ProgramPath}\" ${value1}`
                                            break;
                                    }
                                    OriginData = this.__ExtraChildSystem.execSync(pathProc);
                                    tc = OriginData.toString("utf-8");
                                    ProcData = String(tc).split(spSym);
                                }
                                ProcData.shift();
                                ProcData.pop();
                                ProcData.forEach((v0, i0, a0) => {
                                    if (String(v0).indexOf(" ") > -1) {
                                        let machineType = String(v0).split(" ");
                                        let nulCount = 0;
                                        let nulCount1 = 0;
                                        while (true) {
                                            if (String(machineType[nulCount1]).toString() === "") {
                                                nulCount++;
                                            }
                                            if (nulCount1 >= machineType.length - 1) {
                                                break;
                                            }
                                            nulCount1++;
                                        }
                                        let pd = 0;
                                        while (true) {
                                            for (let num in machineType) {
                                                if (String(machineType[num]).toString() === "") {
                                                    machineType.splice(parseInt(num), 1);
                                                    pd++;
                                                    break;
                                                }
                                            }
                                            if (pd >= nulCount) {
                                                break;
                                            }
                                        }
                                        let hostModel = {
                                            type: machineType[0],
                                            desc: undefined,
                                        };
                                        machineType.shift();
                                        hostModel.desc = machineType.join(" ");
                                        array[index].HostType.push(hostModel);
                                    }
                                });
                                break;
                            case 1:
                                if (String(value.FrameworkType).toLowerCase().includes("arm") || String(value.FrameworkType).toLowerCase().includes("aarch64")) {
                                    switch (this.__vm_HostPlatform) {
                                        case "linux":
                                            spSym = "\n";
                                            pathProc = `${value.ProgramPath} ${value1}`
                                            break;
                                        case "win32":
                                            spSym = "\r\n";
                                            pathProc = `\"${value.ProgramPath}\" ${value1}`
                                            break;
                                    }
                                    OriginData = this.__ExtraChildSystem.execSync(pathProc);
                                    tc = OriginData.toString("utf-8");
                                    ProcData = String(tc).split(spSym);
                                }
                                if (String(value.FrameworkType).toLowerCase().includes("mips")) {
                                    ProcData.pop();
                                    ProcData.forEach((v1, i1, a1) => {
                                        let CPUModel = {
                                            type: undefined,
                                            desc: undefined,
                                        };
                                        if (String(v1).indexOf(" ") > -1) {
                                            let CPUType = String(v1).split(" ");
                                            let gen = Array(...CPUType[1]);
                                            gen.shift();
                                            gen.pop();
                                            CPUModel.type = gen.join("");
                                            CPUModel.desc = gen.join("");
                                            array[index].CPUType.push(CPUModel);
                                        }
                                    });
                                } else {
                                    ProcData.shift();
                                    ProcData.forEach((v1, i1, a1) => {
                                        if (v1 === "") {
                                            a1.splice(i1);
                                        }
                                    });
                                    ProcData.forEach((v1, i1, a1) => {
                                        let CPUModel = {
                                            type: undefined,
                                            desc: undefined,
                                        };
                                        if (String(v1).indexOf("  ") > -1) {
                                            let CPUType = String(v1).split("  ");
                                            let nulCount = 0;
                                            let nulCount1 = 0;
                                            while (true) {
                                                if (String(CPUType[nulCount1]).toString() === "") {
                                                    nulCount++;
                                                }
                                                if (nulCount1 >= CPUType.length - 1) {
                                                    break;
                                                }
                                                nulCount1++;
                                            }
                                            let pd = 0;
                                            while (true) {
                                                for (let num in CPUType) {
                                                    if (String(CPUType[num]).toString() === "") {
                                                        CPUType.splice(parseInt(num), 1);
                                                        pd++;
                                                        break;
                                                    }
                                                }
                                                if (pd >= nulCount) {
                                                    break;
                                                }
                                            }
                                            if (CPUType.length > 1) {
                                                CPUModel.type = String(CPUType[0]).split(" ")[1];
                                                let tmp1 = String(CPUType[1]).split(" ");
                                                if (tmp1[0] === "undefined") {
                                                    tmp1[0] = "";
                                                }
                                                let tmp1_1 = 0;
                                                let tmp1_2 = 0;
                                                tmp1.forEach((e, d, c) => {
                                                    if (e === "") {
                                                        tmp1_1++;
                                                    }
                                                });
                                                while (true) {
                                                    tmp1.forEach((e, d, c) => {
                                                        if (e === "") {
                                                            c.splice(d, 1);
                                                            tmp1_2++;
                                                        }
                                                    });
                                                    if (tmp1_2 >= tmp1_1) {
                                                        break;
                                                    }
                                                }
                                                if (tmp1.length > 0) {
                                                    let OriginDesc = tmp1.join(" ");
                                                    if (String(OriginDesc).charAt(0) === "(") {
                                                        OriginDesc = CPUModel.type + " " + OriginDesc;
                                                        CPUModel.desc = OriginDesc;
                                                    } else {
                                                        CPUModel.desc = tmp1.join(" ");
                                                    }
                                                } else {
                                                    CPUModel.desc = CPUModel.type;
                                                }
                                            } else {
                                                if (String(CPUType[0]).split(" ")[1] === undefined) {
                                                    CPUModel.type = CPUType[0];
                                                } else {
                                                    CPUModel.type = String(CPUType[0]).split(" ")[1];
                                                }
                                                CPUModel.desc = CPUType[0];
                                            }
                                            array[index].CPUType.push(CPUModel);
                                        }
                                    });
                                }
                                break;
                            case 2:
                                if (String(value.FrameworkType).toLowerCase().includes("arm") || String(value.FrameworkType).toLowerCase().includes("aarch64")) {
                                    switch (this.__vm_HostPlatform) {
                                        case "linux":
                                            spSym = "\n";
                                            pathProc = `${value.ProgramPath} -M virt ${value1}`
                                            break;
                                        case "win32":
                                            spSym = "\r\n";
                                            pathProc = `\"${value.ProgramPath}\" -M virt ${value1}`
                                            break;
                                    }
                                    OriginData = this.__ExtraChildSystem.execSync(pathProc);
                                    tc = OriginData.toString("utf-8");
                                    ProcData = String(tc).split(spSym);
                                }
                                ProcData.pop();
                                ProcData.forEach((v, i, a) => {
                                    if (String(v).indexOf(" ") > -1) {
                                        let GPUType = String(v).split(" ");
                                        let nulCount = 0;
                                        let nulCount1 = 0;
                                        while (true) {
                                            if (String(GPUType[nulCount1]).toString() === "") {
                                                nulCount++;
                                            }
                                            if (nulCount1 >= GPUType.length - 1) {
                                                break;
                                            }
                                            nulCount1++;
                                        }
                                        let pd = 0;
                                        while (true) {
                                            for (let num in GPUType) {
                                                if (String(GPUType[num]).toString() === "") {
                                                    GPUType.splice(parseInt(num), 1);
                                                    pd++;
                                                    break;
                                                }
                                            }
                                            if (pd >= nulCount) {
                                                break;
                                            }
                                        }
                                        let GPUModel = {
                                            type: GPUType[0],
                                            desc: undefined,
                                        };
                                        GPUType.shift();
                                        GPUModel.desc = GPUType.join(" ");
                                        array[index].GPUType.push(GPUModel);
                                    }
                                });
                                break;
                            case 3:
                                switch (this.__vm_HostPlatform) {
                                    case "linux":
                                        spSym = "\n";
                                        pathProc = `${value.ProgramPath} ${value1}`
                                        break;
                                    case "win32":
                                        spSym = "\r\n";
                                        pathProc = `\"${value.ProgramPath}\" ${value1}`
                                        break;
                                }
                                OriginData = this.__ExtraChildSystem.execSync(pathProc);
                                tc = OriginData.toString("utf-8");
                                ProcData = String(tc).split(spSym);
                                ProcData.shift();
                                ProcData.pop();
                                array[index].Monitor = ProcData;
                                break
                            case 4:
                                switch (this.__vm_HostPlatform) {
                                    case "linux":
                                        spSym = "\n";
                                        pathProc = `${value.ProgramPath} ${value1}`
                                        break;
                                    case "win32":
                                        spSym = "\r\n";
                                        pathProc = `\"${value.ProgramPath}\" ${value1}`
                                        break;
                                }
                                OriginData = this.__ExtraChildSystem.execSync(pathProc);
                                tc = OriginData.toString("utf-8");
                                ProcData = String(tc).split(spSym);
                                ProcData.shift();
                                ProcData.pop();
                                ProcData.pop();
                                ProcData.pop();
                                ProcData.forEach((v4, i4, a4) => {
                                    if (String(v4).indexOf(" ") > -1) {
                                        let SoundCardType = String(v4).split(" ");
                                        let nulCount = 0;
                                        let nulCount1 = 0;
                                        while (true) {
                                            if (String(SoundCardType[nulCount1]).toString() === "") {
                                                nulCount++;
                                            }
                                            if (nulCount1 >= SoundCardType.length - 1) {
                                                break;
                                            }
                                            nulCount1++;
                                        }
                                        let pd = 0;
                                        while (true) {
                                            for (let num in SoundCardType) {
                                                if (String(SoundCardType[num]).toString() === "") {
                                                    SoundCardType.splice(parseInt(num), 1);
                                                    pd++;
                                                    break;
                                                }
                                            }
                                            if (pd >= nulCount) {
                                                break;
                                            }
                                        }
                                        let SoundModel = {type: SoundCardType[0], desc: undefined};
                                        SoundCardType.shift();
                                        SoundModel.desc = SoundCardType.join(" ");
                                        array[index].SoundHW.push(SoundModel);
                                    }
                                });
                                array[index].SoundHW.push({type: "all", desc: "Enable All SoundCard"});
                                break
                            case 5:
                                if (String(value.FrameworkType).toLowerCase().includes("arm") || String(value.FrameworkType).toLowerCase().includes("aarch64")) {
                                    switch (this.__vm_HostPlatform) {
                                        case "linux":
                                            spSym = "\n";
                                            pathProc = `${value.ProgramPath} ${value1}`
                                            break;
                                        case "win32":
                                            spSym = "\r\n";
                                            pathProc = `\"${value.ProgramPath}\" ${value1}`
                                            break;
                                    }
                                    OriginData = this.__ExtraChildSystem.execSync(pathProc);
                                    tc = OriginData.toString("utf-8");
                                    ProcData = String(tc).split(spSym);
                                }
                                ProcData.pop();
                                while (true) {
                                    try {
                                        let tempData = [];
                                        let ListBody = {label: "", itemData: []}
                                        ProcData.forEach((v5, i5, a5) => {
                                            if (v5 === "" || i5 === ProcData.length - 1) {
                                                if (tempData.length > 0) {
                                                    ListBody.label = String(tempData[0]).substr(0, String(tempData[0]).length - 1);
                                                    tempData.shift();
                                                    if (i5 === ProcData.length - 1) {
                                                        tempData.push(v5);
                                                    }
                                                    tempData.forEach((v5_1, i5_1, a5_1) => {
                                                        let symCount = 0;
                                                        symCount = this.__vm_GenTools.findMultiEqualString(v5_1, ", ", true, 0, 0)
                                                        let tgp = new this.__devObject();
                                                        let ges;
                                                        let syb;
                                                        if (symCount > 0) {
                                                            ges = String(v5_1).split(", ");
                                                            while (true) {
                                                                try {
                                                                    ges.forEach((v5_1_1, i5_1_1, a5_1_1) => {
                                                                        let dge = String(v5_1_1).split(" ");
                                                                        syb = this.__vm_GenTools.findMultiEqualString(v5_1_1, "\"", true, 0, 0);
                                                                        if (dge.length > 2) {
                                                                            let tpd = dge[0];
                                                                            dge.shift();
                                                                            if (this.__vm_GenTools.DataTypeDetect(a5_1_1[i5_1_1 + 1]) === "str") {
                                                                                if (String(v5_1).toString().includes("HDA Audio Codec")) {
                                                                                    a5_1_1.shift();
                                                                                    let dgu = a5_1_1.join(", ");
                                                                                    dge.push(dgu);
                                                                                    a5_1_1.splice(0);
                                                                                } else {
                                                                                    dge.push(a5_1_1[i5_1_1 + 1]);
                                                                                    a5_1_1.pop();
                                                                                }
                                                                            }
                                                                            let tps = dge.join(" ");
                                                                            syb = this.__vm_GenTools.findMultiEqualString(tps, "\"", true, 0, 0);
                                                                            if (syb === 2) {
                                                                                tgp[String(tpd).toLowerCase()] = String(tps).substr(1, String(tps).length - 2);
                                                                            } else {
                                                                                tgp[String(tpd).toLowerCase()] = String(tps).toString();
                                                                            }
                                                                        } else {
                                                                            if (syb === 2) {
                                                                                tgp[String(dge[0]).toLowerCase()] = String(dge[1]).substr(1, String(dge[1]).length - 2);
                                                                            } else {
                                                                                tgp[String(dge[0]).toLowerCase()] = String(dge[1]).toString();
                                                                            }
                                                                        }
                                                                        a5_1_1.shift();
                                                                        throw new Error("");
                                                                    });
                                                                } catch (e) {

                                                                }
                                                                if (ges.length <= 0) {
                                                                    break;
                                                                }
                                                            }
                                                            ListBody.itemData.push(tgp);
                                                        } else {
                                                            ges = String(v5_1).split(" ");
                                                            syb = this.__vm_GenTools.findMultiEqualString(ges[1], "\"", true, 0, 0);
                                                            if (syb === 2) {
                                                                tgp[String(ges[0]).toLowerCase()] = String(ges[1]).substr(1, String(ges[1]).length - 2);
                                                            } else {
                                                                tgp[String(ges[0]).toLowerCase()] = String(ges[1]).toString();
                                                            }
                                                            ListBody.itemData.push(tgp);
                                                        }
                                                    });
                                                    array[index].DeviceList.push(ListBody);
                                                    tempData.splice(0);
                                                    if (i5 + 1 === ProcData.length) {
                                                        a5.splice(0, i5 + 1);
                                                    } else {
                                                        a5.splice(0, i5);
                                                    }
                                                    throw new Error("");
                                                } else {
                                                    ProcData.shift();
                                                    throw new Error("");
                                                }
                                            } else {
                                                tempData.push(v5);
                                            }
                                        });
                                    } catch (e) {

                                    }
                                    if (ProcData.length <= 0) {
                                        break;
                                    }
                                }
                                break;
                            case 6:
                                if (String(value.FrameworkType).toLowerCase().includes("arm") || String(value.FrameworkType).toLowerCase().includes("aarch64")) {
                                    switch (this.__vm_HostPlatform) {
                                        case "linux":
                                            spSym = "\n";
                                            pathProc = `${value.ProgramPath} ${value1}`
                                            break;
                                        case "win32":
                                            spSym = "\r\n";
                                            pathProc = `\"${value.ProgramPath}\" ${value1}`
                                            break;
                                    }
                                    OriginData = this.__ExtraChildSystem.execSync(pathProc);
                                    tc = OriginData.toString("utf-8");
                                    ProcData = String(tc).split(spSym);
                                }
                                ProcData.shift();
                                ProcData.pop();
                                array[index].AcceleratorsSupport.push(...ProcData);
                                break;
                            case 7:
                                if (String(value.FrameworkType).toLowerCase().includes("arm") || String(value.FrameworkType).toLowerCase().includes("aarch64")) {
                                    switch (this.__vm_HostPlatform) {
                                        case "linux":
                                            pathProc = `${value.ProgramPath} ${value1}`
                                            break;
                                        case "win32":
                                            pathProc = `\"${value.ProgramPath}\" ${value1}`
                                            break;
                                    }
                                    OriginData = this.__ExtraChildSystem.execSync(pathProc);
                                    tc = OriginData.toString("utf-8");
                                }
                                let tmp;
                                switch (this.__vm_HostPlatform) {
                                    case "linux":
                                        tmp = String(tc).split("\n");
                                        break;
                                    case "win32":
                                        tmp = String(tc).split("\r\n");
                                        break;
                                }
                                tmp.pop();
                                switch (this.__vm_HostPlatform) {
                                    case "linux":
                                        array[index].version = tmp.join("\n");
                                        break;
                                    case "win32":
                                        array[index].version = tmp.join("\r\n");
                                        break;
                                }
                                break;
                        }
                    } catch (e) {
                        e.message = "";
                    }
                });
                /**UEFI*/
                let typeSelectProc;
                if (String(value.FrameworkType).indexOf(".") >= 0) {
                    typeSelectProc = String(value.FrameworkType).split(".")[0];
                } else {
                    typeSelectProc = String(value.FrameworkType).toString();
                }
                switch (String(typeSelectProc).toLowerCase()) {
                    case "x86_64":
                        UEFI.X64.forEach((value2, index2, array2) => {
                            let FullPath = this.__vm_GenTools.PathStream.join(this.__vm_ProgramPath, value2.join(joinSplit).toString());
                            if (this.__vm_GenTools.FileStream.existsSync(FullPath)) {
                                let UEFI_Object = new this.__uefiObject();
                                UEFI_Object.firmwareName = value2[value2.length - 1].split(".")[0];
                                UEFI_Object.firmwarePath = FullPath;
                                array[index].UEFI_BIOS.push(UEFI_Object);
                            }
                        });
                        UEFI.X32.forEach((value2, index2, array2) => {
                            let FullPath = this.__vm_GenTools.PathStream.join(this.__vm_ProgramPath, value2.join(joinSplit).toString());
                            if (this.__vm_GenTools.FileStream.existsSync(FullPath)) {
                                let UEFI_Object = new this.__uefiObject();
                                UEFI_Object.firmwareName = value2[value2.length - 1].split(".")[0];
                                UEFI_Object.firmwarePath = FullPath;
                                array[index].UEFI_BIOS.push(UEFI_Object);
                            }
                        });
                        break;
                    case "i386":
                        UEFI.X32.forEach((value2, index2, array2) => {
                            let FullPath = this.__vm_GenTools.PathStream.join(this.__vm_ProgramPath, value2.join(joinSplit).toString());
                            if (this.__vm_GenTools.FileStream.existsSync(FullPath)) {
                                let UEFI_Object = new this.__uefiObject();
                                UEFI_Object.firmwareName = value2[value2.length - 1].split(".")[0];
                                UEFI_Object.firmwarePath = FullPath;
                                array[index].UEFI_BIOS.push(UEFI_Object);
                            }
                        });
                        break;
                    case "arm":
                        UEFI.ARM32.forEach((value2, index2, array2) => {
                            let FullPath = this.__vm_GenTools.PathStream.join(this.__vm_ProgramPath, value2.join(joinSplit).toString());
                            if (this.__vm_GenTools.FileStream.existsSync(FullPath)) {
                                let UEFI_Object = new this.__uefiObject();
                                UEFI_Object.firmwareName = value2[value2.length - 1].split(".")[0];
                                UEFI_Object.firmwarePath = FullPath;
                                array[index].UEFI_BIOS.push(UEFI_Object);
                            }
                        });
                        break;
                    case "aarch64":
                        UEFI.ARM64.forEach((value2, index2, array2) => {
                            let FullPath = this.__vm_GenTools.PathStream.join(this.__vm_ProgramPath, value2.join(joinSplit).toString());
                            if (this.__vm_GenTools.FileStream.existsSync(FullPath)) {
                                let UEFI_Object = new this.__uefiObject();
                                UEFI_Object.firmwareName = value2[value2.length - 1].split(".")[0];
                                UEFI_Object.firmwarePath = FullPath;
                                array[index].UEFI_BIOS.push(UEFI_Object);
                            }
                        });
                        break;
                }
                /**UEFI*/
            });
            console.log("完成初始化系统");
            return true;
        } catch (e) {
            console.log(e.message);
            return false;
        }
    },
    afterProcess(config_vm) {
        let current_VM_configure_Path;
        current_VM_configure_Path = this.__vm_GenTools.PathStream.join(this.__vm_MachinePath, config_vm.id);
        let analyzeTask = (vl) => {
            let FinalConvert;
            try {
                let count = 0
                if (this.__vm_GenTools.DataTypeDetect(vl) === "obj" && this.__vm_GenTools.emptyObjectDetect(vl) === false) {
                    FinalConvert = {};
                    for (let nxx in vl) {
                        switch (String(nxx).toLowerCase()) {
                            case "name":
                                if (String(vl[nxx]).toString() !== "") {
                                    FinalConvert.Name = vl[nxx];
                                } else {
                                    throw new Error("");
                                }
                                count++;
                                break;
                            case "task":
                                FinalConvert.Task = String(vl[nxx]).toLowerCase();
                                count++;
                                break;
                            case "type":
                                FinalConvert.Type = String(vl[nxx]).toLowerCase();
                                count++;
                                break;
                            case "size":
                                FinalConvert.Size = vl[nxx];
                                count++;
                                break;
                            case "unit":
                                FinalConvert.Unit = vl[nxx];
                                count++;
                                break;
                            case "fmt":
                                FinalConvert.Fmt = String(vl[nxx]).toLowerCase();
                                count++;
                                break;
                            default:
                                throw new Error("");
                        }
                    }
                    if (count < 5) {
                        throw new Error("");
                    }
                }
            } catch (e) {
                FinalConvert = undefined;
            }
            return FinalConvert;
        }
        if (this.__vm_GenTools.DataTypeDetect(config_vm.hardware) === "arr") {
            config_vm.hardware.forEach((value, index, array) => {
                let {NeedOperate} = value;
                if (NeedOperate !== undefined && NeedOperate !== "") {
                    try {
                        let analyzeData = analyzeTask(NeedOperate);
                        if (analyzeData === undefined) {
                            throw new Error("需要后期处理的数据异常！");
                        } else {
                            let detectRegExp = /([a-z])/i;
                            let {Task, Type, Size, Fmt, Name, Unit} = analyzeData;
                            let OperateInfo;
                            let FinalConfirmFullPath;
                            if (String(Name).charAt(0) === "/" || (detectRegExp.test(String(Name).charAt(0)) && String(Name).charAt(1) === ":")) {
                                FinalConfirmFullPath = Name;
                            } else {
                                FinalConfirmFullPath = this.__vm_GenTools.PathStream.join(current_VM_configure_Path, Name);
                            }
                            let pls = [];
                            let pdir = this.__vm_GenTools.getFileOnDirectoryLocation(this.__vm_framework[0].ProgramPath);
                            switch (this.__vm_HostPlatform) {
                                case "linux":
                                    pls.push(`${this.__vm_GenTools.PathStream.join(pdir, "qemu-img")}`);
                                    break;
                                case "win32":
                                    pls.push(`"${this.__vm_GenTools.PathStream.join(pdir, "qemu-img.exe")}"`);
                                    break;
                            }
                            pls.push(Task);
                            pls.push(`-f ${Fmt}`);
                            pls.push(`${FinalConfirmFullPath}`);
                            pls.push(`${Size}${Unit}`);
                            OperateInfo = this.__ExtraChildSystem.execSync(pls.join(" ")).toString("utf-8");
                            let logNum = this.__vm_logging_Area.length;
                            this.__vm_logging_Area.push({logNumber: logNum, logContent: OperateInfo});
                            array[index].NeedOperate = undefined;
                        }
                    } catch (e) {
                        array[index].throwsLabel = true;
                        let logNum = this.__vm_logging_Area.length;
                        this.__vm_logging_Area.push({logNumber: logNum, logContent: e.message});
                    }
                }
            });
        }
        while (true) {
            let throwNum = 0;
            for (let num in config_vm.hardware) {
                if (this.__vm_GenTools.DataTypeDetect(config_vm.hardware[num].throwsLabel) === "boo") {
                    throwNum++;
                    config_vm.hardware.splice(num, 1);
                }
            }
            if (throwNum <= 0) {
                break;
            }
        }
    },
    vm_updateHostData() {
        let hostList = {hostListData: []};
        this.__vm_existHost.forEach((value, index, array) => {
            let tmp = {};
            for (let num in value) {
                if (String(num).toString() !== "controls") {
                    tmp[num] = value[num];
                }
            }
            hostList.hostListData.push(tmp);
        });
        let updateHost = this.__vm_GenTools.FileStream.createWriteStream(this.__vm_GenTools.PathStream.join(this.__vm_ProgramPath, "host.json"), "utf-8");
        updateHost.write(JSON.stringify(hostList, null, 4));
        updateHost.close();
        let logNum = this.__vm_logging_Area.length;
        this.__vm_logging_Area.push({logNumber: logNum, logContent: "update HostListData"});
    },
    preData() {
        this.__vm_SupportOsList.push({
            TypeName: "x86_64",
            ListCollect: [
                {
                    subTypeName: "Windows",
                    subListCollect: [
                        {
                            osName: "Windows 95 32位",
                            value: "w95",
                            PreConfig: {
                                machine_configure_model: {enable: true, value: "pc"},
                                machine_configure_rtc: {
                                    enable: true,
                                    mode: true,
                                    sim_config: true,
                                    value: "localtime"
                                },
                                machine_configure_cpu: {enable: true, value: "pentium"},
                                machine_configure_memory: {enable: true, value: 128},
                                machine_configure_gpu: {enable: true, value: "std"},
                                machine_configure_display: {
                                    enable: true,
                                    mode: true,
                                    value: "gtk",
                                    fullscreen: false
                                },
                                machine_configure_vts: {enable: true, mode: true, value: "tcg"},
                                machine_configure_kvm: {enable: false, value: false},
                                machine_configure_fips: {enable: false, value: false},
                                machine_configure_smp: {enable: true, mode: true, value: 1},
                                machine_configure_sound: {enable: true, value: "ac97"},
                                machine_configure_uefi: {enable: false, value: ""},
                                machine_configure_boot: {enable: true, mode: true, value: "c"},
                            }
                        },
                        {
                            osName: "Windows 98 32位",
                            value: "w98",
                            PreConfig: {
                                machine_configure_model: {enable: true, value: "pc"},
                                machine_configure_rtc: {
                                    enable: true,
                                    mode: true,
                                    sim_config: true,
                                    value: "localtime"
                                },
                                machine_configure_cpu: {enable: true, value: "pentium3"},
                                machine_configure_memory: {enable: true, value: 128},
                                machine_configure_gpu: {enable: true, value: "std"},
                                machine_configure_display: {
                                    enable: true,
                                    mode: true,
                                    value: "gtk",
                                    fullscreen: false
                                },
                                machine_configure_vts: {enable: true, mode: true, value: "tcg"},
                                machine_configure_kvm: {enable: false, value: false},
                                machine_configure_fips: {enable: false, value: false},
                                machine_configure_smp: {enable: true, mode: true, value: 1},
                                machine_configure_sound: {enable: true, value: "ac97"},
                                machine_configure_uefi: {enable: false, value: ""},
                                machine_configure_boot: {enable: true, mode: true, value: "c"},
                            }
                        },
                        {
                            osName: "Windows ME 32位",
                            value: "wME",
                            PreConfig: {
                                machine_configure_model: {enable: true, value: "pc"},
                                machine_configure_rtc: {
                                    enable: true,
                                    mode: true,
                                    sim_config: true,
                                    value: "localtime"
                                },
                                machine_configure_cpu: {enable: true, value: "pentium3"},
                                machine_configure_memory: {enable: true, value: 128},
                                machine_configure_gpu: {enable: true, value: "std"},
                                machine_configure_display: {
                                    enable: true,
                                    mode: true,
                                    value: "gtk",
                                    fullscreen: false
                                },
                                machine_configure_vts: {enable: true, mode: true, value: "tcg"},
                                machine_configure_kvm: {enable: false, value: false},
                                machine_configure_fips: {enable: false, value: false},
                                machine_configure_smp: {enable: true, mode: true, value: 1},
                                machine_configure_sound: {enable: true, value: "ac97"},
                                machine_configure_uefi: {enable: false, value: ""},
                                machine_configure_boot: {enable: true, mode: true, value: "c"},
                            }
                        },
                        {
                            osName: "Windows 2000 32位",
                            value: "w2K",
                            PreConfig: {
                                machine_configure_model: {enable: true, value: "pc"},
                                machine_configure_rtc: {
                                    enable: true,
                                    mode: true,
                                    sim_config: true,
                                    value: "localtime"
                                },
                                machine_configure_cpu: {enable: true, value: "pentium3"},
                                machine_configure_memory: {enable: true, value: 192},
                                machine_configure_gpu: {enable: true, value: "std"},
                                machine_configure_display: {
                                    enable: true,
                                    mode: true,
                                    value: "gtk",
                                    fullscreen: false
                                },
                                machine_configure_vts: {enable: true, mode: true, value: "tcg"},
                                machine_configure_kvm: {enable: false, value: false},
                                machine_configure_fips: {enable: false, value: false},
                                machine_configure_smp: {enable: true, mode: true, value: 1},
                                machine_configure_sound: {enable: true, value: "ac97"},
                                machine_configure_uefi: {enable: false, value: ""},
                                machine_configure_boot: {enable: true, mode: true, value: "c"},
                            }
                        },
                        {
                            osName: "Windows XP 32位",
                            value: "wXP32",
                            PreConfig: {
                                machine_configure_model: {enable: true, value: "pc"},
                                machine_configure_rtc: {
                                    enable: true,
                                    mode: true,
                                    sim_config: true,
                                    value: "localtime"
                                },
                                machine_configure_cpu: {enable: true, value: "pentium3"},
                                machine_configure_memory: {enable: true, value: 512},
                                machine_configure_gpu: {enable: true, value: "std"},
                                machine_configure_display: {
                                    enable: true,
                                    mode: true,
                                    value: "gtk",
                                    fullscreen: false
                                },
                                machine_configure_vts: {enable: true, mode: true, value: "tcg"},
                                machine_configure_kvm: {enable: false, value: false},
                                machine_configure_fips: {enable: false, value: false},
                                machine_configure_smp: {enable: true, mode: true, value: 1},
                                machine_configure_sound: {enable: true, value: "ac97"},
                                machine_configure_uefi: {enable: false, value: ""},
                                machine_configure_boot: {enable: true, mode: true, value: "c"},
                            }
                        },
                        {
                            osName: "Windows XP 64位",
                            value: "wXP64",
                            PreConfig: {
                                machine_configure_model: {enable: true, value: "pc"},
                                machine_configure_rtc: {
                                    enable: true,
                                    mode: true,
                                    sim_config: true,
                                    value: "localtime"
                                },
                                machine_configure_cpu: {enable: true, value: "core2duo-v1"},
                                machine_configure_memory: {enable: true, value: 1024},
                                machine_configure_gpu: {enable: true, value: "std"},
                                machine_configure_display: {
                                    enable: true,
                                    mode: true,
                                    value: "gtk",
                                    fullscreen: false
                                },
                                machine_configure_vts: {enable: true, mode: true, value: "tcg"},
                                machine_configure_kvm: {enable: false, value: false},
                                machine_configure_fips: {enable: false, value: false},
                                machine_configure_smp: {enable: true, mode: true, value: 1},
                                machine_configure_sound: {enable: true, value: "ac97"},
                                machine_configure_uefi: {enable: false, value: ""},
                                machine_configure_boot: {enable: true, mode: true, value: "c"},
                            }
                        },
                        {
                            osName: "Windows Vista 32位",
                            value: "wVista32",
                            PreConfig: {
                                machine_configure_model: {enable: true, value: "pc"},
                                machine_configure_rtc: {
                                    enable: true,
                                    mode: true,
                                    sim_config: true,
                                    value: "localtime"
                                },
                                machine_configure_cpu: {enable: true, value: "Conroe-v1"},
                                machine_configure_memory: {enable: true, value: 3072},
                                machine_configure_gpu: {enable: true, value: "std"},
                                machine_configure_display: {
                                    enable: true,
                                    mode: true,
                                    value: "gtk",
                                    fullscreen: false
                                },
                                machine_configure_vts: {enable: true, mode: true, value: "tcg"},
                                machine_configure_kvm: {enable: false, value: false},
                                machine_configure_fips: {enable: false, value: false},
                                machine_configure_smp: {enable: true, mode: true, value: 1},
                                machine_configure_sound: {enable: true, value: "hda"},
                                machine_configure_uefi: {enable: false, value: ""},
                                machine_configure_boot: {enable: true, mode: true, value: "c"},
                            }
                        },
                        {
                            osName: "Windows Vista 64位",
                            value: "wVista64",
                            PreConfig: {
                                machine_configure_model: {enable: true, value: "pc"},
                                machine_configure_rtc: {
                                    enable: true,
                                    mode: true,
                                    sim_config: true,
                                    value: "localtime"
                                },
                                machine_configure_cpu: {enable: true, value: "Conroe-v1"},
                                machine_configure_memory: {enable: true, value: 4096},
                                machine_configure_gpu: {enable: true, value: "std"},
                                machine_configure_display: {
                                    enable: true,
                                    mode: true,
                                    value: "gtk",
                                    fullscreen: false
                                },
                                machine_configure_vts: {enable: true, mode: true, value: "tcg"},
                                machine_configure_kvm: {enable: false, value: false},
                                machine_configure_fips: {enable: false, value: false},
                                machine_configure_smp: {enable: true, mode: true, value: 1},
                                machine_configure_sound: {enable: true, value: "hda"},
                                machine_configure_uefi: {enable: false, value: ""},
                                machine_configure_boot: {enable: true, mode: true, value: "c"},
                            }
                        },
                        {
                            osName: "Windows 7 32位",
                            value: "wSeven32",
                            PreConfig: {
                                machine_configure_model: {enable: true, value: "pc"},
                                machine_configure_rtc: {
                                    enable: true,
                                    mode: true,
                                    sim_config: true,
                                    value: "localtime"
                                },
                                machine_configure_cpu: {enable: true, value: "Conroe-v1"},
                                machine_configure_memory: {enable: true, value: 3072},
                                machine_configure_gpu: {enable: true, value: "std"},
                                machine_configure_display: {
                                    enable: true,
                                    mode: true,
                                    value: "gtk",
                                    fullscreen: false
                                },
                                machine_configure_vts: {enable: true, mode: true, value: "tcg"},
                                machine_configure_kvm: {enable: false, value: false},
                                machine_configure_fips: {enable: false, value: false},
                                machine_configure_smp: {enable: true, mode: true, value: 1},
                                machine_configure_sound: {enable: true, value: "hda"},
                                machine_configure_uefi: {enable: false, value: ""},
                                machine_configure_boot: {enable: true, mode: true, value: "c"},
                            }
                        },
                        {
                            osName: "Windows 7 64位",
                            value: "wSeven64",
                            PreConfig: {
                                machine_configure_model: {enable: true, value: "pc"},
                                machine_configure_rtc: {
                                    enable: true,
                                    mode: true,
                                    sim_config: true,
                                    value: "localtime"
                                },
                                machine_configure_cpu: {enable: true, value: "Conroe-v1"},
                                machine_configure_memory: {enable: true, value: 4096},
                                machine_configure_gpu: {enable: true, value: "std"},
                                machine_configure_display: {
                                    enable: true,
                                    mode: true,
                                    value: "gtk",
                                    fullscreen: false
                                },
                                machine_configure_vts: {enable: true, mode: true, value: "tcg"},
                                machine_configure_kvm: {enable: false, value: false},
                                machine_configure_fips: {enable: false, value: false},
                                machine_configure_smp: {enable: true, mode: true, value: 1},
                                machine_configure_sound: {enable: true, value: "hda"},
                                machine_configure_uefi: {enable: false, value: ""},
                                machine_configure_boot: {enable: true, mode: true, value: "c"},
                            }
                        },
                        {
                            osName: "Windows 8 32位",
                            value: "wEight32",
                            PreConfig: {
                                machine_configure_model: {enable: true, value: "q35"},
                                machine_configure_rtc: {
                                    enable: true,
                                    mode: true,
                                    sim_config: true,
                                    value: "localtime"
                                },
                                machine_configure_cpu: {enable: true, value: "Skylake-Client"},
                                machine_configure_memory: {enable: true, value: 3072},
                                machine_configure_gpu: {enable: true, value: "virtio"},
                                machine_configure_display: {
                                    enable: true,
                                    mode: true,
                                    value: "gtk",
                                    fullscreen: false
                                },
                                machine_configure_vts: {enable: true, mode: true, value: "tcg"},
                                machine_configure_kvm: {enable: false, value: false},
                                machine_configure_fips: {enable: false, value: false},
                                machine_configure_smp: {enable: true, mode: true, value: 1},
                                machine_configure_sound: {enable: true, value: "hda"},
                                machine_configure_uefi: {enable: false, value: ""},
                                machine_configure_boot: {enable: true, mode: true, value: "c"},
                            }
                        },
                        {
                            osName: "Windows 8 64位",
                            value: "wEight64",
                            PreConfig: {
                                machine_configure_model: {enable: true, value: "q35"},
                                machine_configure_rtc: {
                                    enable: true,
                                    mode: true,
                                    sim_config: true,
                                    value: "localtime"
                                },
                                machine_configure_cpu: {enable: true, value: "Skylake-Client"},
                                machine_configure_memory: {enable: true, value: 4096},
                                machine_configure_gpu: {enable: true, value: "virtio"},
                                machine_configure_display: {
                                    enable: true,
                                    mode: true,
                                    value: "gtk",
                                    fullscreen: false
                                },
                                machine_configure_vts: {enable: true, mode: true, value: "tcg"},
                                machine_configure_kvm: {enable: false, value: false},
                                machine_configure_fips: {enable: false, value: false},
                                machine_configure_smp: {enable: true, mode: true, value: 1},
                                machine_configure_sound: {enable: true, value: "hda"},
                                machine_configure_uefi: {enable: false, value: ""},
                                machine_configure_boot: {enable: true, mode: true, value: "c"},
                            }
                        },
                        {
                            osName: "Windows 8.1 32位",
                            value: "wEight_One,32",
                            PreConfig: {
                                machine_configure_model: {enable: true, value: "q35"},
                                machine_configure_rtc: {
                                    enable: true,
                                    mode: true,
                                    sim_config: true,
                                    value: "localtime"
                                },
                                machine_configure_cpu: {enable: true, value: "Skylake-Client"},
                                machine_configure_memory: {enable: true, value: 3072},
                                machine_configure_gpu: {enable: true, value: "virtio"},
                                machine_configure_display: {
                                    enable: true,
                                    mode: true,
                                    value: "gtk",
                                    fullscreen: false
                                },
                                machine_configure_vts: {enable: true, mode: true, value: "tcg"},
                                machine_configure_kvm: {enable: false, value: false},
                                machine_configure_fips: {enable: false, value: false},
                                machine_configure_smp: {enable: true, mode: true, value: 1},
                                machine_configure_sound: {enable: true, value: "hda"},
                                machine_configure_uefi: {enable: false, value: ""},
                                machine_configure_boot: {enable: true, mode: true, value: "c"},
                            }
                        },
                        {
                            osName: "Windows 8.1 64位",
                            value: "wEight_One,64",
                            PreConfig: {
                                machine_configure_model: {enable: true, value: "q35"},
                                machine_configure_rtc: {
                                    enable: true,
                                    mode: true,
                                    sim_config: true,
                                    value: "localtime"
                                },
                                machine_configure_cpu: {enable: true, value: "Skylake-Client"},
                                machine_configure_memory: {enable: true, value: 4096},
                                machine_configure_gpu: {enable: true, value: "virtio"},
                                machine_configure_display: {
                                    enable: true,
                                    mode: true,
                                    value: "gtk",
                                    fullscreen: false
                                },
                                machine_configure_vts: {enable: true, mode: true, value: "tcg"},
                                machine_configure_kvm: {enable: false, value: false},
                                machine_configure_fips: {enable: false, value: false},
                                machine_configure_smp: {enable: true, mode: true, value: 1},
                                machine_configure_sound: {enable: true, value: "hda"},
                                machine_configure_uefi: {enable: false, value: ""},
                                machine_configure_boot: {enable: true, mode: true, value: "c"},
                            }
                        },
                        {
                            osName: "Windows 10 32位",
                            value: "wTen32",
                            PreConfig: {
                                machine_configure_model: {enable: true, value: "q35"},
                                machine_configure_rtc: {
                                    enable: true,
                                    mode: true,
                                    sim_config: true,
                                    value: "localtime"
                                },
                                machine_configure_cpu: {enable: true, value: "Icelake-Client"},
                                machine_configure_memory: {enable: true, value: 3072},
                                machine_configure_gpu: {enable: true, value: "virtio"},
                                machine_configure_display: {
                                    enable: true,
                                    mode: true,
                                    value: "gtk",
                                    fullscreen: false
                                },
                                machine_configure_vts: {enable: true, mode: true, value: "tcg"},
                                machine_configure_kvm: {enable: false, value: false},
                                machine_configure_fips: {enable: false, value: false},
                                machine_configure_smp: {enable: true, mode: true, value: 1},
                                machine_configure_sound: {enable: true, value: "hda"},
                                machine_configure_uefi: {enable: false, value: ""},
                                machine_configure_boot: {enable: true, mode: true, value: "c"},
                            }
                        },
                        {
                            osName: "Windows 10 64位",
                            value: "wTen64",
                            PreConfig: {
                                machine_configure_model: {enable: true, value: "q35"},
                                machine_configure_rtc: {
                                    enable: true,
                                    mode: true,
                                    sim_config: true,
                                    value: "localtime"
                                },
                                machine_configure_cpu: {enable: true, value: "Icelake-Client"},
                                machine_configure_memory: {enable: true, value: 8192},
                                machine_configure_gpu: {enable: true, value: "virtio"},
                                machine_configure_display: {
                                    enable: true,
                                    mode: true,
                                    value: "gtk",
                                    fullscreen: false
                                },
                                machine_configure_vts: {enable: true, mode: true, value: "tcg"},
                                machine_configure_kvm: {enable: false, value: false},
                                machine_configure_fips: {enable: false, value: false},
                                machine_configure_smp: {enable: true, mode: true, value: 1},
                                machine_configure_sound: {enable: true, value: "hda"},
                                machine_configure_uefi: {enable: false, value: ""},
                                machine_configure_boot: {enable: true, mode: true, value: "c"},
                            }
                        },
                        {
                            osName: "Windows 11 64位",
                            value: "wEleven64",
                            PreConfig: {
                                machine_configure_model: {enable: true, value: "q35"},
                                machine_configure_rtc: {
                                    enable: true,
                                    mode: true,
                                    sim_config: true,
                                    value: "localtime"
                                },
                                machine_configure_cpu: {enable: true, value: "Icelake-Client"},
                                machine_configure_memory: {enable: true, value: 16384},
                                machine_configure_gpu: {enable: true, value: "virtio"},
                                machine_configure_display: {
                                    enable: true,
                                    mode: true,
                                    value: "gtk",
                                    fullscreen: false
                                },
                                machine_configure_vts: {enable: true, mode: true, value: "tcg"},
                                machine_configure_kvm: {enable: false, value: false},
                                machine_configure_fips: {enable: false, value: false},
                                machine_configure_smp: {enable: true, mode: true, value: 1},
                                machine_configure_sound: {enable: true, value: "hda"},
                                machine_configure_uefi: {enable: false, value: ""},
                                machine_configure_boot: {enable: true, mode: true, value: "c"},
                            }
                        },
                        {
                            osName: "Other Windows 32位",
                            value: "wOther32",
                            PreConfig: {
                                machine_configure_model: {enable: true, value: "pc"},
                                machine_configure_rtc: {
                                    enable: true,
                                    mode: true,
                                    sim_config: true,
                                    value: "localtime"
                                },
                                machine_configure_cpu: {enable: true, value: "base"},
                                machine_configure_memory: {enable: true, value: 1024},
                                machine_configure_gpu: {enable: true, value: "std"},
                                machine_configure_display: {
                                    enable: true,
                                    mode: true,
                                    value: "gtk",
                                    fullscreen: false
                                },
                                machine_configure_vts: {enable: true, mode: true, value: "tcg"},
                                machine_configure_kvm: {enable: false, value: false},
                                machine_configure_fips: {enable: false, value: false},
                                machine_configure_smp: {enable: true, mode: true, value: 1},
                                machine_configure_sound: {enable: true, value: "hda"},
                                machine_configure_uefi: {enable: false, value: ""},
                                machine_configure_boot: {enable: true, mode: true, value: "c"},
                            }
                        },
                        {
                            osName: "Other Windows 64位",
                            value: "wOther64",
                            PreConfig: {
                                machine_configure_model: {enable: true, value: "pc"},
                                machine_configure_rtc: {
                                    enable: true,
                                    mode: true,
                                    sim_config: true,
                                    value: "localtime"
                                },
                                machine_configure_cpu: {enable: true, value: "base"},
                                machine_configure_memory: {enable: true, value: 3072},
                                machine_configure_gpu: {enable: true, value: "std"},
                                machine_configure_display: {
                                    enable: true,
                                    mode: true,
                                    value: "gtk",
                                    fullscreen: false
                                },
                                machine_configure_vts: {enable: true, mode: true, value: "tcg"},
                                machine_configure_kvm: {enable: false, value: false},
                                machine_configure_fips: {enable: false, value: false},
                                machine_configure_smp: {enable: true, mode: true, value: 1},
                                machine_configure_sound: {enable: true, value: "hda"},
                                machine_configure_uefi: {enable: false, value: ""},
                                machine_configure_boot: {enable: true, mode: true, value: "c"},
                            }
                        }
                    ]
                },
                {
                    subTypeName: "Linux",
                    subListCollect: [
                        {
                            osName: "Deepin 32位"
                        }, {
                            osName: "Deepin 64位"
                        }, {
                            osName: "Ubuntu 32位"
                        }, {
                            osName: "Ubuntu 64位"
                        }, {
                            osName: "Manjaro 64位"
                        }, {
                            osName: "Other Linux 32位"
                        }, {
                            osName: "Other Linux 64位"
                        },
                    ]
                }
            ]
        });
        this.__vm_SupportOsList.push({
            TypeName: "i386",
            ListCollect: [
                {
                    subTypeName: "Windows",
                    subListCollect: [
                        {
                            osName: "Windows 95",
                            value: "w95",
                            PreConfig: {
                                machine_configure_model: {enable: true, value: "pc"},
                                machine_configure_rtc: {
                                    enable: true,
                                    mode: true,
                                    sim_config: true,
                                    value: "localtime"
                                },
                                machine_configure_cpu: {enable: true, value: "pentium"},
                                machine_configure_memory: {enable: true, value: 128},
                                machine_configure_gpu: {enable: true, value: "std"},
                                machine_configure_display: {
                                    enable: true,
                                    mode: true,
                                    value: "gtk",
                                    fullscreen: false
                                },
                                machine_configure_vts: {enable: true, mode: true, value: "tcg"},
                                machine_configure_kvm: {enable: false, value: false},
                                machine_configure_fips: {enable: false, value: false},
                                machine_configure_smp: {enable: true, mode: true, value: 1},
                                machine_configure_sound: {enable: true, value: "ac97"},
                                machine_configure_uefi: {enable: false, value: ""},
                                machine_configure_boot: {enable: true, mode: true, value: "c"},
                            }
                        },
                        {
                            osName: "Windows 98",
                            value: "w98",
                            PreConfig: {
                                machine_configure_model: {enable: true, value: "pc"},
                                machine_configure_rtc: {
                                    enable: true,
                                    mode: true,
                                    sim_config: true,
                                    value: "localtime"
                                },
                                machine_configure_cpu: {enable: true, value: "pentium3"},
                                machine_configure_memory: {enable: true, value: 128},
                                machine_configure_gpu: {enable: true, value: "std"},
                                machine_configure_display: {
                                    enable: true,
                                    mode: true,
                                    value: "gtk",
                                    fullscreen: false
                                },
                                machine_configure_vts: {enable: true, mode: true, value: "tcg"},
                                machine_configure_kvm: {enable: false, value: false},
                                machine_configure_fips: {enable: false, value: false},
                                machine_configure_smp: {enable: true, mode: true, value: 1},
                                machine_configure_sound: {enable: true, value: "ac97"},
                                machine_configure_uefi: {enable: false, value: ""},
                                machine_configure_boot: {enable: true, mode: true, value: "c"},
                            }
                        },
                        {
                            osName: "Windows ME",
                            value: "wME",
                            PreConfig: {
                                machine_configure_model: {enable: true, value: "pc"},
                                machine_configure_rtc: {
                                    enable: true,
                                    mode: true,
                                    sim_config: true,
                                    value: "localtime"
                                },
                                machine_configure_cpu: {enable: true, value: "pentium3"},
                                machine_configure_memory: {enable: true, value: 128},
                                machine_configure_gpu: {enable: true, value: "std"},
                                machine_configure_display: {
                                    enable: true,
                                    mode: true,
                                    value: "gtk",
                                    fullscreen: false
                                },
                                machine_configure_vts: {enable: true, mode: true, value: "tcg"},
                                machine_configure_kvm: {enable: false, value: false},
                                machine_configure_fips: {enable: false, value: false},
                                machine_configure_smp: {enable: true, mode: true, value: 1},
                                machine_configure_sound: {enable: true, value: "ac97"},
                                machine_configure_uefi: {enable: false, value: ""},
                                machine_configure_boot: {enable: true, mode: true, value: "c"},
                            }
                        },
                        {
                            osName: "Windows 2000",
                            value: "w2K",
                            PreConfig: {
                                machine_configure_model: {enable: true, value: "pc"},
                                machine_configure_rtc: {
                                    enable: true,
                                    mode: true,
                                    sim_config: true,
                                    value: "localtime"
                                },
                                machine_configure_cpu: {enable: true, value: "pentium3"},
                                machine_configure_memory: {enable: true, value: 192},
                                machine_configure_gpu: {enable: true, value: "std"},
                                machine_configure_display: {
                                    enable: true,
                                    mode: true,
                                    value: "gtk",
                                    fullscreen: false
                                },
                                machine_configure_vts: {enable: true, mode: true, value: "tcg"},
                                machine_configure_kvm: {enable: false, value: false},
                                machine_configure_fips: {enable: false, value: false},
                                machine_configure_smp: {enable: true, mode: true, value: 1},
                                machine_configure_sound: {enable: true, value: "ac97"},
                                machine_configure_uefi: {enable: false, value: ""},
                                machine_configure_boot: {enable: true, mode: true, value: "c"},
                            }
                        },
                        {
                            osName: "Windows XP",
                            value: "wXP32",
                            PreConfig: {
                                machine_configure_model: {enable: true, value: "pc"},
                                machine_configure_rtc: {
                                    enable: true,
                                    mode: true,
                                    sim_config: true,
                                    value: "localtime"
                                },
                                machine_configure_cpu: {enable: true, value: "pentium3"},
                                machine_configure_memory: {enable: true, value: 512},
                                machine_configure_gpu: {enable: true, value: "std"},
                                machine_configure_display: {
                                    enable: true,
                                    mode: true,
                                    value: "gtk",
                                    fullscreen: false
                                },
                                machine_configure_vts: {enable: true, mode: true, value: "tcg"},
                                machine_configure_kvm: {enable: false, value: false},
                                machine_configure_fips: {enable: false, value: false},
                                machine_configure_smp: {enable: true, mode: true, value: 1},
                                machine_configure_sound: {enable: true, value: "ac97"},
                                machine_configure_uefi: {enable: false, value: ""},
                                machine_configure_boot: {enable: true, mode: true, value: "c"},
                            }
                        },
                        {
                            osName: "Windows Vista",
                            value: "wVista32",
                            PreConfig: {
                                machine_configure_model: {enable: true, value: "pc"},
                                machine_configure_rtc: {
                                    enable: true,
                                    mode: true,
                                    sim_config: true,
                                    value: "localtime"
                                },
                                machine_configure_cpu: {enable: true, value: "Conroe-v1"},
                                machine_configure_memory: {enable: true, value: 3072},
                                machine_configure_gpu: {enable: true, value: "std"},
                                machine_configure_display: {
                                    enable: true,
                                    mode: true,
                                    value: "gtk",
                                    fullscreen: false
                                },
                                machine_configure_vts: {enable: true, mode: true, value: "tcg"},
                                machine_configure_kvm: {enable: false, value: false},
                                machine_configure_fips: {enable: false, value: false},
                                machine_configure_smp: {enable: true, mode: true, value: 1},
                                machine_configure_sound: {enable: true, value: "hda"},
                                machine_configure_uefi: {enable: false, value: ""},
                                machine_configure_boot: {enable: true, mode: true, value: "c"},
                            }
                        },
                        {
                            osName: "Windows 7",
                            value: "wSeven32",
                            PreConfig: {
                                machine_configure_model: {enable: true, value: "pc"},
                                machine_configure_rtc: {
                                    enable: true,
                                    mode: true,
                                    sim_config: true,
                                    value: "localtime"
                                },
                                machine_configure_cpu: {enable: true, value: "Conroe-v1"},
                                machine_configure_memory: {enable: true, value: 3072},
                                machine_configure_gpu: {enable: true, value: "std"},
                                machine_configure_display: {
                                    enable: true,
                                    mode: true,
                                    value: "gtk",
                                    fullscreen: false
                                },
                                machine_configure_vts: {enable: true, mode: true, value: "tcg"},
                                machine_configure_kvm: {enable: false, value: false},
                                machine_configure_fips: {enable: false, value: false},
                                machine_configure_smp: {enable: true, mode: true, value: 1},
                                machine_configure_sound: {enable: true, value: "hda"},
                                machine_configure_uefi: {enable: false, value: ""},
                                machine_configure_boot: {enable: true, mode: true, value: "c"},
                            }
                        },
                        {
                            osName: "Windows 8",
                            value: "wEight32",
                            PreConfig: {
                                machine_configure_model: {enable: true, value: "pc"},
                                machine_configure_rtc: {
                                    enable: true,
                                    mode: true,
                                    sim_config: true,
                                    value: "localtime"
                                },
                                machine_configure_cpu: {enable: true, value: "Skylake-Client"},
                                machine_configure_memory: {enable: true, value: 3072},
                                machine_configure_gpu: {enable: true, value: "virtio"},
                                machine_configure_display: {
                                    enable: true,
                                    mode: true,
                                    value: "gtk",
                                    fullscreen: false
                                },
                                machine_configure_vts: {enable: true, mode: true, value: "tcg"},
                                machine_configure_kvm: {enable: false, value: false},
                                machine_configure_fips: {enable: false, value: false},
                                machine_configure_smp: {enable: true, mode: true, value: 1},
                                machine_configure_sound: {enable: true, value: "hda"},
                                machine_configure_uefi: {enable: false, value: ""},
                                machine_configure_boot: {enable: true, mode: true, value: "c"},
                            }
                        },
                        {
                            osName: "Windows 8.1",
                            value: "wEight_One,32",
                            PreConfig: {
                                machine_configure_model: {enable: true, value: "pc"},
                                machine_configure_rtc: {
                                    enable: true,
                                    mode: true,
                                    sim_config: true,
                                    value: "localtime"
                                },
                                machine_configure_cpu: {enable: true, value: "Skylake-Client"},
                                machine_configure_memory: {enable: true, value: 3072},
                                machine_configure_gpu: {enable: true, value: "virtio"},
                                machine_configure_display: {
                                    enable: true,
                                    mode: true,
                                    value: "gtk",
                                    fullscreen: false
                                },
                                machine_configure_vts: {enable: true, mode: true, value: "tcg"},
                                machine_configure_kvm: {enable: false, value: false},
                                machine_configure_fips: {enable: false, value: false},
                                machine_configure_smp: {enable: true, mode: true, value: 1},
                                machine_configure_sound: {enable: true, value: "hda"},
                                machine_configure_uefi: {enable: false, value: ""},
                                machine_configure_boot: {enable: true, mode: true, value: "c"},
                            }
                        },
                        {
                            osName: "Windows 10",
                            value: "wTen32",
                            PreConfig: {
                                machine_configure_model: {enable: true, value: "pc"},
                                machine_configure_rtc: {
                                    enable: true,
                                    mode: true,
                                    sim_config: true,
                                    value: "localtime"
                                },
                                machine_configure_cpu: {enable: true, value: "Icelake-Client"},
                                machine_configure_memory: {enable: true, value: 3072},
                                machine_configure_gpu: {enable: true, value: "virtio"},
                                machine_configure_display: {
                                    enable: true,
                                    mode: true,
                                    value: "gtk",
                                    fullscreen: false
                                },
                                machine_configure_vts: {enable: true, mode: true, value: "tcg"},
                                machine_configure_kvm: {enable: false, value: false},
                                machine_configure_fips: {enable: false, value: false},
                                machine_configure_smp: {enable: true, mode: true, value: 1},
                                machine_configure_sound: {enable: true, value: "hda"},
                                machine_configure_uefi: {enable: false, value: ""},
                                machine_configure_boot: {enable: true, mode: true, value: "c"},
                            }
                        },
                        {
                            osName: "Other Windows",
                            value: "wOther32",
                            PreConfig: {
                                machine_configure_model: {enable: true, value: "pc"},
                                machine_configure_rtc: {
                                    enable: true,
                                    mode: true,
                                    sim_config: true,
                                    value: "localtime"
                                },
                                machine_configure_cpu: {enable: true, value: "base"},
                                machine_configure_memory: {enable: true, value: 1024},
                                machine_configure_gpu: {enable: true, value: "std"},
                                machine_configure_display: {
                                    enable: true,
                                    mode: true,
                                    value: "gtk",
                                    fullscreen: false
                                },
                                machine_configure_vts: {enable: true, mode: true, value: "tcg"},
                                machine_configure_kvm: {enable: false, value: false},
                                machine_configure_fips: {enable: false, value: false},
                                machine_configure_smp: {enable: true, mode: true, value: 1},
                                machine_configure_sound: {enable: true, value: "hda"},
                                machine_configure_uefi: {enable: false, value: ""},
                                machine_configure_boot: {enable: true, mode: true, value: "c"},
                            }
                        }
                    ]
                },
                {
                    subTypeName: "Linux",
                    subListCollect: [
                        {
                            osName: "Deepin"
                        }, {
                            osName: "Ubuntu"
                        }, {
                            osName: "Other Linux"
                        },
                    ]
                }
            ]
        });
        this.__vm_SupportOsList.push({
            TypeName: "arm",
            ListCollect: [
                {
                    subTypeName: "Windows",
                    subListCollect: [
                        {
                            osName: "Other Windows 32位",
                            value: "wOther32",
                        }
                    ]
                },
                {
                    subTypeName: "Linux",
                    subListCollect: [
                        {
                            osName: "Other Linux 32位"
                        },
                    ]
                }
            ]
        });
        this.__vm_SupportOsList.push({
            TypeName: "aarch64",
            ListCollect: [
                {
                    subTypeName: "Windows",
                    subListCollect: [
                        {
                            osName: "Windows 10 Arm 64位",
                            value: "wTen64",
                        }, {
                            osName: "Windows 11 Arm 64位",
                            value: "wEleven64",
                        }, {
                            osName: "Other Windows Arm 64位",
                            value: "wOther64",
                        }
                    ]
                },
                {
                    subTypeName: "Linux",
                    subListCollect: [
                        {
                            osName: "Manjaro Arm 64位"
                        }, {
                            osName: "Ubuntu Arm 64位"
                        }, {
                            osName: "Other Linux Arm 64位"
                        },
                    ]
                }
            ]
        });
        this.__vm_SupportOsList.push({
            TypeName: "mips",
            ListCollect: [
                {
                    subTypeName: "Linux",
                    subListCollect: [
                        {
                            osName: "Manjaro Arm 64位"
                        }
                    ]
                }
            ]
        });
        this.__vm_SupportOsList.push({
            TypeName: "mips64",
            ListCollect: [
                {
                    subTypeName: "Linux",
                    subListCollect: [
                        {
                            osName: "Manjaro Arm 64位"
                        }
                    ]
                }
            ]
        });
        this.__vm_SupportOsList.push({
            TypeName: "ppc",
            ListCollect: [
                {
                    subTypeName: "Windows",
                    subListCollect: [
                        {
                            osName: "Windows 10 Arm 64位"
                        }, {
                            osName: "Windows 11 Arm 64位"
                        }, {
                            osName: "Other Windows Arm 64位"
                        }
                    ]
                },
                {
                    subTypeName: "Linux",
                    subListCollect: [
                        {
                            osName: "Manjaro Arm 64位"
                        }, {
                            osName: "Ubuntu Arm 64位"
                        }, {
                            osName: "Other Linux Arm 64位"
                        },
                    ]
                }
            ]
        });
        this.__vm_SupportOsList.push({
            TypeName: "ppc64",
            ListCollect: [
                {
                    subTypeName: "Windows",
                    subListCollect: [
                        {
                            osName: "Windows 10 Arm 64位"
                        }, {
                            osName: "Windows 11 Arm 64位"
                        }, {
                            osName: "Other Windows Arm 64位"
                        }
                    ]
                },
                {
                    subTypeName: "Linux",
                    subListCollect: [
                        {
                            osName: "Manjaro Arm 64位"
                        }, {
                            osName: "Ubuntu Arm 64位"
                        }, {
                            osName: "Other Linux Arm 64位"
                        },
                    ]
                }
            ]
        });
    },
    softwareDetectPreData() {
        let PreSoftwareDetectList = [];
        switch (this.__vm_HostPlatform) {
            case "linux":
                PreSoftwareDetectList.push({
                    PlatformType: "linux",
                    Body: [
                        {
                            path: ["/usr/bin"],
                            program: [
                                "qemu-system-x86_64",
                                "qemu-system-i386",
                                "qemu-system-arm",
                                "qemu-system-aarch64",
                                "qemu-system-mips",
                                "qemu-system-mips64",
                                "qemu-system-mips64el",
                                "qemu-system-ppc",
                                "qemu-system-ppc64",
                            ]
                        }
                    ]
                });
                break;
            case "win32":
                PreSoftwareDetectList.push({
                    PlatformType: "win32",
                    Body: [
                        {
                            path: [
                                `${this.__vm_GenTools.PathStream.join(process.env.ProgramFiles, "\\", "qemu")}`,
                            ],
                            program: [
                                "qemu-system-x86_64.exe",
                                "qemu-system-i386.exe",
                                "qemu-system-arm.exe",
                                "qemu-system-aarch64.exe",
                                "qemu-system-mips.exe",
                                "qemu-system-mips64.exe",
                                "qemu-system-mips64el.exe",
                                "qemu-system-ppc.exe",
                                "qemu-system-ppc64.exe",
                            ]
                        }
                    ]
                })
                break;
        }
        if (PreSoftwareDetectList.length === 0) {
            PreSoftwareDetectList = undefined;
        }
        return PreSoftwareDetectList;
    },
    vm_logging_controls(operate) {
        switch (String(operate).toLowerCase()) {
            case "clear":
                this.__vm_logging_Area.splice(0);
                if (this.__vm_GenTools.FileStream.existsSync(this.__vm_GenTools.PathStream.join(this.__vm_ProgramPath, "logging.json"))) {
                    this.__vm_GenTools.rmData(this.__vm_GenTools.PathStream.join(this.__vm_ProgramPath, "logging.json"));
                }
                break;
            case "dump":
                let Logging = this.__vm_GenTools.FileStream.createWriteStream(this.__vm_GenTools.PathStream.join(this.__vm_ProgramPath, "logging.json"), "utf8");
                Logging.write(JSON.stringify(this.__vm_logging_Area, null, 4));
                Logging.close();
                break;
        }
    }
}