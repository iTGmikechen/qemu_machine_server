module.exports = {
    ServiceSystem(callback) {
        console.log("虚拟机系统，正在启动");
        const Success_VERYGOOD = 9;
        const Success_MEDIUM = 8;
        const Success_GENERAL = 7;
        const Failure_DANGER = 0;
        const Failure_MEDIUM = 1;
        const Failure_GENERAL = 2;
        let mainBody = (resolve, reject) => {
            let rawConfigData = [];
            let rawHostListData = [];
            let rawOSSData = [];
            let rawSoftwareInstall = [];
            let rawFrameworkData = [];
            let OriginConfig;
            let OriginHostList;
            let OriginOSSData;
            let OriginSoftwareInstall;
            let OriginFramework;
            let OriginConfigThrow = false;
            let OriginHostListThrow = false;
            let OriginOSSDataThrow = false;
            let OriginSoftwareInstallThrow = false;
            let OriginFrameworkThrow = false;

            let softwareDataObject;
            let hostDataObject;
            let osDataObject;
            let configDataObject;
            let frameworkDataObject;

            let OneSt = false;
            let OneStep_count = 0;
            let OneStepCheck = (value) => {
                OneStep_count++;
                if (OneStep_count >= value) {
                    OneStep_count = 0;
                    OneSt = true;
                } else {
                    OneSt = false;
                }
            }

            let OneStComplete = false;
            if (this.__vm_GenTools.FileStream.existsSync(this.__vm_GenTools.PathStream.join(this.__vm_ProgramPath, "resource", "temp"))) {
                this.__vm_GenTools.rmData(this.__vm_GenTools.PathStream.join(this.__vm_ProgramPath, "resource", "temp"), "system");
                this.__vm_GenTools.createDirectory(this.__vm_GenTools.PathStream.join(this.__vm_ProgramPath, "resource", "temp"));
                this.__vm_GenTools.createDirectory(this.__vm_GenTools.PathStream.join(this.__vm_ProgramPath, "resource", "temp"), "pkg");
                this.__vm_GenTools.createDirectory(this.__vm_GenTools.PathStream.join(this.__vm_ProgramPath, "resource", "temp"), "Receive");
            } else {
                this.__vm_GenTools.createDirectory(this.__vm_GenTools.PathStream.join(this.__vm_ProgramPath, "resource", "temp"));
                this.__vm_GenTools.createDirectory(this.__vm_GenTools.PathStream.join(this.__vm_ProgramPath, "resource", "temp"), "pkg");
                this.__vm_GenTools.createDirectory(this.__vm_GenTools.PathStream.join(this.__vm_ProgramPath, "resource", "temp"), "Receive");
            }
            if (this.__vm_GenTools.FileStream.existsSync(this.__vm_GenTools.PathStream.join(this.__vm_ProgramPath, "logging.json"))) {
                let rawLogging = [];
                let Logging = this.__vm_GenTools.FileStream.createReadStream(this.__vm_GenTools.PathStream.join(this.__vm_ProgramPath, "logging.json"), "utf8");
                Logging.on("data", (vl) => {
                    rawLogging.push(vl);
                })
                Logging.on("end", () => {
                    let OriginLogging;
                    try {
                        OriginLogging = JSON.parse(rawLogging.join(""));
                        this.__vm_logging_Area.push(...OriginLogging);
                    } catch (e) {
                        this.__vm_logging_Area.splice(0);
                    }
                });
            }
            if (
                this.__vm_GenTools.FileStream.existsSync(this.__vm_GenTools.PathStream.join(this.__vm_ProgramPath, "software.json"))
                ||
                this.__vm_GenTools.FileStream.existsSync(this.__vm_GenTools.PathStream.join(this.__vm_ProgramPath, "config.json"))
                ||
                this.__vm_GenTools.FileStream.existsSync(this.__vm_GenTools.PathStream.join(this.__vm_ProgramPath, "framework.json"))
                ||
                this.__vm_GenTools.FileStream.existsSync(this.__vm_GenTools.PathStream.join(this.__vm_ProgramPath, "host.json"))
                ||
                this.__vm_GenTools.FileStream.existsSync(this.__vm_GenTools.PathStream.join(this.__vm_ProgramPath, "system.json"))
            ) {
                if (!this.__vm_GenTools.FileStream.existsSync(this.__vm_GenTools.PathStream.join(this.__vm_ProgramPath, "software.json"))) {
                    this.__vm_softwareDetectList = this.__vm_softwareDetectPreData();
                    softwareDataObject = this.__vm_GenTools.FileStream.createWriteStream(this.__vm_GenTools.PathStream.join(this.__vm_ProgramPath, "software.json"), "utf8");
                    softwareDataObject.on("error", () => {
                        OriginSoftwareInstallThrow = true;
                    });
                    softwareDataObject.write(JSON.stringify(this.__vm_softwareDetectList, null, 4), (err) => {
                        if (!err) {
                            softwareDataObject.close();
                            OneStepCheck(1);
                            OneStComplete = OneSt;
                        } else {
                            OriginSoftwareInstallThrow = true;
                        }
                    });
                } else {
                    softwareDataObject = this.__vm_GenTools.FileStream.createReadStream(this.__vm_GenTools.PathStream.join(this.__vm_ProgramPath, "software.json"), "utf8");
                    softwareDataObject.on("data", (value) => {
                        rawSoftwareInstall.push(value);
                    }).on("end", () => {
                        try {
                            OriginSoftwareInstall = JSON.parse(rawSoftwareInstall.join(""));
                            let FinalCheckData = this.__softwareDetectDataCheck(OriginSoftwareInstall);
                            if (FinalCheckData) {
                                this.__vm_softwareDetectList.push(...FinalCheckData);
                                OneStepCheck(1);
                                OneStComplete = OneSt;
                            } else {
                                throw new Error("");
                            }
                        } catch (e) {
                            OriginSoftwareInstallThrow = true;
                        }
                    }).on("error", () => {
                        OriginSoftwareInstallThrow = true;
                    });
                }
                let FullComplete = false;
                let TwoNd = () => {
                    if (!this.__vm_GenTools.FileStream.existsSync(this.__vm_GenTools.PathStream.join(this.__vm_ProgramPath, "config.json"))) {
                        try {
                            let reLoadConfig = new this.__vm_configObject();
                            if (this.__vm_GenTools.FileStream.existsSync(this.__vm_GenTools.PathStream.join(this.__vm_ProgramPath, "vmHost"))) {
                                reLoadConfig.vm_MachinePath = this.__vm_GenTools.PathStream.join(this.__vm_ProgramPath, "vmHost");
                                reLoadConfig.vm_DefaultMachinePath = this.__vm_GenTools.PathStream.join(this.__vm_ProgramPath, "vmHost");
                            } else {
                                this.__vm_GenTools.createDirectory(this.__vm_GenTools.PathStream.join(this.__vm_ProgramPath, "vmHost"));
                                reLoadConfig.vm_MachinePath = this.__vm_GenTools.PathStream.join(this.__vm_ProgramPath, "vmHost");
                                reLoadConfig.vm_DefaultMachinePath = this.__vm_GenTools.PathStream.join(this.__vm_ProgramPath, "vmHost");
                            }
                            this.__vm_MachinePath = reLoadConfig.vm_MachinePath;
                            reLoadConfig.vm_HostPlatform = this.__vm_HostPlatform;
                            this.AccessConfig = reLoadConfig.vm_AccessConfig;
                            configDataObject = this.__vm_GenTools.FileStream.createWriteStream(this.__vm_GenTools.PathStream.join(this.__vm_ProgramPath, "config.json"), "utf8");
                            configDataObject.write(JSON.stringify(reLoadConfig, null, 4), (err) => {
                                if (!err) {
                                    configDataObject.close();
                                    OneStepCheck(4);
                                    FullComplete = OneSt;
                                } else {
                                    OriginConfigThrow = true;
                                    configDataObject.close();
                                }
                            });
                        } catch (e) {
                            OriginConfigThrow = true;
                        }
                    } else {
                        configDataObject = this.__vm_GenTools.FileStream.createReadStream(this.__vm_GenTools.PathStream.join(this.__vm_ProgramPath, "config.json"), "utf8");
                        configDataObject.on("data", (value) => {
                            rawConfigData.push(value);
                        }).on("end", () => {
                            try {
                                OriginConfig = JSON.parse(rawConfigData.join(""));
                                if (OriginConfig.vm_HostPlatform === this.__vm_HostPlatform) {
                                    this.__vm_MachinePath = OriginConfig.vm_MachinePath;
                                    this.AccessConfig = OriginConfig.vm_AccessConfig;
                                    OneStepCheck(4);
                                    FullComplete = OneSt;
                                } else {
                                    throw new Error("");
                                }
                            } catch (e) {
                                OriginConfigThrow = true;
                            }
                            configDataObject.close();
                        }).on("error", () => {
                            OriginConfigThrow = true;
                            configDataObject.close();
                        });
                    }
                    if (!this.__vm_GenTools.FileStream.existsSync(this.__vm_GenTools.PathStream.join(this.__vm_ProgramPath, "framework.json"))) {
                        let reLoad = this.__FirstInitial();
                        if (reLoad) {
                            let reLoadFramework = new this.__vm_frameworkObject();
                            reLoadFramework.PlatformType = this.__vm_HostPlatform;
                            reLoadFramework.vm_Framework = this.__vm_framework;
                            frameworkDataObject = this.__vm_GenTools.FileStream.createWriteStream(this.__vm_GenTools.PathStream.join(this.__vm_ProgramPath, "framework.json"), "utf8");
                            frameworkDataObject.write(JSON.stringify(reLoadFramework, null, 4), (err) => {
                                if (!err) {
                                    frameworkDataObject.close();
                                    OneStepCheck(4);
                                    FullComplete = OneSt;
                                } else {
                                    OriginConfigThrow = true;
                                    frameworkDataObject.close();
                                }
                            });
                        } else {
                            OriginFrameworkThrow = true;
                        }
                    } else {
                        frameworkDataObject = this.__vm_GenTools.FileStream.createReadStream(this.__vm_GenTools.PathStream.join(this.__vm_ProgramPath, "framework.json"), "utf8");
                        frameworkDataObject.on("data", (value) => {
                            rawFrameworkData.push(value);
                        }).on("end", (value) => {
                            try {
                                OriginFramework = JSON.parse(rawFrameworkData.join(""));
                                if (OriginFramework.PlatformType === this.__vm_HostPlatform) {
                                    this.__vm_framework.push(...OriginFramework.vm_Framework);
                                    OneStepCheck(4);
                                    FullComplete = OneSt;
                                } else {
                                    throw new Error("");
                                }
                            } catch (e) {
                                OriginFrameworkThrow = true;
                            }
                            frameworkDataObject.close();
                        }).on("error", (value) => {
                            OriginFrameworkThrow = true;
                            frameworkDataObject.close();
                        })
                    }
                    if (!this.__vm_GenTools.FileStream.existsSync(this.__vm_GenTools.PathStream.join(this.__vm_ProgramPath, "host.json"))) {
                        hostDataObject = this.__vm_GenTools.FileStream.createWriteStream(this.__vm_GenTools.PathStream.join(this.__vm_ProgramPath, "host.json"), "utf8");
                        hostDataObject.write(JSON.stringify({hostListData: []}, null, 4), (err) => {
                            if (!err) {
                                hostDataObject.close();
                                OneStepCheck(4);
                                FullComplete = OneSt;
                            } else {
                                OriginHostListThrow = true;
                                hostDataObject.close();
                            }
                        });
                    } else {
                        hostDataObject = this.__vm_GenTools.FileStream.createReadStream(this.__vm_GenTools.PathStream.join(this.__vm_ProgramPath, "host.json"), "utf8");
                        hostDataObject.on("data", (value) => {
                            rawHostListData.push(value);
                        }).on("end", () => {
                            try {
                                OriginHostList = JSON.parse(rawHostListData.join(""));
                                if (this.__vm_GenTools.DataTypeDetect(OriginHostList.hostListData) === "arr") {
                                    this.__vm_existHost.push(...OriginHostList.hostListData);
                                    this.__vm_existHost.forEach((value, index, array) => {
                                        array[index].lastState = "close";
                                    })
                                    OneStepCheck(4);
                                    FullComplete = OneSt;
                                } else {
                                    OriginHostListThrow = true;
                                }
                            } catch (e) {
                                OriginHostListThrow = true;
                            }
                        }).on("error", () => {
                            OriginHostListThrow = true;
                        });
                    }
                    if (!this.__vm_GenTools.FileStream.existsSync(this.__vm_GenTools.PathStream.join(this.__vm_ProgramPath, "system.json"))) {
                        this.__preData();
                        osDataObject = this.__vm_GenTools.FileStream.createWriteStream(this.__vm_GenTools.PathStream.join(this.__vm_ProgramPath, "system.json"), "utf8");
                        osDataObject.write(JSON.stringify(this.__vm_SupportOsList, null, 4), (err) => {
                            if (!err) {
                                osDataObject.close();
                                OneStepCheck(4);
                                FullComplete = OneSt;
                            } else {
                                OriginOSSDataThrow = true;
                            }
                        });
                    } else {
                        osDataObject = this.__vm_GenTools.FileStream.createReadStream(this.__vm_GenTools.PathStream.join(this.__vm_ProgramPath, "system.json"), "utf8");
                        osDataObject.on("data", (value) => {
                            rawOSSData.push(value);
                        }).on("end", () => {
                            try {
                                OriginOSSData = JSON.parse(rawOSSData.join(""));
                                let FinalCheckData = this.__preDataDetect(OriginOSSData);
                                if (FinalCheckData) {
                                    this.__vm_SupportOsList.push(...FinalCheckData);
                                    OneStepCheck(4);
                                    FullComplete = OneSt;
                                } else {
                                    OriginOSSDataThrow = true;
                                }
                            } catch (e) {
                                OriginOSSDataThrow = true;
                            }
                        }).on("error", () => {
                            OriginOSSDataThrow = true;
                        });
                    }
                };
                let isExecute = false;
                let nextStep = setInterval(() => {
                    this.__readyStatus = false;
                    if (OneStComplete) {
                        if (!isExecute) {
                            TwoNd();
                            isExecute = true;
                        }
                        if (FullComplete) {
                            resolve({message: "数据已成功加载", Type: Success_VERYGOOD});
                            this.__confirmReadyStatus(1);
                            clearInterval(nextStep);
                        } else {
                            if (OriginSoftwareInstallThrow) {
                                reject({message: ['software'], Type: Failure_DANGER});
                                clearInterval(nextStep);
                            }
                            if (OriginHostListThrow) {
                                reject({message: ['host'], Type: Failure_GENERAL});
                                clearInterval(nextStep);
                            }
                            if (OriginConfigThrow) {
                                reject({message: ['config'], Type: Failure_MEDIUM});
                                clearInterval(nextStep);
                            }
                            if (OriginOSSDataThrow) {
                                reject({message: ['system'], Type: Failure_GENERAL});
                                clearInterval(nextStep);
                            }
                            if (OriginFrameworkThrow) {
                                reject({message: ['framework'], Type: Failure_DANGER});
                                clearInterval(nextStep);
                            }
                        }
                    } else {
                        if (OriginSoftwareInstallThrow) {
                            reject({message: ['software'], Type: Failure_DANGER});
                            clearInterval(nextStep);
                        }
                    }
                }, 500);
            } else {
                if (this.__vm_softwareDetectPreData()) {
                    this.__vm_softwareDetectList.push(...this.__vm_softwareDetectPreData());
                } else {
                    reject({message: "无法确定操作系统类型！", Type: Failure_DANGER});
                    return;
                }
                this.__preData();
                let initialization = this.__FirstInitial();
                if (initialization) {
                    let firstConfig = new this.__vm_configObject();
                    let frameworkCollect = new this.__vm_frameworkObject();
                    if (this.__vm_GenTools.FileStream.existsSync(this.__vm_GenTools.PathStream.join(this.__vm_ProgramPath, "vmHost"))) {
                        firstConfig.vm_MachinePath = this.__vm_GenTools.PathStream.join(this.__vm_ProgramPath, "vmHost");
                        firstConfig.vm_DefaultMachinePath = this.__vm_GenTools.PathStream.join(this.__vm_ProgramPath, "vmHost");
                    } else {
                        this.__vm_GenTools.createDirectory(this.__vm_GenTools.PathStream.join(this.__vm_ProgramPath, "vmHost"));
                        firstConfig.vm_MachinePath = this.__vm_GenTools.PathStream.join(this.__vm_ProgramPath, "vmHost");
                        firstConfig.vm_DefaultMachinePath = this.__vm_GenTools.PathStream.join(this.__vm_ProgramPath, "vmHost");
                    }
                    this.__vm_MachinePath = firstConfig.vm_MachinePath;
                    firstConfig.vm_HostPlatform = this.__vm_HostPlatform;
                    frameworkCollect.PlatformType = this.__vm_HostPlatform;
                    this.AccessConfig = firstConfig.vm_AccessConfig;
                    frameworkCollect.vm_Framework = this.__vm_framework;
                    let wholeComplete = false;
                    frameworkDataObject = this.__vm_GenTools.FileStream.createWriteStream(this.__vm_GenTools.PathStream.join(this.__vm_ProgramPath, "framework.json"), "utf8");
                    configDataObject = this.__vm_GenTools.FileStream.createWriteStream(this.__vm_GenTools.PathStream.join(this.__vm_ProgramPath, "config.json"), "utf8");
                    hostDataObject = this.__vm_GenTools.FileStream.createWriteStream(this.__vm_GenTools.PathStream.join(this.__vm_ProgramPath, "host.json"), "utf8");
                    softwareDataObject = this.__vm_GenTools.FileStream.createWriteStream(this.__vm_GenTools.PathStream.join(this.__vm_ProgramPath, "software.json"), "utf8");
                    osDataObject = this.__vm_GenTools.FileStream.createWriteStream(this.__vm_GenTools.PathStream.join(this.__vm_ProgramPath, "system.json"), "utf8");
                    frameworkDataObject.write(JSON.stringify(frameworkCollect, null, 4), (err) => {
                        if (!err) {
                            frameworkDataObject.close();
                            this.__confirmReadyStatus(5);
                            if (this.__readyStatus) {
                                wholeComplete = true;
                            }
                        }
                    });
                    configDataObject.write(JSON.stringify(firstConfig, null, 4), (err) => {
                        if (!err) {
                            configDataObject.close();
                            this.__confirmReadyStatus(5);
                            if (this.__readyStatus) {
                                wholeComplete = true;
                            }
                        }
                    });
                    hostDataObject.write(JSON.stringify({hostListData: []}, null, 4), (err) => {
                        if (!err) {
                            hostDataObject.close();
                            this.__confirmReadyStatus(5);
                            if (this.__readyStatus) {
                                wholeComplete = true;
                            }
                        }
                    });
                    softwareDataObject.write(JSON.stringify(this.__vm_softwareDetectList, null, 4), (err) => {
                        if (!err) {
                            softwareDataObject.close();
                            this.__confirmReadyStatus(5);
                            if (this.__readyStatus) {
                                wholeComplete = true;
                            }
                        }
                    });
                    osDataObject.write(JSON.stringify(this.__vm_SupportOsList, null, 4), (err) => {
                        if (!err) {
                            osDataObject.close();
                            this.__confirmReadyStatus(5);
                            if (this.__readyStatus) {
                                wholeComplete = true;
                            }
                        }
                    });
                    let waitFinish = setInterval(() => {
                        if (wholeComplete) {
                            resolve({message: "数据已成功加载", Type: Success_VERYGOOD});
                            clearInterval(waitFinish);
                        }
                    }, 100);
                } else {
                    reject({message: "初始化失败！", Type: Failure_DANGER});
                }
            }
        };
        let ExecPromise = new Promise(mainBody);
        ExecPromise.then((result) => {
            switch (result.Type) {
                case Success_VERYGOOD:
                    console.log(result.message);
                    let logNum = this.__vm_logging_Area.length;
                    this.__vm_logging_Area.push({logNumber: logNum, logContent: result.message});
                    break;
                case Success_MEDIUM:
                    break;
                case Success_GENERAL:
                    break;
            }
            console.log("虚拟机系统，完成启动");
            if (typeof callback === "function") {
                callback(result);
            }
        }).catch((result) => {
            console.log("虚拟机系统，无法启动");
            if (typeof callback === "function") {
                callback(result);
            }
            switch (result.Type) {
                case Failure_DANGER:
                    if (this.__vm_GenTools.DataTypeDetect(result.message) === "arr") {
                        result.message.forEach((v, i, a) => {
                            a[i] += ".json";
                        });
                        if (result.message.length > 1) {
                            console.error(`请删除 ${result.message.join("、")} 这些文件，然后重新启动软件。`);
                        } else {
                            console.error(`请删除 ${result.message.join("")} 文件，然后重新启动软件。`);
                        }
                    } else {
                        console.error(result.message);
                    }
                    process.exit(0);
                    break;
                case Failure_MEDIUM:
                    if (this.__vm_GenTools.DataTypeDetect(result.message) === "arr") {
                        result.message.forEach((v, i, a) => {
                            a[i] += ".json";
                        });
                        if (result.message.length > 1) {
                            console.error(`请删除 ${result.message.join("、")} 这些文件，然后重新启动软件。`);
                        } else {
                            console.error(`请删除 ${result.message.join("")} 文件，然后重新启动软件。`);
                        }
                    } else {
                        console.error(result.message);
                    }
                    process.exit(0);
                    break;
                case Failure_GENERAL:
                    if (this.__vm_GenTools.DataTypeDetect(result.message) === "arr") {
                        result.message.forEach((v, i, a) => {
                            a[i] += ".json";
                        });
                        if (result.message.length > 1) {
                            console.error(`请删除 ${result.message.join("、")} 这些文件，然后重新启动软件。`);
                        } else {
                            console.error(`请删除 ${result.message.join("")} 文件，然后重新启动软件。`);
                        }
                    } else {
                        console.error(result.message);
                    }
                    process.exit(0);
                    break;
            }
        });
    },
    vmSystem_baseInfo(req, res, next) {
        try {
            let getTechSupport = (val) => {
                let FinalValue = undefined;
                this.__vm_framework.forEach((value) => {
                    if (String(value.FrameworkType).toLowerCase() === String(val).toLowerCase()) {
                        FinalValue = value;
                    }
                });
                return FinalValue;
            };
            let getOperateSystemSupport = (val) => {
                let FinalValue = undefined;
                this.__vm_SupportOsList.forEach((value) => {
                    if (String(value.TypeName).toLowerCase() === String(val).toLowerCase()) {
                        FinalValue = value;
                    }
                });
                return FinalValue;
            }
            if (this.__readyStatus) {
                let os = require("node:os");
                let par = this.__vm_GenTools.getRequestMiddlewareData(req);
                if (par && this.__vm_GenTools.DataTypeDetect(par) === "obj") {
                    let {existHost, SupportTech, SupportOperateSystem} = par;
                    let FinalData = {};
                    if (typeof existHost === "string") {
                        FinalData.ExistsHost = [];
                        if (String(existHost).toLowerCase() === "all") {
                            FinalData.ExistsHost.push(...this.__vm_existHost);
                        } else {
                            this.__vm_existHost.forEach((value) => {
                                if (value.name === existHost || value.id === existHost) {
                                    FinalData.ExistsHost.push(value);
                                }
                            })
                        }
                    }
                    if (typeof SupportTech === "string") {
                        switch (String(SupportTech).toLowerCase()) {
                            case "x86_64":
                                FinalData.SupportTech = [getTechSupport(SupportTech)];
                                break;
                            case "i386":
                                FinalData.SupportTech = [getTechSupport(SupportTech)];
                                break;
                            case "arm":
                                FinalData.SupportTech = [getTechSupport(SupportTech)];
                                break;
                            case "aarch64":
                                FinalData.SupportTech = [getTechSupport(SupportTech)];
                                break;
                            case "all":
                                FinalData.SupportTech = this.__vm_framework;
                                break;
                            default:
                                throw new Error("SupportTech字段数据异常！");
                        }
                    }
                    if (typeof SupportOperateSystem === "string") {
                        switch (String(SupportOperateSystem).toLowerCase()) {
                            case "x86_64":
                                FinalData.SupportOperateSystem = [getOperateSystemSupport(SupportOperateSystem)];
                                break;
                            case "i386":
                                FinalData.SupportOperateSystem = [getOperateSystemSupport(SupportOperateSystem)];
                                break;
                            case "arm":
                                FinalData.SupportOperateSystem = [getOperateSystemSupport(SupportOperateSystem)];
                                break;
                            case "aarch64":
                                FinalData.SupportOperateSystem = [getOperateSystemSupport(SupportOperateSystem)];
                                break;
                            case "all":
                                FinalData.SupportOperateSystem = this.__vm_SupportOsList;
                                break;
                            default:
                                throw new Error("SupportOperateSystem字段数据异常！");
                        }
                    }
                    if (this.__vm_GenTools.emptyObjectDetect(FinalData) === false) {
                        FinalData.PhysicalHostInfo = {
                            TotalMemory: parseInt(`${os.totalmem() / 1024}`),
                            CPU_info: os.cpus(),
                            ComputerName: os.hostname(),
                            OSType: os.type(),
                            OSPlatform: os.platform(),
                            OSVersion: os.version(),
                            OSVersionNumber: os.release(),
                            architecture: os.arch(),
                        };
                        FinalData.PhysicalHostInfo.NetworkAddress = [
                            os.hostname(),
                            "localhost",
                            "0.0.0.0",
                            ...this.__vm_GenTools.getCurrentHostIPAddress("ipv4")
                        ];
                        if (this.__delayStatus) {
                            let waitProc = () => {
                                if (!this.__delayStatus) {
                                    res.send(JSON.stringify(FinalData, null, 4));
                                    res.end();
                                    clearInterval(waitDelay);
                                }
                            }
                            let waitDelay = setInterval(() => {
                                waitProc();
                            }, 100);
                        } else {
                            res.send(JSON.stringify(FinalData, null, 4));
                            res.end();
                        }
                    } else {
                        throw new Error("执行请求异常！");
                    }
                } else {
                    throw new Error("传参数据异常！");
                }
            } else {
                throw new Error("系统未准备就绪！");
            }
        } catch (e) {
            res.statusCode = 404;
            let logNum = this.__vm_logging_Area.length;
            this.__vm_logging_Area.push({logNumber: logNum, logContent: e.message});
            res.send(e.message);
            res.end();
        }
    },
    vmSystem_HostDetails_Info(req, res, next) {
        try {
            if (this.__readyStatus) {
                let dfg = this.__vm_GenTools.getRequestMiddlewareData(req);
                let {existHost} = dfg;
                try {
                    this.__vm_existHost.forEach((value) => {
                        if (value.id === existHost) {
                            throw new Error(this.__vm_GenTools.PathStream.join(value.path, `${value.name}.json`));
                        }
                    });
                } catch (e1) {
                    try {
                        let reData = this.__vm_GenTools.FileStream.readFileSync(e1.message, "utf-8");
                        res.send(reData);
                        res.end();
                    } catch (e2) {
                        throw new Error("虚拟机数据读取异常");
                    }
                }
            } else {
                throw new Error("系统未准备就绪");
            }
        } catch (e) {
            let logNum = this.__vm_logging_Area.length;
            this.__vm_logging_Area.push({logNumber: logNum, logContent: e.message});
            res.statusCode = 404;
            res.send(e.message);
            res.end();
        }
    },
    vmSystem_UI_ExtraResource(req, res, next) {
        try {
            let FinalReader;
            if (req.method === "POST" && this.__readyStatus) {
                let dfg = this.__vm_GenTools.getRequestMiddlewareData(req);
                if (this.__vm_GenTools.DataTypeDetect(dfg) === "obj" && this.__vm_GenTools.DataTypeDetect(dfg.need) === "arr") {
                    let {need} = dfg;
                    let curResPath = this.__vm_GenTools.PathStream.join(this.__vm_ProgramPath, "resource");
                    if (this.__vm_GenTools.FileStream.existsSync(curResPath)) {
                        if (typeof need[0] === 'string' && String(need[0]).toLowerCase() !== 'ui' && String(need[0]).toLowerCase() !== 'temp') {
                            let confirmResPath = this.__vm_GenTools.PathStream.join(curResPath, need[0], need[1]);
                            if (this.__vm_GenTools.FileStream.existsSync(confirmResPath)) {
                                let extName = this.__vm_GenTools.getFileNameExtName(confirmResPath);
                                let ggc = [
                                    {extName: "txt,log", type: "text/plain"},
                                    {extName: "reg", type: "text/x-ms-regedit"},
                                    {extName: "md", type: "text/markdown"},
                                    {extName: "htm,html", type: "text/html"},
                                    {extName: "js", type: "text/javascript"},
                                    {extName: "css", type: "text/css"},
                                    {extName: "scss", type: "text/x-scss"},
                                    {extName: "wma", type: "audio/x-ms-wma"},
                                    {extName: "mid", type: "audio/midi"},
                                    {extName: "mp3", type: "audio/mpeg"},
                                    {extName: "wmv", type: "video/x-ms-wmv"},
                                    {extName: "avi", type: "video/x-msvideo"},
                                    {extName: "mp4", type: "video/mp4"},
                                    {extName: "jpg,jpeg", type: "image/jpeg"},
                                    {extName: "png", type: "image/png"},
                                    {extName: "gif", type: "image/gif"},
                                    {extName: "webp", type: "image/webp"},
                                    {extName: "psd", type: "image/vnd.adobe.photoshop"},
                                    {extName: "cur", type: "image/x-win-bitmap"},
                                    {extName: "bmp", type: "image/bmp"},
                                    {extName: "ttf", type: "font/ttf"},
                                    {extName: "woff", type: "font/woff"},
                                    {extName: "woff2", type: "font/woff2"},
                                    {extName: "ttc", type: "font/collection"},
                                    {extName: "pdf", type: "application/pdf"},
                                    {extName: "doc,docx", type: "application/msword"},
                                    {extName: "zip", type: "application/zip"},
                                    {extName: "7z", type: "application/x-7z-compressed"},
                                    {extName: "exe", type: "application/x-ms-dos-executable"},
                                    {extName: "jar", type: "application/x-java-archive"},
                                    {extName: "crx", type: "application/x-chrome-extension"},
                                    {extName: "php", type: "application/x-php"},
                                    {extName: "sql", type: "application/sql"},
                                    {extName: "svg", type: "image/svg+xml"},
                                    {extName: "img", type: "application/x-raw-disk-image"},
                                ];
                                extName = String(extName).substr(1);
                                let outputDirection;
                                ggc.forEach((value, index, array) => {
                                    if (String(value.extName).includes(extName)) {
                                        if (String(value.type).includes('image')) {
                                            outputDirection = 'image';
                                        }
                                    }
                                });
                                switch (outputDirection) {
                                    case 'image':
                                        FinalReader = this.__vm_GenTools.anyDataConvertBase64(confirmResPath, {JoinTypeLabel: true});
                                        break;
                                    default:
                                        break;
                                }
                                res.send(FinalReader);
                            } else {
                                throw new Error("资源找不到！");
                            }
                        } else {
                            throw new Error("请求数据异常！");
                        }
                    } else {
                        throw new Error("资源目录不存在！");
                    }
                } else {
                    throw new Error("请求数据异常！");
                }
            } else {
                throw new Error("请求方式错误");
            }
            res.end();
        } catch (e) {
            res.statusCode = 404;
            let logNum = this.__vm_logging_Area.length;
            this.__vm_logging_Area.push({logNumber: logNum, logContent: e.message});
            res.send(e.message);
            res.end();
        }
    },
    vmSystem_softwareSetting(req, res, next) {
        try {
            let dirDetect = (path) => {
                let detectValue = [];
                try {
                    if (this.__vm_GenTools.FileStream.existsSync(path) && this.__vm_GenTools.FileStream.statSync(path).isDirectory()) {
                        detectValue.push(true);
                        let testDir = this.__vm_GenTools.FileStream.statSync(path);
                        switch (process.platform) {
                            case "linux":
                                if ((testDir.mode) < 16877 || (testDir.mode) > 20479) {
                                    detectValue.push(false);
                                } else {
                                    detectValue.push(true);
                                }
                                break;
                            case "win32":
                                if ((testDir.mode) < 16677 || (testDir.mode) > 16822) {
                                    detectValue.push(false);
                                } else {
                                    detectValue.push(true);
                                }
                                break;
                        }
                    } else {
                        detectValue.push(false);
                    }
                } catch (e) {
                    detectValue.push(false);
                }
                return detectValue;
            }
            if (this.__readyStatus) {
                switch (req.method) {
                    case "GET":
                        let ssp = this.__vm_GenTools.FileStream.readFileSync(this.__vm_GenTools.PathStream.join(this.__vm_ProgramPath, "config.json")).toString('utf-8');
                        let dataBody = JSON.parse(ssp);
                        res.send(JSON.stringify(dataBody, null, 4));
                        res.end();
                        break;
                    case "POST":
                        let Pars = this.__vm_GenTools.getRequestMiddlewareData(req);
                        if (this.__vm_GenTools.DataTypeDetect(Pars) === "obj" && this.__vm_GenTools.emptyObjectDetect(Pars) === false) {
                            let FinalObject = {};
                            for (let largeItem in Pars) {
                                switch (String(largeItem).toLowerCase()) {
                                    case "vm_machinepath":
                                        let grh = dirDetect(Pars[largeItem]);
                                        if (grh[0] === true && grh[1] === true) {
                                            FinalObject.vm_MachinePath = Pars[largeItem];
                                        } else {
                                            throw new Error("");
                                        }
                                        break;
                                    case "vm_accessconfig":
                                        if (this.__vm_GenTools.DataTypeDetect(Pars[largeItem]) === "obj") {
                                            FinalObject.vm_AccessConfig = {};
                                            for (let subItem in Pars[largeItem]) {
                                                switch (String(subItem).toLowerCase()) {
                                                    case "address":
                                                        FinalObject.vm_AccessConfig.Address = Pars[largeItem][subItem];
                                                        break;
                                                    case "port":
                                                        if (typeof Pars[largeItem][subItem] === "number" || typeof Pars[largeItem][subItem] === "string") {
                                                            let convertValue;
                                                            switch (typeof Pars[largeItem][subItem]) {
                                                                case "string":
                                                                    convertValue = parseInt(Pars[largeItem][subItem]);
                                                                    break;
                                                                case "number":
                                                                    convertValue = Pars[largeItem][subItem];
                                                                    break;
                                                            }
                                                            if (convertValue > 1 && convertValue < 65535) {
                                                                FinalObject.vm_AccessConfig.Port = convertValue;
                                                            } else {
                                                                throw new Error("");
                                                            }
                                                        } else {
                                                            throw new Error("");
                                                        }
                                                        break;
                                                    default:
                                                        throw new Error("");
                                                }
                                            }
                                        } else {
                                            throw new Error("");
                                        }
                                        break;
                                    default:
                                        throw new Error("");
                                }
                            }
                            let OriginSetting = this.__vm_GenTools.FileStream.readFileSync(this.__vm_GenTools.PathStream.join(this.__vm_ProgramPath, "config.json")).toString('utf-8');
                            let convertOriginSetting = JSON.parse(OriginSetting);
                            for (let change in FinalObject) {
                                convertOriginSetting[change] = FinalObject[change];
                            }
                            let writeConfig = this.__vm_GenTools.FileStream.createWriteStream(this.__vm_GenTools.PathStream.join(this.__vm_ProgramPath, "config.json"), "utf8");
                            writeConfig.write(JSON.stringify(convertOriginSetting, null, 4));
                            writeConfig.close((err) => {
                                if (!err) {
                                    let logNum = this.__vm_logging_Area.length;
                                    this.__vm_logging_Area.push({logNumber: logNum, logContent: "软件设置修改成功"});
                                    res.end();
                                }
                            })
                        } else {
                            throw new Error("");
                        }
                        break;
                    default:
                        throw new Error("");
                }
            } else {
                throw new Error("系统未准备就绪！");
            }
        } catch (e) {
            res.statusCode = 404;
            let logNum = this.__vm_logging_Area.length;
            this.__vm_logging_Area.push({logNumber: logNum, logContent: e.message});
            res.send(e.message);
            res.end();
        }
    },
    vmSystem_ioData(req, res, next) {
        let ioProc = (resolve, reject) => {
            let ReceivePath = this.__vm_GenTools.PathStream.join(this.__vm_ProgramPath, "resource", "temp", "Receive");
            let tmpSavePath = this.__vm_GenTools.PathStream.join(this.__vm_ProgramPath, "resource", "temp", "pkg");
            let FinalValue;
            switch (String(req.method).toLowerCase()) {
                case "get":
                    let Par = this.__vm_GenTools.getRequestMiddlewareData(req);
                    if (this.__vm_GenTools.DataTypeDetect(Par) === "obj" && Par.needExport !== undefined) {
                        let {needExport} = Par;
                        let vmDirPath;
                        let existVM = false;
                        let curVM_dirs;
                        this.__vm_existHost.forEach((value, index, array) => {
                            if (value.id === needExport) {
                                existVM = true;
                                vmDirPath = value.path;
                            }
                        });
                        if (existVM) {
                            curVM_dirs = this.__vm_GenTools.getFileOnDirectoryLocation(vmDirPath);
                            let vmDirsName = this.__vm_GenTools.getFullPathFileOrDirectoryName(vmDirPath);
                            let compress;
                            switch (this.__vm_HostPlatform) {
                                case "linux":
                                    compress = this.__ExtraChildSystem.exec(
                                        `zip ${this.__vm_GenTools.PathStream.join(tmpSavePath, `${vmDirsName}.zip`)} ${vmDirsName}/** -r -X -D`,
                                        {cwd: curVM_dirs});
                                    break;
                                case "win32":
                                    compress = this.__ExtraChildSystem.exec(
                                        `cscript ${this.__vm_ProgramPath}\\bin\\cc.vbs -c ${this.__vm_GenTools.PathStream.join(tmpSavePath, `${vmDirsName}.zip`)} ${vmDirPath}`,
                                        {cwd: curVM_dirs});
                                    break;
                                default:
                                    throw new Error("未支持该操作系统平台实现打包导出！");
                            }
                            compress.on("exit", (vl) => {
                                switch (vl) {
                                    case 0:
                                        FinalValue = {type: "download", content: ""};
                                        FinalValue.content = this.__vm_GenTools.PathStream.join(tmpSavePath, `${vmDirsName}.zip`);
                                        resolve(FinalValue);
                                        break;
                                    default:
                                        throw new Error(vl);
                                }
                            }).on("error", (vl) => {
                                throw new Error(vl);
                            });
                        } else {
                            reject("不存在需要导出的虚拟机！");
                        }
                    } else {
                        reject("传递参数异常！");
                    }
                    break;
                case "post":
                    let receiveDetect = 0;
                    let lastReceiveDetect = 0;
                    let detectControls = setInterval(() => {
                        if (receiveDetect > lastReceiveDetect) {
                            lastReceiveDetect = receiveDetect;
                        } else {
                            clearInterval(detectControls);
                            reject("");
                        }
                    }, 1000);
                    let ReceiveFileName;
                    if (!this.__vm_GenTools.FileStream.existsSync(ReceivePath)) {
                        this.__vm_GenTools.createDirectory(ReceivePath);
                    }
                    this.__vm_GenTools.webServerDataUpdate(req, {
                        SaveLocation: ReceivePath,
                        FileName: (OriginMsg, confirm) => {
                            ReceiveFileName = OriginMsg.FileName;
                            confirm(OriginMsg.FileName);
                        },
                        before_Receive: () => {

                        },
                        running_Receive: (ReceiveFileName, ReceiveFileSize) => {
                            receiveDetect += ReceiveFileSize;
                        },
                        complete_Receive: () => {
                            FinalValue = {type: "upload", content: ""}
                            FinalValue.content = this.__vm_GenTools.PathStream.join(ReceivePath, ReceiveFileName);
                            resolve(FinalValue);
                        }
                    });
                    break;
                default:
                    reject("请求方式异常！");
                    break;
            }
        }
        let ioPromise = new Promise(ioProc);
        ioPromise.then((val) => {
            let cleanReceive = () => {
                let tmpDir = this.__vm_GenTools.PathStream.join(this.__vm_ProgramPath, "resource", "temp", "Receive");
                if (this.__vm_GenTools.FileStream.existsSync(tmpDir)) {
                    this.__vm_GenTools.rmData(tmpDir,"system");
                    if (!this.__vm_GenTools.FileStream.existsSync(tmpDir)) {
                        this.__vm_GenTools.createDirectory(tmpDir);
                    }
                } else {
                    this.__vm_GenTools.createDirectory(tmpDir);
                }
            }
            if (val.type === "download") {
                let FileNames = this.__vm_GenTools.getFullPathFileOrDirectoryName(val.content);
                let downloadInfoBody = this.__downloadResponseHeaderInfo(FileNames);
                res.writeHead(200, downloadInfoBody.body);
                let sendFileStream = this.__vm_GenTools.FileStream.createReadStream(val.content, downloadInfoBody.format);
                sendFileStream.on("data", (vl) => {
                    res.write(vl, downloadInfoBody.format);
                }).on("end", () => {
                    sendFileStream.close((err) => {
                        this.__vm_GenTools.rmData(val.content);
                        let logNum = this.__vm_logging_Area.length;
                        this.__vm_logging_Area.push({logNumber: logNum, logContent: "虚拟机导出成功"});
                        res.end();
                    });
                });
            } else if (val.type === "upload") {
                let HostDataDir = this.__vm_GenTools.PathStream.join(this.__vm_ProgramPath, "resource", "temp", "Receive");
                let uncompress;
                switch (this.__vm_HostPlatform) {
                    case "linux":
                        uncompress = this.__ExtraChildSystem.exec(`unzip ${this.__vm_GenTools.getFullPathFileOrDirectoryName(val.content)}`, {cwd: HostDataDir});
                        break;
                    case "win32":
                        uncompress = this.__ExtraChildSystem.exec(`cscript ${this.__vm_ProgramPath}\\bin\\cc.vbs -d ${val.content} ${HostDataDir}`, {cwd: HostDataDir});
                        break;
                }
                uncompress.on("exit", (vl) => {
                    switch (vl) {
                        case 0:
                            let tmpDirList = this.__vm_GenTools.FileStream.readdirSync(HostDataDir);
                            let FinalConfirmPath;
                            let FinalConfirmProfile;
                            if (tmpDirList.length > 0) {
                                for (let item in tmpDirList) {
                                    let tmpGroupPath = this.__vm_GenTools.PathStream.join(HostDataDir, tmpDirList[item]);
                                    let chkPoint = this.__vm_GenTools.FileStream.statSync(tmpGroupPath);
                                    if (chkPoint.isDirectory()) {
                                        let detProfileList = this.__vm_GenTools.FileStream.readdirSync(tmpGroupPath);
                                        if (detProfileList.length > 0) {
                                            let jsonFileCount = 0;
                                            for (let seek in detProfileList) {
                                                let getExtNames = this.__vm_GenTools.getFileNameExtName(String(detProfileList[seek]).toString());
                                                if (String(getExtNames).toLowerCase().includes("json")) {
                                                    jsonFileCount++;
                                                    FinalConfirmProfile = String(detProfileList[seek]).toString();
                                                    FinalConfirmPath = tmpGroupPath;
                                                }
                                            }
                                            if (jsonFileCount === 1) {
                                                try {
                                                    let rawContent = this.__vm_GenTools.FileStream.readFileSync(this.__vm_GenTools.PathStream.join(FinalConfirmPath, FinalConfirmProfile));
                                                    let jsonFileContent = JSON.parse(rawContent);
                                                    let detValue = this.__importProFileCheck(jsonFileContent);
                                                    if (detValue) {
                                                        if (
                                                            this.__vm_GenTools.getFullPathFileOrDirectoryName(FinalConfirmPath) === detValue.id
                                                            &&
                                                            String(FinalConfirmProfile).includes(detValue.name)
                                                        ) {
                                                            let ExistVM = false;
                                                            this.__vm_existHost.forEach((value, index, array) => {
                                                                if (value.id === detValue.id) {
                                                                    ExistVM = true;
                                                                }
                                                            });
                                                            if (!ExistVM) {
                                                                this.__vm_GenTools.FileStream.cpSync(
                                                                    FinalConfirmPath,
                                                                    this.__vm_GenTools.PathStream.join(this.__vm_MachinePath, tmpDirList[item]),
                                                                    {
                                                                        force: true,
                                                                        recursive: true
                                                                    }
                                                                );
                                                                let joinVM_info = new this.__vm_HostItemObject();
                                                                joinVM_info.path = this.__vm_GenTools.PathStream.join(this.__vm_MachinePath, tmpDirList[item]);
                                                                joinVM_info.id = detValue.id;
                                                                joinVM_info.name = detValue.name;
                                                                joinVM_info.framework = detValue.framework;
                                                                this.__vm_existHost.push(joinVM_info);
                                                                this.__vm_updateHostData();
                                                                let logNum = this.__vm_logging_Area.length;
                                                                this.__vm_logging_Area.push({
                                                                    logNumber: logNum,
                                                                    logContent: `虚拟机 ${detValue.name} 导入成功`
                                                                });
                                                                res.send(`虚拟机 ${detValue.name} 导入成功`);
                                                            } else {
                                                                res.statusCode = 404;
                                                                let logNum = this.__vm_logging_Area.length;
                                                                this.__vm_logging_Area.push({
                                                                    logNumber: logNum,
                                                                    logContent: "存在相同的虚拟机"
                                                                });
                                                            }
                                                        } else {
                                                            res.statusCode = 404;
                                                            let logNum = this.__vm_logging_Area.length;
                                                            this.__vm_logging_Area.push({
                                                                logNumber: logNum,
                                                                logContent: "导入的虚拟机数据异常"
                                                            });
                                                        }
                                                    } else {
                                                        res.statusCode = 404;
                                                        let logNum = this.__vm_logging_Area.length;
                                                        this.__vm_logging_Area.push({
                                                            logNumber: logNum,
                                                            logContent: "导入的虚拟机数据异常"
                                                        });
                                                    }
                                                } catch (e) {
                                                    res.statusCode = 404;
                                                    let logNum = this.__vm_logging_Area.length;
                                                    this.__vm_logging_Area.push({
                                                        logNumber: logNum,
                                                        logContent: "导入的虚拟机数据异常"
                                                    });
                                                }
                                                break;
                                            } else {
                                                res.statusCode = 404;
                                                let logNum = this.__vm_logging_Area.length;
                                                this.__vm_logging_Area.push({
                                                    logNumber: logNum,
                                                    logContent: "无法确认虚拟机的配置数据"
                                                });
                                                break;
                                            }
                                        } else {
                                            res.statusCode = 404;
                                            let logNum = this.__vm_logging_Area.length;
                                            this.__vm_logging_Area.push({
                                                logNumber: logNum,
                                                logContent: "未检测到有效的虚拟机数据"
                                            });
                                        }
                                    }
                                }
                            }
                            break;
                        default:
                            res.statusCode = 404;
                            let logNum = this.__vm_logging_Area.length;
                            this.__vm_logging_Area.push({logNumber: logNum, logContent: "虚拟机导入失败"});
                            break;
                    }
                    res.end();
                    cleanReceive();
                })
            }
        }).catch((val) => {
            res.statusCode = 404;
            let logNum = this.__vm_logging_Area.length;
            this.__vm_logging_Area.push({logNumber: logNum, logContent: val});
            res.send(val);
            res.end();
        });
    },
    vmSystem_Logging(req, res, next) {
        try {
            if (req.method === "GET") {
                let Par = this.__vm_GenTools.getRequestMiddlewareData(req);
                if (this.__vm_GenTools.DataTypeDetect(Par) === "obj" && this.__vm_GenTools.emptyObjectDetect(Par) === false) {
                    let {ops} = Par;
                    switch (String(ops).toLowerCase()) {
                        case "look":
                            res.send(JSON.stringify(this.__vm_logging_Area, null, 4));
                            res.end();
                            break;
                        case "len":
                            let len = this.__vm_logging_Area.length;
                            res.send(JSON.stringify({len: len}, null, 4));
                            res.end();
                            break;
                        case "export":
                            let dg = this.__downloadResponseHeaderInfo("logs.log");
                            res.writeHead(200, dg.body);
                            res.write(JSON.stringify(this.__vm_logging_Area, null, 4), dg.format);
                            res.end();
                            break;
                        case "clear":
                            this.__vm_logging_controls("clear");
                            res.end();
                            break;
                        case "newest":
                            let last = this.__vm_logging_Area.length - 1;
                            res.send(JSON.stringify(this.__vm_logging_Area[last], null, 4));
                            res.end();
                            break;
                        case "dump":
                            this.__vm_logging_controls("dump");
                            res.end();
                            break;
                        default:
                            throw new Error("指令异常");
                    }
                } else {
                    throw new Error("参数异常");
                }
            } else {
                throw new Error("请求方式异常");
            }
        } catch (e) {
            res.statusCode = 404;
            let logNum = this.__vm_logging_Area.length;
            this.__vm_logging_Area.push({logNumber: logNum, logContent: e.message});
            res.send(e.message);
            res.end();
        }
    }
}