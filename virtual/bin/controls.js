module.exports = {
    vmSystem_create(req, res, next) {
        try {
            let createProcess = (dataObjs) => {
                let writeControls;
                if (this.__vm_GenTools.FileStream.existsSync(this.__vm_GenTools.PathStream.join(this.__vm_MachinePath, dataObjs.id))) {
                    throw new Error("存在相同的目录！");
                } else {
                    let hostListBody = new this.__vm_HostItemObject();
                    hostListBody.framework = dataObjs.framework;
                    hostListBody.name = dataObjs.name;
                    hostListBody.id = dataObjs.id;
                    hostListBody.path = this.__vm_GenTools.PathStream.join(this.__vm_MachinePath, dataObjs.id);
                    this.__vm_GenTools.createDirectory(hostListBody.path);
                    this.__afterProcess(dataObjs);
                    writeControls = this.__vm_GenTools.FileStream.createWriteStream(this.__vm_GenTools.PathStream.join(hostListBody.path, `${dataObjs.name}.json`), "utf-8");
                    writeControls.write(JSON.stringify(dataObjs, null, 4), (err) => {
                        if (!err) {
                            writeControls.close();
                            writeControls = undefined;
                        }
                    });
                    this.__vm_existHost.push(hostListBody);
                    this.__vm_updateHostData();
                }
            }
            if (req.method === "POST" && this.__readyStatus) {
                let requestData = this.__vm_GenTools.getRequestMiddlewareData(req);
                let confirmData = this.__detectData(requestData);
                if (this.__vm_GenTools.DataTypeDetect(requestData) === "obj" && this.__vm_GenTools.emptyObjectDetect(requestData) === false && confirmData) {
                    confirmData.id = this.__vm_GenTools.randomStringMakerEx({
                        BuilderLength: 32,
                        EnableUpperEng: true,
                        EnableLowerEng: true,
                        EnableNumberText: true
                    });
                    if (this.__vm_existHost.length > 0) {
                        let exist = false;
                        this.__vm_existHost.forEach((v10, i10, a10) => {
                            if (v10.name === confirmData.name) {
                                exist = true;
                            }
                        });
                        if (!exist) {
                            createProcess(confirmData);
                        } else {
                            throw new Error("已存在相同的虚拟机！");
                        }
                    } else {
                        createProcess(confirmData);
                    }
                } else {
                    throw new Error("传参数据异常！");
                }
                let logNum = this.__vm_logging_Area.length;
                this.__vm_logging_Area.push({logNumber: logNum, logContent: `虚拟机 ${confirmData.name} 创建完成`});
            } else {
                throw new Error("系统未准备就绪或请求方式异常！");
            }
            res.end();
        } catch (e) {
            res.statusCode = 404;
            let logNum = this.__vm_logging_Area.length;
            this.__vm_logging_Area.push({logNumber: logNum, logContent: e.message});
            res.send(e.message);
            res.end();
        }
    },
    vmSystem_modify(req, res, next) {
        try {
            if (req.method === "POST" && this.__readyStatus) {
                let requestData = this.__vm_GenTools.getRequestMiddlewareData(req);
                if (this.__vm_GenTools.DataTypeDetect(requestData) === "obj") {
                    let {id} = requestData;
                    this.__vm_existHost.forEach((value, index, array) => {
                        if (id === value.id) {
                            if (value.controls === undefined) {
                                let verifyData = this.__detectData(requestData);
                                if (verifyData) {
                                    verifyData.id = id;
                                    this.__afterProcess(verifyData);
                                    let WriteSave = this.__vm_GenTools.FileStream.createWriteStream(this.__vm_GenTools.PathStream.join(value.path, `${value.name}.json`), "utf-8");
                                    WriteSave.write(JSON.stringify(verifyData, null, 4));
                                    WriteSave.close((err) => {
                                        if (!err) {
                                            this.__vm_GenTools.FileStream.renameSync(this.__vm_GenTools.PathStream.join(value.path, `${value.name}.json`), this.__vm_GenTools.PathStream.join(value.path, `${verifyData.name}.json`));
                                            array[index].name = verifyData.name;
                                            array[index].framework = verifyData.framework;
                                            array[index].path = this.__vm_GenTools.PathStream.join(this.__vm_MachinePath, `${verifyData.id}`);
                                            this.__vm_updateHostData();
                                            let logNum = this.__vm_logging_Area.length;
                                            this.__vm_logging_Area.push({
                                                logNumber: logNum,
                                                logContent: `虚拟机 ${verifyData.name} 修改完成`
                                            });
                                        }
                                    });
                                } else {
                                    throw new Error("需要更新的虚拟机配置数据异常！");
                                }
                            } else {
                                throw new Error("虚拟机正在运行，无法修改配置！");
                            }
                        }
                    });
                } else {
                    throw new Error("传参数据异常！");
                }
            } else {
                throw new Error("系统未准备就绪或请求方式异常！");
            }
            res.end();
        } catch (e) {
            res.statusCode = 404;
            let logNum = this.__vm_logging_Area.length;
            this.__vm_logging_Area.push({logNumber: logNum, logContent: e.message});
            res.send(e.message);
            res.end();
        }
    },
    vmSystem_remove(req, res, next) {
        try {
            if (req.method === "GET" && this.__readyStatus) {
                let needRemovePar = this.__vm_GenTools.getRequestMiddlewareData(req);
                if (this.__vm_GenTools.DataTypeDetect(needRemovePar) === "obj") {
                    let {id, all} = needRemovePar;
                    if (String(all).toLowerCase() === "true") {
                        all = true;
                    } else {
                        all = false;
                    }
                    let confirmRemovePath;
                    let confirm_VM_name;
                    this.__vm_existHost.forEach((value, index, array) => {
                        if (value.id === id) {
                            confirm_VM_name = value.name;
                            if (typeof value.pid === "undefined") {
                                if (all) {
                                    confirmRemovePath = value.path;
                                    array.splice(index, 1);
                                } else {
                                    array.splice(index, 1);
                                }
                            } else {
                                throw new Error("Virtual Machine is Running, Please Stop Virtual Machine Running Bofore Execute Remove")
                            }
                            if (all && confirmRemovePath !== undefined) {
                                this.__vm_GenTools.rmData(confirmRemovePath);
                            }
                            this.__vm_updateHostData();
                            let logNum = this.__vm_logging_Area.length;
                            this.__vm_logging_Area.push({
                                logNumber: logNum,
                                logContent: `虚拟机 ${confirm_VM_name} 删除完成`
                            });
                        }
                    });
                } else {
                    throw new Error("传参数据异常！");
                }
            } else {
                throw new Error("系统未准备就绪或请求方式异常！");
            }
            res.end();
        } catch (e) {
            res.statusCode = 404;
            let logNum = this.__vm_logging_Area.length;
            this.__vm_logging_Area.push({logNumber: logNum, logContent: e.message});
            res.send(e.message);
            res.end();
        }
    },
    vmSystem_start(req, res, next) {
        try {
            if (this.__readyStatus) {
                let dataParam = this.__vm_GenTools.getRequestMiddlewareData(req);
                if (this.__vm_GenTools.DataTypeDetect(dataParam) === "obj" && Reflect.ownKeys(dataParam).length > 0) {
                    let {name, id} = dataParam;
                    let initVM_data, vm_id;
                    this.__vm_existHost.forEach((value, index, array) => {
                        if (value.name === name || id === value.id) {
                            vm_id = value.id;
                            initVM_data = this.__vm_GenTools.FileStream.readFileSync(this.__vm_GenTools.PathStream.join(value.path, `${value.name}.json`)).toString("utf-8");
                            let convertData = JSON.parse(initVM_data);
                            let ExecParam = [];
                            let smpColl = [];
                            let bootColl = [];
                            let rtcPro = [];
                            let vtsColl = [];
                            for (let initItem in convertData) {
                                switch (initItem) {
                                    case "name":
                                        if (String(convertData[initItem]).includes(" ")) {
                                            ExecParam.push(`-name "${convertData[initItem]}"`);
                                        } else {
                                            ExecParam.push(`-name ${convertData[initItem]}`);
                                        }
                                        break;
                                    case "model":
                                        ExecParam.push(`-M ${convertData[initItem]}`);
                                        break;
                                    case "kvm":
                                        if (convertData[initItem]) {
                                            ExecParam.push("-enable-kvm");
                                        }
                                        break;
                                    case "fips":
                                        if (convertData[initItem]) {
                                            ExecParam.push("-enable-fips");
                                        }
                                        break;
                                    case "cpu":
                                        ExecParam.push(`-cpu ${convertData[initItem]}`);
                                        break;
                                    case "gpu":
                                        ExecParam.push(`-vga ${convertData[initItem]}`);
                                        break;
                                    case "mem":
                                        ExecParam.push(`-m ${convertData[initItem]}`);
                                        break;
                                    case "monitor":
                                        if (this.__vm_GenTools.DataTypeDetect(convertData[initItem]) === "obj") {
                                            let {gl} = convertData[initItem];
                                            for (let displayItem in convertData[initItem]) {
                                                switch (String(displayItem).toLowerCase()) {
                                                    case "display":
                                                        if (gl !== undefined) {
                                                            if (String(convertData[initItem][displayItem]).toLowerCase() === "sdl") {
                                                                if (gl) {
                                                                    ExecParam.push(`-display ${convertData[initItem][displayItem]},gl=on`);
                                                                } else {
                                                                    ExecParam.push(`-display ${convertData[initItem][displayItem]},gl=off`);
                                                                }
                                                            } else {
                                                                ExecParam.push(`-display ${convertData[initItem][displayItem]}`);
                                                            }
                                                        } else {
                                                            ExecParam.push(`-display ${convertData[initItem][displayItem]}`);
                                                        }
                                                        break;
                                                    case "fullscreen":
                                                        if (
                                                            this.__vm_GenTools.DataTypeDetect(convertData[initItem][displayItem]) === "boo"
                                                            &&
                                                            convertData[initItem][displayItem]
                                                        ) {
                                                            ExecParam.push(`-full-screen`);
                                                        }
                                                        break;
                                                }
                                            }
                                        } else if (this.__vm_GenTools.DataTypeDetect(convertData[initItem]) === "str") {
                                            ExecParam.push(`-display ${convertData[initItem]}`);
                                        }
                                        break;
                                    case "sound":
                                        ExecParam.push(`-soundhw ${convertData[initItem]}`);
                                        break;
                                    case "usb":
                                        let usbEnable = false;
                                        for (let detItem in convertData[initItem]) {
                                            if (String(detItem).toLowerCase() === "usb") {
                                                if (convertData[initItem][detItem]) {
                                                    ExecParam.push("-usb");
                                                    usbEnable = true;
                                                }
                                            } else {
                                                if (usbEnable && convertData[initItem][detItem]) {
                                                    ExecParam.push(`-usbdevice ${detItem}`);
                                                }
                                            }
                                        }
                                        break;
                                    case "efi":
                                        ExecParam.push(`-pflash ${convertData[initItem]}`);
                                        break;
                                    case "hardware":
                                        convertData[initItem].forEach((vx, ix, ax) => {
                                            if (String(vx.Operate_Item).includes("hd")
                                                ||
                                                String(vx.Operate_Item).includes("fd")
                                                ||
                                                String(vx.Operate_Item).includes("cd")
                                                ||
                                                String(vx.Operate_Item).includes("-pflash")) {
                                                if (this.__vm_GenTools.FileStream.existsSync(vx.Operate_Value)) {
                                                    ExecParam.push(vx.Operate_Item);
                                                    if (String(vx.Operate_Value).indexOf(" ") > -1 && this.__vm_HostPlatform === 'win32') {
                                                        ExecParam.push(`"${vx.Operate_Value}"`);
                                                    } else {
                                                        ExecParam.push(vx.Operate_Value);
                                                    }
                                                } else {
                                                    if (this.__vm_GenTools.FileStream.existsSync(this.__vm_GenTools.PathStream.join(this.__vm_MachinePath, value.id, vx.Operate_Value))) {
                                                        ExecParam.push(vx.Operate_Item);
                                                        let FinalPath = this.__vm_GenTools.PathStream.join(this.__vm_MachinePath, value.id, vx.Operate_Value);
                                                        if (String(vx.Operate_Value).indexOf(" ") > -1) {
                                                            ExecParam.push(`"${FinalPath}"`);
                                                        } else {
                                                            ExecParam.push(FinalPath);
                                                        }
                                                    } else {
                                                        throw new Error(`${vx.Operate_Item} 找不到资源`);
                                                    }
                                                }
                                            } else {
                                                switch (String(vx.DeviceType).toLowerCase()) {
                                                    case "drive":
                                                        ExecParam.push(vx.Operate_Item);
                                                        let gro = [];
                                                        for (let joinItem in vx.Operate_Value) {
                                                            switch (String(joinItem).toLowerCase()) {
                                                                case "file":
                                                                    if (this.__vm_GenTools.FileStream.existsSync(vx.Operate_Value[joinItem])) {
                                                                        if (String(vx.Operate_Value).indexOf(" ") > -1) {
                                                                            gro.push(`${joinItem}="${vx.Operate_Value[joinItem]}"`);
                                                                        } else {
                                                                            gro.push(`${joinItem}=${vx.Operate_Value[joinItem]}`);
                                                                        }
                                                                    } else {
                                                                        if (this.__vm_GenTools.FileStream.existsSync(this.__vm_GenTools.PathStream.join(value.path, vx.Operate_Value[joinItem]))) {
                                                                            if (String(vx.Operate_Value).indexOf(" ") > -1) {
                                                                                gro.push(`${joinItem}="${this.__vm_GenTools.PathStream.join(value.path, vx.Operate_Value[joinItem])}"`);
                                                                            } else {
                                                                                gro.push(`${joinItem}=${this.__vm_GenTools.PathStream.join(value.path, vx.Operate_Value[joinItem])}`);
                                                                            }
                                                                        } else {
                                                                            throw new Error(`${vx.Operate_Item} 找不到资源`);
                                                                        }
                                                                    }
                                                                    break;
                                                                default:
                                                                    gro.push(`${joinItem}=${vx.Operate_Value[joinItem]}`);
                                                                    break;
                                                            }
                                                        }
                                                        ExecParam.push(gro.join(","));
                                                        break;
                                                    case "device":
                                                        let tmpDevice = [vx.Operate_Value];
                                                        ExecParam.push(vx.Operate_Item);
                                                        if (typeof vx.Extra_Param === 'string' && String(vx.Extra_Param).length > 0) {
                                                            tmpDevice.push(vx.Extra_Param);
                                                            ExecParam.push(tmpDevice.join(","));
                                                        } else {
                                                            ExecParam.push(vx.Operate_Value);
                                                        }
                                                        break;
                                                    case "custom":
                                                        if (typeof vx.Operate_Item === 'string' && String(vx.Operate_Item).length > 0) {
                                                            ExecParam.push(vx.Operate_Item);
                                                        }
                                                        if (typeof vx.Operate_Value === 'string' && String(vx.Operate_Value).length > 0) {
                                                            ExecParam.push(vx.Operate_Value);
                                                        }
                                                        break;
                                                }
                                            }
                                        });
                                        break;
                                    case "smp":
                                        switch (this.__vm_GenTools.DataTypeDetect(convertData[initItem])) {
                                            case "num":
                                                ExecParam.push(`-smp ${parseInt(convertData[initItem])}`);
                                                break;
                                            case "obj":
                                                for (let nums in convertData[initItem]) {
                                                    smpColl.push(`${String(nums).toLowerCase()}=${parseInt(convertData[initItem][nums])}`);
                                                }
                                                break;
                                        }
                                        break;
                                    case "boot":
                                        if (this.__vm_GenTools.DataTypeDetect(convertData[initItem]) === "str") {
                                            ExecParam.push(`-boot ${convertData[initItem]}`);
                                        } else if (this.__vm_GenTools.DataTypeDetect(convertData[initItem]) === "obj") {
                                            for (let nums in convertData[initItem]) {
                                                switch (String(nums).toLowerCase()) {
                                                    case "menu":
                                                        if (convertData[initItem][nums]) {
                                                            bootColl.push(`${String(nums).toLowerCase()}=on`);
                                                        } else {
                                                            bootColl.push(`${String(nums).toLowerCase()}=off`);
                                                        }
                                                        break;
                                                    case "strict":
                                                        if (convertData[initItem][nums]) {
                                                            bootColl.push(`${String(nums).toLowerCase()}=on`);
                                                        } else {
                                                            bootColl.push(`${String(nums).toLowerCase()}=off`);
                                                        }
                                                        break;
                                                    case "splash":
                                                        if (String(convertData[initItem][nums]) !== "") {
                                                            bootColl.push(`${String(nums).toLowerCase()}=${convertData[initItem][nums]}`);
                                                        }
                                                        break;
                                                    default:
                                                        bootColl.push(`${String(nums).toLowerCase()}=${convertData[initItem][nums]}`);
                                                        break;
                                                }
                                            }
                                        }
                                        break;
                                    case "vts":
                                        if (this.__vm_GenTools.DataTypeDetect(convertData[initItem]) === "str") {
                                            ExecParam.push(`-accel ${convertData[initItem]}`);
                                        } else if (this.__vm_GenTools.DataTypeDetect(convertData[initItem]) === "obj") {
                                            for (let nums in convertData[initItem]) {
                                                vtsColl.push(`${String(nums).toLowerCase()}=${convertData[initItem][nums]}`);
                                            }
                                        }
                                        break;
                                    case "rtc":
                                        if (this.__vm_GenTools.DataTypeDetect(convertData[initItem]) === "str") {
                                            ExecParam.push(`-rtc base=${convertData[initItem]}`);
                                        } else if (this.__vm_GenTools.DataTypeDetect(convertData[initItem]) === "obj") {
                                            for (let nums in convertData[initItem]) {
                                                rtcPro.push(`${String(nums).toLowerCase()}=${convertData[initItem][nums]}`);
                                            }
                                        }
                                        break;
                                }
                            }
                            if (smpColl.length > 0) {
                                ExecParam.unshift(`-smp ${smpColl.join(",")}`);
                            }
                            if (bootColl.length > 0) {
                                ExecParam.unshift(`-boot ${bootColl.join(",")}`);
                            }
                            if (rtcPro.length > 0) {
                                ExecParam.unshift(`-rtc ${rtcPro.join(",")}`);
                            }
                            if (vtsColl.length > 0) {
                                ExecParam.unshift(`-accel ${vtsColl.join(",")}`);
                            }
                            this.__vm_framework.forEach((v9, i9, a9) => {
                                if (String(v9.FrameworkType).toLowerCase().includes(convertData.framework)) {
                                    switch (this.__vm_HostPlatform) {
                                        case "linux":
                                            ExecParam.unshift(v9.ProgramPath);
                                            break;
                                        case "win32":
                                            ExecParam.unshift(`"${v9.ProgramPath}"`);
                                            break;
                                    }
                                }
                            });
                            let logNum = this.__vm_logging_Area.length;
                            let executeLine;
                            switch (this.__vm_HostPlatform) {
                                case "linux":
                                    executeLine = `Execute:\n${ExecParam.join(" ")}`;
                                    break;
                                case "win32":
                                    executeLine = `Execute:\r\n${ExecParam.join(" ")}`;
                                    break;
                            }
                            this.__vm_logging_Area.push({
                                logNumber: logNum,
                                logContent: executeLine
                            });
                            if (this.__vm_GenTools.DataTypeDetect(array[index].controls) === "und") {
                                array[index].controls = this.__ExtraChildSystem.exec(ExecParam.join(" "), {cwd: value.path});
                                array[index].controls.on("exit", (vl) => {
                                    switch (vl) {
                                        case 0:
                                            array[index].lastState = "close";
                                            this.__stopDelay(2);
                                            logNum = this.__vm_logging_Area.length;
                                            this.__vm_logging_Area.push({
                                                logNumber: logNum,
                                                logContent: `虚拟机 ${array[index].name} 关闭成功`
                                            });
                                            break;
                                        default:
                                            array[index].lastState = "aborted";
                                            logNum = this.__vm_logging_Area.length;
                                            this.__vm_logging_Area.push({
                                                logNumber: logNum,
                                                logContent: `虚拟机 ${array[index].name} 异常退出`
                                            });
                                            this.__stopDelay(2);
                                            break;
                                    }
                                    array[index].controls = undefined;
                                    if (!this.__delayStatus) {
                                        this.__vm_updateHostData();
                                    } else {
                                        this.__stopDelay(2);
                                    }
                                });
                                array[index].lastState = "running";
                                this.__vm_updateHostData();
                                let logNum = this.__vm_logging_Area.length;
                                this.__vm_logging_Area.push({
                                    logNumber: logNum,
                                    logContent: `虚拟机 ${array[index].name} 启动成功`
                                });
                                res.end();
                            } else {
                                throw new Error(`虚拟机 ${array[index].name} 正在运行，不能重复启动`);
                            }
                        }
                    });
                } else {
                    throw new Error("未获取到传参数据！");
                }
            } else {
                throw new Error("系统未准备就绪！");
            }
        } catch (e) {
            res.statusCode = 404;
            let logNum = this.__vm_logging_Area.length;
            this.__vm_logging_Area.push({logNumber: logNum, logContent: e.message});
            res.send(e.message);
            res.end();
        }
    },
    vmSystem_stop(req, res, next) {
        try {
            if (req.method === "GET" && this.__readyStatus) {
                let needStopPar = this.__vm_GenTools.getRequestMiddlewareData(req);
                if (this.__vm_GenTools.DataTypeDetect(needStopPar) === "obj") {
                    let {id} = needStopPar;
                    this.__vm_existHost.forEach((value, index, array) => {
                        if (value.id === id) {
                            if (this.__vm_GenTools.DataTypeDetect(value.controls) === "obj") {
                                switch (this.__vm_HostPlatform) {
                                    case"win32":
                                        value.controls.kill();
                                        break;
                                    case"linux":
                                        value.controls.kill();
                                        break;
                                }
                                this.__stopDelay(2);
                                if (!this.__delayStatus) {
                                    this.__vm_updateHostData();
                                }
                            } else {
                                throw new Error("需要停止的虚拟机未处于运行状态！");
                            }
                        }
                    });
                } else {
                    throw new Error("传参数据异常！");
                }
            } else {
                throw new Error("系统未准备就绪或请求错误！");
            }
            res.end();
        } catch (e) {
            res.statusCode = 404;
            let logNum = this.__vm_logging_Area.length;
            this.__vm_logging_Area.push({logNumber: logNum, logContent: e.message});
            res.send(e.message);
            res.end();
        }
    }
}