#!/usr/bin/env node
"use strict";
let website_express = require("website_express");
let {vm_webInterface, webRouter} = require("./virtual/interface.js");
let path = require('node:path');
let VirtualMachine_Service = new website_express();
VirtualMachine_Service.EnableHttpMode = true;
VirtualMachine_Service.DefaultRootPath = path.join(__dirname, "virtual", "resource", "ui");
VirtualMachine_Service.SpecialSupport = [webRouter];
vm_webInterface.ServiceSystem(() => {
    VirtualMachine_Service.ServiceHost = vm_webInterface.AccessConfig.Address;
    VirtualMachine_Service.ServicePort = vm_webInterface.AccessConfig.Port;
    VirtualMachine_Service.openServer().then(() => {
    }).catch(() => {
    });
});