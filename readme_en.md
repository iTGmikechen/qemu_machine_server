#QEMU Virtual Machine System Management Software

##introduce

QEMU Virtual Machine System Management Software made By node JS, express,website_express, gtcpk-be, gtcpk-fe, Axios, Vue, VueX, ElementUI Components.

The premise of using this Software is to Install the Software of "QEMU Virtual Machine 7.0", "node JS 14.17.5" or above in the default Installation location and in a typical or full Installation manner

QEMU Virtual Machine Official Website Download and Installation Address: https://www.qemu.org/download

QEMU Virtual Machine System Management Software is the Product Branch of [Gitee](https://gitee.com/iTGmikechen/qemu_machine_server.git) Code Library. It is Open for Download by Default and Convenient for Everyone to use. 

##Installation tutorial

#####1.  Open the terminal in the directory specified by your self (such as "test") and enter "npm i qemu_machine_manager"  will Install QEMU Virtual Machine Management Software, As Shown in the Figure: 
![](docres/1.png)

#####2.  Modify "package.json" File, Add The following content: "scripts":{"qemu_vm_manager":"qemu_machine_manager_start"}, . As Shown in the Figure: 
![](docres/2.png)

#####3.  in After The Terminal Input Command "npm run qemu_machine_manager"  Terminal Interface Displays The Running Process Information, As Shown in the Figure Below: 

![](docres/3.png)

Note: Since the QEMU Virtual Machine Management Software Needs to Collect, Analyze and Process the data of the QEMU Virtual Machine Software of the Current Computer for the first time, it Needs to Wait for Some Time. The initialization Process will Not occur During the Next Startup. 

##instructions

####1. Interface display.

in After the Terminal is Started Successfully, in Google Chrome Browser input Terminals Display default "http://localhost:8080" The website can be accessed, As Shown in the Figure: 
![](docres/4.png)

####2. Basic Software Settings.

QEMU Virtual Machine System Management Software provides the Default Virtual Machine Configuration Storage Location, network access address and port Configuration change. The Software setting is extremely important and is related to the normal operation of QEMU Virtual Machine. Due to the reasons of QEMU Virtual Machine Software itself, the path specified by the Virtual Machine Configuration storage location can only be in English, not in Chinese or other characters. The network access address and port Configuration can only be changed to the network address and idle network port number owned by the current computer. As shown in the figure: 

![](docres/5.png)

![](docres/6.png)

Rendering after Restarting the Software

![](docres/7.png)

####3. Create and edit Virtual Machine Configuration data

QEMU Virtual Machine System Management Software provides Graphical data Operation Components to Create and Edit Virtual Machine Configuration data, As Shown in the Figure Below: 

![](docres/8.png)

![](docres/9.png)

![](docres/10.png)

![](docres/11.png)

####4. Import and export of Virtual Machine data

QEMU Virtual Machine System Management Software provides Import and Export functions to facilitate the import and export of Virtual Machine Configuration data that users consider important. As shown in the figure: 

![](docres/12.png)

![](docres/13.png)

####5. About Software

QEMU Virtual Machine System Management Software provides Graphical display of the Current Version of Virtual Machine Management Software, technical support, physical real Machine and QEMU Virtual Machine Software Version. As Shown in the Figure: 

![](docres/14.png)

![](docres/15.png)

![](docres/16.png)

![](docres/17.png)

![](docres/18.png)

####6. Operation effect

The names "Seven Ultimate x64" Virtual Machine has been created. The following is a screenshot of the Run effect: 

![](docres/19.png)

![](docres/23.png)

![](docres/20.png)

![](docres/21.png)

The CPU model Display depends on the Virtualization technology Configured by QEMU Virtual Machine

![](docres/22.png)

The Following log Section shows some Operation logs of QEMU Virtual Machine Management Software

####7. Conclusion of the Author.

QEMU Virtual Machine Management Software is Made to Improve the author's own Front-End development Level. The Author is Only an ordinary Front-End Engineer with a College degree. 

The Author Compare Prefers Software Products using Virtualization technologies such as VMware, VirtualBox and QEMU. Considering that QEMU does not Have its own Management UI interface as the former two, it not Convenient for Operation and Use.

The Development and Production of QEMU Virtual Machine Management Software took the Author more than 3 Months. The Author has Little Experience in the Development of the Back-End and does not Understand the Development of The Back-End and Cross Platform data Input and Output Processing. 

After the Study of some Back-End Project Cases, node JS is Finally Selected as The Back-End of the Software and Compatible with The Front-End development. It can be used across Linux and Windows Platforms. 

The Purpose and Original Intention of Making QEMU Virtual Machine Management Software have Been Shown in the Figure. After All, Virtual Machine Software is All Command-Line Operation, Which is Really Difficult for Novices. 

if You have Good Suggestions or Development Technologies, you can Discuss them on Gitee. 

Space is Limited, I'll Introduce more Skills Here, Waiting for You to Explore.

This Software does Not support multi Language Display Switching.