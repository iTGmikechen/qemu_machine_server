# QEMU 虚拟机系统管理软件

## 介绍

一款使用 node JS、express、website_express、gtcpk-be、gtcpk-fe、axios、Vue、VueX、ElementUI 制作的 QEMU 虚拟机系统管理软件。

使用本软件的前提是以默认安装位置，按典型或完全安装的方式安装好的 “QEMU 虚拟机 7.0”、“node JS 14.17.5” 或以上版本的软件


QEMU 虚拟机官网下载安装地址：https://www.qemu.org/download

QEMU 虚拟机系统管理软件为 [Gitee](https://gitee.com/iTGmikechen/qemu_machine_server.git) 仓库的 product 分支，是默认开放下载的，方便大家使用。

##安装教程

#####1.  在自己指定的目录中，(如 “test”) 打开终端，输入 npm i qemu_machine_manager 将安装 QEMU 虚拟机管理软件，如图：

![](docres/1.png)

#####2.  将目录下的 package.json 文件加入以下内容 "scripts":{"qemu_vm_manager":"qemu_machine_manager_start"}, 如图：

![](docres/2.png)

#####3.  在终端输入 npm run qemu_vm_manager 运行后，终端界面显示运行的过程信息，如图：

![](docres/3.png)

注意：由于 QEMU 虚拟机管理软件首次运行需要进行对当前电脑的 QEMU 虚拟机软件的数据收集分析处理，需要等待一部分时间，下次启动时将不会出现初始化的过程。

## 使用说明

###1.  界面展示

在终端启动成功后，在 Google Chrome 浏览器输入终端上面显示的默认 http://localhost:8080 网址即可访问，如图：

![](docres/4.png)

###2.  软件基本设置

QEMU 虚拟机系统管理软件提供默认的虚拟机配置存放位置、网络访问地址和端口配置的更改，软件设置极为重要，关系着 QEMU 虚拟机的正常运行。由于 QEMU 虚拟机软件自身的原因，虚拟机配置存放位置指定的路径只能是英文路径，不可为中文或其他字符的路径。网络访问地址和端口配置的更改，只能更改成当前电脑所拥有的网络地址以及空闲的网络端口号。如图：

![](docres/5.png)

![](docres/6.png)

重新启动软件后的效果图

![](docres/7.png)

###3.  新建和编辑虚拟机配置数据

QEMU 虚拟机系统管理软件提供图形化数据操作组件实现虚拟机配置数据的创建和编辑，如图：

![](docres/8.png)

![](docres/9.png)

![](docres/10.png)

![](docres/11.png)

###4.  虚拟机数据的导入导出

QEMU 虚拟机系统管理软件提供导入导出功能，方便将用户认为重要的虚拟机配置数据实现导入导出。如图：

![](docres/12.png)

![](docres/13.png)

###5.  关于软件

QEMU 虚拟机系统管理软件提供图形化展示当前的虚拟机管理软件的版本、技术支持、物理真机、QEMU 虚拟机软件版本的信息。如图：

![](docres/14.png)

![](docres/15.png)

![](docres/16.png)

![](docres/17.png)

![](docres/18.png)

###6.  运行效果

前面已经创建了 "Seven Ultimate x64" 虚拟机，以下是运行效果截图：

![](docres/19.png)

![](docres/23.png)

![](docres/20.png)

![](docres/21.png)

CPU 的型号显示，取决与 QEMU 虚拟机配置的虚拟化技术而决定的

![](docres/22.png)

下面日志部分，展示了 QEMU 虚拟机管理软件的一些操作记录日志

###7.   作者结语

制作 QEMU 虚拟机管理软件 是为了提升作者本人自身的前端开发水平，作者本人只是一名拥有专科学历的普通的前端工程师。

作者本人也比较喜欢用 vmware、VirtualBox、QEMU等虚拟化技术的软件产品，考虑到 QEMU 不像前两者那样自有管理UI界面，不方便操作使用。

QEMU 虚拟机管理软件 开发制作花费了作者3多个月的时间，作者本人对于后端的开发几乎没有经验，开发后端以及跨平台数据输入输出处理不了解。

经过部分的后端项目案例的研究后，终究选择node JS 作为软件的后端，同时兼容前端开发。实现跨 Linux、Windows 平台使用。

制作 QEMU 虚拟机管理软件的目的和初衷，已经在图上表示。毕竟全是命令行操作的虚拟机软件，对于新手的确很难。

如果有好的建议或开发技术，可在 Gitee 上多讨论。

篇幅有限，介绍到这，更多技巧，等你探索

此软件暂未支持多语言显示切换